module Veeta
  module I18n
    class Compile
      @@folder = File.join(Rails.root, 'config/locales')
      @@result_folder = File.join(Rails.root, 'public/locales')

      def initialize
        Dir.mkdir(@@result_folder) unless File.exists?(@@result_folder)
        @hash = {}
      end

      def call
        convert_all
        save_all Rails.env.production?
      end

      def convert_all
        Dir[File.join(@@folder, '*.yml')].sort.each do |locale|
          convert YAML::load(IO.read(locale))
        end
      end

      def convert yaml
        @hash.deep_merge! yaml.to_hash
      end

      def save_all production = true
        @hash.each do |key, value|
          production == true ? save(key, value) : save_dev(key, value)
        end
      end

      def save key, value
        File.open("#{@@result_folder}/#{key}.json","w") do |f|
          f.write(value.to_json)
        end
      end

      def save_dev key, value
        File.open("#{@@result_folder}/#{key}.pretty.json","w") do |f|
          f.write(JSON.pretty_generate(value))
        end
      end

      def self.call(*args)
        new(*args).call
      end
    end

    def self.compile
      Compile.call
    end
  end
end
