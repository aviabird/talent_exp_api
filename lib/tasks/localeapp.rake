namespace :localeapp do
  desc "Pull all translations from localeapp"
  task pull: :environment do
    Rake::Task["localeapp:talent"].invoke
    Rake::Task["localeapp:recruiter"].invoke
    Rake::Task["localeapp:datamapping"].invoke
  end

  desc "Pull talent translations from localeapp and rename them as talent translations"
  task talent: :environment do
    ENV['LOCALEAPP_API_KEY'] = ENV['LOCALEAPP_API_KEY_TALENT']
    p %x{bundle exec localeapp pull}

    I18n.available_locales.each do |locale|
      File.rename("config/locales/#{locale}.yml","config/locales/talent-#{locale}.yml")
    end
  end

  desc "Pull recruiter translations from localeapp and rename them as recruiter translations"
  task recruiter: :environment do
    ENV['LOCALEAPP_API_KEY'] = ENV['LOCALEAPP_API_KEY_RECRUITER']
    p %x{bundle exec localeapp pull}

    I18n.available_locales.each do |locale|
      File.rename("config/locales/#{locale}.yml","config/locales/recruiter-#{locale}.yml")
    end
  end

  desc "Pull data mappings from localeapp and rename them as datamapping translations"
  task datamapping: :environment do
    ENV['LOCALEAPP_API_KEY'] = ENV['LOCALEAPP_API_KEY_DATAMAPPING']
    p %x{bundle exec localeapp pull}
    _3p_fake_locales = [:li,:ng,:te, :ve]
    _3p_fake_locales.each do |locale|
      File.rename("config/locales/#{locale}.yml","config/locales/datamapping-#{locale}.yml")
    end
  end
end
