require_relative '../veeta/i18n/compile'

namespace :i18n do
  desc "Compile YAML to JSON"
  task compile: :environment do
    Veeta::I18n.compile
  end
end
