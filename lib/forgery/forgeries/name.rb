class Forgery::Name < Forgery
  def self.academic_title(locale='en')
    dictionaries["academic_titles_#{locale}".to_s].random.unextend
  end
end