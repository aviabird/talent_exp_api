class Forgery::Personal < Forgery
  def self.skill(locale='en')
    dictionaries["skills_#{locale}".to_s].random.unextend
  end
end