class Rack::Attack
  Rack::Attack.cache.store = ActiveSupport::Cache::RedisStore.new()

  @@limits = {}
  @@limits[:time_period] = 20 # in minutes # previous value is 10
  @@limits[:ban_time] = 120 # in minutes # previous value is 60

  @@limits[:general_request] = {}
  @@limits[:general_request][:track] = 199 # tracks 200. trial # previous value is 99
  @@limits[:general_request][:ban] = 2500 # bans 1000th trial # previous value is 2000
  @@limits[:recruiter_sign_in] = {}
  @@limits[:recruiter_sign_in][:track] = 10 # previous value is 4
  @@limits[:recruiter_sign_in][:ban] = 100 # previous value is 25
  @@limits[:recruiter_sign_in_per_email] = {}
  @@limits[:recruiter_sign_in_per_email][:track] = 10 # previous value is 2
  @@limits[:recruiter_sign_in_per_email][:ban] = 20 # previous value is 5
  @@limits[:recruiter_sign_out] = {}
  @@limits[:recruiter_sign_out][:track] = 10 # previous value is 4
  @@limits[:recruiter_sign_out][:ban] = 100 # previous vlaue is 25
  @@limits[:recruiter_token_validation] = {}
  @@limits[:recruiter_token_validation][:track] = 40 # previous value is 19
  @@limits[:recruiter_token_validation][:ban] = 500 # previous  value is 300
  @@limits[:recruiter_show_details] = {}
  @@limits[:recruiter_show_details][:track] = 50 # previous value is 19
  @@limits[:recruiter_show_details][:ban] = 500 # previous value is 300
  @@limits[:recruiter_talent_search] = {}
  @@limits[:recruiter_talent_search][:track] = 199 # previous value is 99
  @@limits[:recruiter_talent_search][:ban] = 2500 # previous value is 1500


  begin
    # Always allow requests from localhost
    # (blacklist & throttles are skipped)
    Rack::Attack.whitelist('allow from localhost') do |req|
      # Requests are allowed if the return value is truthy
      '127.0.0.1' == req.ip || '::1' == req.ip
    end
    unless Rails.env.test?
      # Cloud flare IPs from https://www.cloudflare.com/ips/
      @@cloud_flare_ips_v4 = HTTP.get("https://www.cloudflare.com/ips-v4").to_s.split("\n")
      @@cloud_flare_ips_v6 = HTTP.get("https://www.cloudflare.com/ips-v6").to_s.split("\n")

      # Whitelist cloud flare IPs
      (@@cloud_flare_ips_v4 + @@cloud_flare_ips_v6).each do |cf_ip|
        Rack::Attack.whitelist("allow from cloudflare ip #{cf_ip}") do |request|
          IPAddr.new(cf_ip).include? client_ip(request)
        end
      end
    end
  rescue
    LogService.call "rack_attack.teapi.init", {details: "Could not initialize IP Whitelist fetched from CloudFlare.", priority: :high, notify_im: true}
  end if defined?(HTTP)


  Rack::Attack.blacklisted_response = lambda do |env|
    # Using 503 because it may make attacker think that they have successfully
    # DOSed the site. Rack::Attack returns 403 for blacklists by default
    [ 429, {}, ['Blocked due to too many requsts. This event is tracked for further investigation by myVeeta.']]
  end

  ###
  # Make sure that each blacklist has a different name
  # Also make sure that each Allow2Ban filter overall has a unique identifier (including IP)
  # Once one Allow2Ban filter exceeded the limits, the ip will be banned
  ###


  # Track it using ActiveSupport::Notification
  ActiveSupport::Notifications.subscribe("rack.attack") do |name, start, finish, request_id, req|
    #filter passwords for logging
    params_copy = req.params.dup if req.params
    params_copy.delete("password") if params_copy
    action_dispatch_params_copy = req.env['action_dispatch.request.request_parameters'].dup if req.env['action_dispatch.request.request_parameters']
    action_dispatch_params_copy.delete("password") if action_dispatch_params_copy
    if req.env['rack.attack.match_type'] == :track
      LogService.call "rack_attack.teapi.#{req.env['rack.attack.matched']}.track", {source_ip: client_ip(req), details: "Tracking request form suspicious IP #{client_ip(req)}. Most recent request came with following params: {#{action_dispatch_params_copy}}", priority: :high, limit_log: true, notify_im: true}
    end

    if req.env['rack.attack.match_type'] == :blacklist
      LogService.call "rack_attack.teapi.#{req.env['rack.attack.matched']}.ban", {source_ip: client_ip(req), details: "Blocking IP #{client_ip(req)} for an hour due to too many requests. Most recent request came with following params: { #{action_dispatch_params_copy}}", priority: :high, limit_log: true, notify_im: true}
    end

  end


  ### Any request hitting the API
  Rack::Attack.track("general_request", :limit => @@limits[:general_request][:track], :period =>  @@limits[:time_period].minutes) do |req|
    client_ip(req)
  end

  Rack::Attack.blacklist('general_request') do |req|
    Rack::Attack::Allow2Ban.filter("general_request-#{client_ip(req)}", :maxretry => @@limits[:general_request][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      client_ip(req)
    end
  end


  ### Login request with specific credentials (recruiter)
  Rack::Attack.track("recruiter_sign_in_per_email", :limit => @@limits[:recruiter_sign_in_per_email][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path == '/api/recruiter/auth/sign_in'  && req.post? && client_ip(req) && self.email(req)
  end

  Rack::Attack.blacklist('recruiter_sign_in_per_email') do |req|
    Rack::Attack::Allow2Ban.filter("recruiter_sign_in_per_email-#{client_ip(req)}-#{self.email(req)}", :maxretry => @@limits[:recruiter_sign_in_per_email][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/api/recruiter/auth/sign_in' && req.post? && client_ip(req) && self.email(req)
    end
  end

  ### Login request
  Rack::Attack.track("recruiter_sign_in", :limit => @@limits[:recruiter_sign_in][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path == '/api/recruiter/auth/sign_in'  && req.post? && client_ip(req)
  end

  Rack::Attack.blacklist('recruiter_sign_in') do |req|
    Rack::Attack::Allow2Ban.filter("recruiter_sign_in-#{client_ip(req)}", :maxretry => @@limits[:recruiter_sign_in][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/api/recruiter/auth/sign_in'  && req.post? && client_ip(req)
    end
  end

  # Logout request
  Rack::Attack.track("recruiter_sign_out", :limit => @@limits[:recruiter_sign_out][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path == '/api/recruiter/auth/sign_out' && req.delete?  && client_ip(req)
  end

  Rack::Attack.blacklist('recruiter_sign_out') do |req|
    Rack::Attack::Allow2Ban.filter("recruiter_sign_out-#{client_ip(req)}", :maxretry => @@limits[:recruiter_sign_out][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/api/recruiter/auth/sign_out'  && req.delete? && client_ip(req)
    end
  end

  # Token validation request
  Rack::Attack.track("recruiter_token_validation", :limit => @@limits[:recruiter_token_validation][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path == '/api/recruiter/auth/validate_token' && req.get?  && client_ip(req)
  end

  Rack::Attack.blacklist('recruiter_token_validation') do |req|
    Rack::Attack::Allow2Ban.filter("recruiter_token_validation-#{client_ip(req)}", :maxretry => @@limits[:recruiter_token_validation][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/api/recruiter/auth/validate_token'  && req.get? && client_ip(req)
    end
  end

  # Requesting a signed in recruiters details
  Rack::Attack.track("recruiter_show_details", :limit => @@limits[:recruiter_show_details][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path == '/api/recruiter/recruiter' && req.get?  && client_ip(req)
  end

  Rack::Attack.blacklist('recruiter_show_details') do |req|
    Rack::Attack::Allow2Ban.filter("recruiter_show_details-#{client_ip(req)}", :maxretry => @@limits[:recruiter_show_details][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/api/recruiter/recruiter'  && req.get? && client_ip(req)
    end
  end

  # Token validation request
  Rack::Attack.track("recruiter_talent_search", :limit => @@limits[:recruiter_talent_search][:track], :period =>  @@limits[:time_period].minutes) do |req|
    req.path == '/api/recruiter/talents' && req.get?  && client_ip(req)
  end

  Rack::Attack.blacklist('recruiter_talent_search') do |req|
    Rack::Attack::Allow2Ban.filter("recruiter_talent_search-#{client_ip(req)}", :maxretry => @@limits[:recruiter_talent_search][:ban], :findtime => @@limits[:time_period].minutes, :bantime => @@limits[:ban_time].minutes) do
      req.path == '/api/recruiter/talents'  && req.get? && client_ip(req)
    end
  end

  def self.client_ip request
    ip = ""
    if request.env['HTTP_X_FORWARDED_FOR']
      ips = request.env['HTTP_X_FORWARDED_FOR'].split(",")
      if ips
        ip = ips[0].strip
      end
    end
    if ip.blank?
      ip = request.ip
    end
    ip
  end

  def self.email req
    if req.env['action_dispatch.request.request_parameters']
      req.env['action_dispatch.request.request_parameters']['email']
    elsif req.params['email']
      req.params['email']
    else
      nil
    end
  end

end
