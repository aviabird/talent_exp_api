require 'resque'
require 'resque_scheduler'
require 'resque_scheduler/server'
#require 'resque/scheduler/server'
Resque.redis = ENV['REDIS_URL']
if ENV.has_key?("PROCESS_TYPE") && ENV["PROCESS_TYPE"] == 'web'
  #Resque.workers.each {|w| w.unregister_worker}
  # unregister only the workers that are not actual processes - http://stackoverflow.com/a/10627255
  Resque.workers.each {|w| matches = w.id.match(/^[^:]*:([0-9]*):[^:]*$/); pid = matches[1]; w.unregister_worker unless w.worker_pids.include?(pid.to_s)}
end
Resque::Scheduler.dynamic = false
