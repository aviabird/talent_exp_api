require File.expand_path('../boot', __FILE__)

require 'rails/all'
#require 'oauth/rack/oauth_filter'
require 'rack/attack'


# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Veeta
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    # Set English and Deutsch as supported languages
    config.i18n.available_locales = [:en, :de]

    # Load libraries
    config.eager_load_paths << root.join('lib', 'veeta')
    config.eager_load_paths << root.join('lib', 'myveeta_domain')

    # Use Resque to run background jobs
    config.active_job.queue_adapter = :resque

    #config.middleware.use Rack::Attack
    config.middleware.insert_after(ActionDispatch::ParamsParser, Rack::Attack)

    config.assets.precompile << /\.(?:svg|eot|woff|ttf)\z/
    # Make sure we have a process type variable
    if ENV.has_key?("PROCESS_TYPE")
        # If we do, assuming its a comma seperated list
        ENV["PROCESS_TYPE"].split(",").each { |type|
            # Require the current process type, and
            # current process type and environment joined by an underscore
            Bundler.require(type, "#{type}")
            Bundler.require(type, "#{type}_#{Rails.env}")
        }
    end



  end
end
