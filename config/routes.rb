
Rails.application.routes.draw do

  # mount the models
  mount MyveetaDomain::Engine => "/easy"


  root 'welcome#index'

  # required for unauthenticated access to auth object
  #devise_for :recruiter_auths, :path => '', :skip => [:registerable, :registrations,  :confirmable, :confirmations, :passwords], :path_names => {:sign_in => 'login', :sign_out => 'logout'}, controllers: {
  #  confirmations: 'api/recruiter/auth/confirmations',
  #  passwords: 'api/recruiter/auth/devise_passwords',
  #  unlocks: 'api/recruiter/auth/unlocks' }

  #universal functionality introduced to other classes
  concern :validable do
    member do
      put :validate
    end
  end

  concern :validables do
    collection do
      post :validate
    end
  end

  namespace :api, defaults: {format: :json} do
#### PLEASE CAREFULLY IMPLEMENT AUTHORIZATION BEFORE ACTIVATING IT!!!!
    namespace :recruiter do
      # this namepsace accepts authenticated requests only

      #overwriting controllers of devise with custom controllers
      mount_devise_token_auth_for 'RecruiterAuth', skip: [:omniauth_callbacks, :registrations, :confirmable, :confirmations, :unlockable, :unlocks], at: '/auth',
                                  controllers: {
                                      sessions: 'api/recruiter/auth/sessions',
                                      token_validations:  'api/recruiter/auth/token_validations', 
                                      #registrations: 'api/recruiter/auth/registrations',
                                      passwords: 'api/recruiter/auth/passwords'
                                  }

      resources :talents, only: [:index] do
        put :update_talents_rating
      end

      resource :recruiter, only: [:show], concerns: :validable do
        put :update_locale
      end
      resources :jobs, only: [:index]
    end
=begin

      resources :jobs, concerns: :validables
      resources :jobapps, except: [:new, :create], concerns: :validables do
        member do
          put :withdrawn
        end
      end

      resources :talents, only: [:index, :show]
      namespace :talent do
        resources :recruiter_info
        resources :company_info
      end

      resources :resumes, only: [:show]

      resource :recruiter, only: [:show, :edit, :update], concerns: :validable do
        collection do
          post :avatar_upload
        end
      end

      resources :pools
      resources :shortlists, only: [:index]
    end
=end

#### TEMP UPLOAD DEACTIVATED - PLEASE CAREFULLY CHECK/IMPLEMENT AUTHORIZATION BEFORE ACTIVATING IT!!!!
#    resources :temp_uploaders do
#      collection do
#        post :image
#        post :document
#      end
#    end

    # common list data
    resources :locales, only: :index
    resources :countries, only: :index

  end

end
