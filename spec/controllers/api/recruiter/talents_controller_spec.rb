require "rails_helper"

RSpec.describe Api::Recruiter::TalentsController, :type => :controller do
  describe "search" do

    before :each do
      allow_any_instance_of(ResumeSharing).to receive(:latest_sharing_with_company?).and_return(true)
      @resume = FactoryGirl.create(:resume)
      @resume.update_attribute :origin_id, @resume.id
      @resume_sharing = FactoryGirl.create(:resume_sharing, resume_id: @resume.id)
      @recruiter = @resume_sharing.company.recruiter
      @recruiter.update_column(:company_id,@resume_sharing.company.id)
      @resumes = FactoryGirl.create_list(:resume, 10)
      @resumes.each do|r|
        r.update_attribute :origin_id, r.id
        FactoryGirl.create(:resume_sharing, resume_id:r.id, company_id: @resume_sharing.company.id )
      end
      @headers = @resume_sharing.company.recruiter.auth.create_new_auth_token
      @headers.merge!("myveeta-verification-token" => ENV['MYVEETA_VERIFICATION_TOKENS'])
      request.headers.merge!(@headers)
    end

    it "search with search_params" do
      get :index, {format: :json, search: {query: 'John' }}
      expect(response.status).to be(200)
      expect(JSON.parse(response.body)["meta"]["exact_total"] != 0).to be_truthy
      expect(JSON.parse(response.body)["meta"]["total"] != 0).to be_truthy
    end

    it "search without search_params" do
      get :index, {format: :json}
      expect(response.status).to be(200)
      expect(JSON.parse(response.body)["meta"]["exact_total"] != 0).to be_truthy
      expect(JSON.parse(response.body)["meta"]["total"] != 0).to be_truthy
    end

    it "search with pagination per page parameters" do
      get :index, {format: :json, search: {query: '' },per:2}
      expect(response.status).to be(200)
      json_obj = JSON.parse(response.body)
      expect(json_obj["meta"]["page"]).to eq(1)
      expect(json_obj["meta"]["per"]).to eq(2)
    end

    it "search with pagination page parameters" do
      get :index, {format: :json, search: {query: '' }, page: 2}
      expect(response.status).to be(200)
      json_obj = JSON.parse(response.body)
      expect(json_obj["meta"]["per"]).to eq(25)
      expect(json_obj["meta"]["page"]).to eq(2)
    end

    it "search with wrong query search parameters" do
      get :index, {format: :json, search: {query: 'xyz' }}
      expect(response.status).to be(200)
      expect(JSON.parse(response.body)["meta"]["exact_total"] == 0).to be_truthy
      expect(JSON.parse(response.body)["meta"]["total"] == 0).to be_truthy
    end

    it "search with availability filter" do
      get :index, {format: :json, search: {query: 'John', availability: '1m' }}
      expect(response.status).to be(200)
      expect(JSON.parse(response.body)["meta"]["exact_total"] != 0).to be_truthy
      expect(JSON.parse(response.body)["meta"]["total"] != 0).to be_truthy
    end

    it "search with with middle word" do
      get :index, {format: :json, search: {query: 'oh'}}
      expect(response.status).to be(200)
      expect(JSON.parse(response.body)["talents"].first["first_name"]).to eq("John")
    end

    it "search with with end word" do
      get :index, {format: :json, search: {query: 'hn'}}
      expect(response.status).to be(200)
      expect(JSON.parse(response.body)["talents"].first["first_name"]).to eq("John")
    end

    it "search with with start word" do
      get :index, {format: :json, search: {query: 'jo'}}
      expect(response.status).to be(200)
      expect(JSON.parse(response.body)["talents"].first["first_name"]).to eq("John")
    end

    it "should not search" do
      get :index, {format: :json, search: {query: 'sakfhjhsahfdklfkjd'}}
      expect(response.status).to be(200)
      expect(JSON.parse(response.body)["talents"].present?).to be false
    end

    it "should update rating of talent" do
       @comapany_talent_info=FactoryGirl.create(:company_talent_info, company_id: @recruiter.company_id, talent_id: @resume.talent_id )
      get :update_talents_rating, { format: :json, talent_id: @resume.talent_id, rating_value: "5" }
      expect(response.status).to be(200)
      expect(JSON.parse(response.body)["success"]).to be true
    end

     it "should not update rating of talent" do
       @comapany_talent_info=FactoryGirl.create(:company_talent_info, company_id: @recruiter.company_id, talent_id: @resume.talent_id )
      get :update_talents_rating, { format: :json, talent_id: @resume.talent_id, rating_value: nil }
      expect(response.status).to be(422)
      expect(JSON.parse(response.body)["success"]).to be false
    end

    it "should create company_talent_info and update rating" do
      get :update_talents_rating, { format: :json, talent_id: @resume.talent_id, rating_value: "5" }
      expect(response.status).to be(200)
      expect(JSON.parse(response.body)["success"]).to be true
    end

    it "should not update rating of talent create and update rating" do
      get :update_talents_rating, { format: :json, talent_id: @resume.talent_id, rating_value: nil }
      expect(response.status).to be(422)
      expect(JSON.parse(response.body)["success"]).to be false
    end
  end
end
