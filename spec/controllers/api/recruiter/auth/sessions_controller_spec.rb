require "rails_helper"

RSpec.describe Api::Recruiter::Auth::SessionsController, :type => :controller do
  describe "session" do
    let(:password) { "test@123" }
    let(:email) {"test@yopmail.com"}
    let(:error) {"User was not found or was not logged in."}

    before :each do
      request.env['devise.mapping'] = Devise.mappings[:api_recruiter_recruiter_auth]
      @recruiter_auth = FactoryGirl.create(:recruiter_auth)
    end

    context "Sign-in" do
      it "should sign-in the recruiter with valid email and password" do
        post :create, { name: "Recruiter Sign In", email: @recruiter_auth.email, password: @recruiter_auth.password }
        expect(response.headers["access-token"].present?).to be_truthy
        expect(response.headers["client"].present?).to be_truthy
      end

      it "should not sign-in with in valid email" do
        post :create, { name: "Recruiter Sign In", email: email, password: password }
        expect(response.headers["access-token"].present?).to be(false)
        expect(response.headers["client"].present?).to be(false)
      end

      it "should not sign-in with in valid password" do
        post :create, { name: "Recruiter Sign In", email: @recruiter_auth.email, password: password }
        expect(response.headers["access-token"].present?).to be(false)
        expect(response.headers["client"].present?).to be(false)
      end
    end

    context "sign-out" do
      it "should sign-out recruiter" do
        headers = @recruiter_auth.create_new_auth_token
        delete :destroy, {}.merge(headers)
        expect(response.status).to be(200)
        expect(JSON.parse(response.body)["success"]).to be_truthy
      end

      it "should not sign-out recruiter without uid" do
        headers = @recruiter_auth.create_new_auth_token
        headers.delete("uid")
        delete :destroy, {}.merge(headers)
        expect(response.status).to be(404)
        expect(JSON.parse(response.body)["errors"].include? error).to be_truthy 
      end

      it "should not sign-out recruiter without access-token" do
        headers = @recruiter_auth.create_new_auth_token
        headers.delete("access-token")
        delete :destroy, {}.merge(headers)
        expect(response.status).to be(404)
        expect(JSON.parse(response.body)["errors"].include? error).to be_truthy 
      end

      it "should not sign-out recruiter without client" do
        headers = @recruiter_auth.create_new_auth_token
        headers.delete("client")
        delete :destroy, {}.merge(headers)
        expect(response.status).to be(404)
        expect(JSON.parse(response.body)["errors"].include? error).to be_truthy 
      end
    end
  end
end
