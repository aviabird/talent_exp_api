# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160815102715) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "accepted_jobs_languages", id: false, force: :cascade do |t|
    t.uuid    "job_id"
    t.integer "language_id"
  end

  add_index "accepted_jobs_languages", ["job_id"], name: "index_accepted_jobs_languages_on_job_id", using: :btree
  add_index "accepted_jobs_languages", ["language_id"], name: "index_accepted_jobs_languages_on_language_id", using: :btree

  create_table "access_security_token_mappings", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "individual_key"
    t.string   "resource_type"
    t.string   "external_entity_type"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.uuid     "resource_id"
    t.uuid     "external_entity_id"
    t.datetime "deleted_at"
  end

  add_index "access_security_token_mappings", ["deleted_at"], name: "index_access_security_token_mappings_on_deleted_at", using: :btree
  add_index "access_security_token_mappings", ["external_entity_type"], name: "index_access_security_token_mappings_on_external_entity_type", using: :btree
  add_index "access_security_token_mappings", ["individual_key"], name: "index_access_security_token_mappings_on_individual_key", unique: true, using: :btree
  add_index "access_security_token_mappings", ["resource_type"], name: "index_access_security_token_mappings_on_resource_type", using: :btree

  create_table "admin_auths", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.uuid     "admin_id",                            null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "failed_attempts",        default: 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "uid"
    t.string   "provider"
    t.string   "tokens"
  end

  add_index "admin_auths", ["created_at"], name: "index_admin_auths_on_created_at", using: :btree
  add_index "admin_auths", ["email"], name: "index_admin_auths_on_email", unique: true, using: :btree
  add_index "admin_auths", ["reset_password_token"], name: "index_admin_auths_on_reset_password_token", unique: true, using: :btree
  add_index "admin_auths", ["unlock_token"], name: "index_admin_auths_on_unlock_token", using: :btree

  create_table "admins", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "first_name", null: false
    t.string   "last_name",  null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "admins", ["created_at"], name: "index_admins_on_created_at", using: :btree

  create_table "application_settings", force: :cascade do |t|
    t.string   "setting"
    t.string   "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "application_settings", ["setting"], name: "index_application_settings_on_setting", using: :btree

  create_table "ats_integrations", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "company_id",                                      null: false
    t.string   "integration_type",                                null: false
    t.string   "integration_endpoint"
    t.string   "integration_service_type",                        null: false
    t.string   "integration_service_details",      default: "{}"
    t.string   "transition_service_type",                         null: false
    t.string   "transition_service_details",       default: "{}"
    t.string   "authentication_type"
    t.string   "authentication_details",           default: "{}"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.boolean  "active",                           default: true
    t.string   "temporary_authentication_details", default: "{}"
  end

  add_index "ats_integrations", ["company_id"], name: "index_ats_integrations_on_company_id", using: :btree
  add_index "ats_integrations", ["integration_service_type"], name: "index_ats_integrations_on_integration_service_type", using: :btree
  add_index "ats_integrations", ["integration_type"], name: "index_ats_integrations_on_integration_type", using: :btree

  create_table "autosuggestions", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "key"
    t.string   "value"
    t.string   "text"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "language"
    t.uuid     "stem"
    t.date     "approved_at"
    t.string   "approved_by"
  end

  add_index "autosuggestions", ["created_at"], name: "index_autosuggestions_on_created_at", using: :btree

  create_table "client_applications", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.string   "url"
    t.string   "support_url"
    t.string   "callback_url"
    t.string   "key",                           limit: 40
    t.string   "secret",                        limit: 40
    t.uuid     "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "webhook_url"
    t.string   "delete_user_profile_deep_link"
    t.string   "logo"
    t.string   "cancel_url"
    t.boolean  "use_cancel_callback",                      default: true
    t.string   "contact"
    t.datetime "deleted_at"
    t.string   "token"
    t.text     "logo_meta"
  end

  add_index "client_applications", ["created_at"], name: "index_client_applications_on_created_at", using: :btree
  add_index "client_applications", ["key"], name: "index_client_applications_on_key", unique: true, using: :btree
  add_index "client_applications", ["token"], name: "index_client_applications_on_token", using: :btree

  create_table "companies", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "owner_id"
    t.string   "name",                                   null: false
    t.text     "tnc_text"
    t.string   "tnc_link"
    t.datetime "veeta_terms_accepted_at"
    t.string   "veeta_terms_accepted_ip"
    t.string   "veeta_terms_accepted_by"
    t.string   "plan"
    t.datetime "deleted_at"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.text     "contact"
    t.string   "logo"
    t.boolean  "blocking_notice",         default: true
    t.string   "token"
    t.text     "logo_meta"
    t.string   "short_name"
    t.string   "custom_company_type"
  end

  add_index "companies", ["created_at"], name: "index_companies_on_created_at", using: :btree
  add_index "companies", ["deleted_at"], name: "index_companies_on_deleted_at", using: :btree
  add_index "companies", ["name"], name: "index_companies_on_name", using: :btree
  add_index "companies", ["owner_id"], name: "index_companies_on_owner_id", using: :btree
  add_index "companies", ["short_name"], name: "index_companies_on_short_name", unique: true, using: :btree
  add_index "companies", ["token"], name: "index_companies_on_token", using: :btree

  create_table "company_settings", force: :cascade do |t|
    t.uuid     "company_id"
    t.text     "additional_settings"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.boolean  "withdraw_mail_enabled",       default: true
    t.boolean  "revoke_mail_enabled",         default: true
    t.boolean  "update_mail_enabled",         default: true
    t.boolean  "jobapp_mail_enabled",         default: true
    t.boolean  "talentpool_mail_enabled",     default: true
    t.uuid     "job_fetching_integration_id"
  end

  add_index "company_settings", ["company_id"], name: "index_company_settings_on_company_id", using: :btree

  create_table "company_talent_infos", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "company_id"
    t.uuid     "talent_id"
    t.integer  "rating"
    t.text     "note"
    t.string   "category"
    t.integer  "sequential_id",         null: false
    t.string   "hashed_talent_id"
    t.string   "hashed_talent_id_salt"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "ext_id"
    t.text     "ext_myveeta_events"
  end

  add_index "company_talent_infos", ["company_id"], name: "index_company_talent_infos_on_company_id", using: :btree
  add_index "company_talent_infos", ["ext_id"], name: "index_company_talent_infos_on_ext_id", using: :btree
  add_index "company_talent_infos", ["hashed_talent_id"], name: "index_company_talent_infos_on_hashed_talent_id", using: :btree
  add_index "company_talent_infos", ["sequential_id", "company_id"], name: "index_company_talent_infos_on_sequential_id_and_company_id", unique: true, using: :btree
  add_index "company_talent_infos", ["sequential_id"], name: "index_company_talent_infos_on_sequential_id", using: :btree
  add_index "company_talent_infos", ["talent_id"], name: "index_company_talent_infos_on_talent_id", using: :btree

  create_table "custom_autosuggestions", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "key"
    t.string   "scope"
    t.string   "value"
    t.string   "text"
    t.uuid     "talent_id"
    t.uuid     "recruiter_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "custom_autosuggestions", ["created_at"], name: "index_custom_autosuggestions_on_created_at", using: :btree

  create_table "cv_sharing_requests", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "company_id",        null: false
    t.uuid     "recruiter_id"
    t.uuid     "pool_id"
    t.integer  "language_id",       null: false
    t.string   "status"
    t.boolean  "blocking_notice"
    t.text     "contact_details"
    t.string   "ext_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "pool_welcome_text"
    t.string   "redirect_link"
  end

  add_index "cv_sharing_requests", ["company_id"], name: "index_cv_sharing_requests_on_company_id", using: :btree
  add_index "cv_sharing_requests", ["ext_id"], name: "index_cv_sharing_requests_on_ext_id", using: :btree
  add_index "cv_sharing_requests", ["language_id"], name: "index_cv_sharing_requests_on_language_id", using: :btree
  add_index "cv_sharing_requests", ["pool_id"], name: "index_cv_sharing_requests_on_pool_id", using: :btree

  create_table "cv_sharing_responses", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "resume_id"
    t.uuid     "cv_sharing_request_id",                           null: false
    t.string   "cv_sharing_request_type",                         null: false
    t.text     "blocked_companies"
    t.datetime "read_at"
    t.boolean  "draft",                           default: true
    t.string   "application_referrer_url"
    t.boolean  "allow_recruiter_contact_sharing", default: false, null: false
    t.datetime "company_terms_accepted_at"
    t.string   "company_terms_accepted_ip"
    t.datetime "access_revoked_at"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.boolean  "update_only",                     default: false
    t.string   "revoke_reason"
    t.datetime "submitted_at"
  end

  add_index "cv_sharing_responses", ["access_revoked_at"], name: "index_cv_sharing_responses_on_access_revoked_at", using: :btree
  add_index "cv_sharing_responses", ["cv_sharing_request_id"], name: "index_cv_sharing_responses_on_cv_sharing_request_id", using: :btree
  add_index "cv_sharing_responses", ["read_at"], name: "index_cv_sharing_responses_on_read_at", using: :btree
  add_index "cv_sharing_responses", ["resume_id"], name: "index_cv_sharing_responses_on_resume_id", using: :btree

  create_table "email_applications", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "company_id"
    t.uuid     "recruiter_id"
    t.string   "company_name"
    t.string   "jobname"
    t.string   "recruiter_email"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.boolean  "draft",           default: true
    t.boolean  "unsolicited"
  end

  add_index "email_applications", ["company_id"], name: "index_email_applications_on_company_id", using: :btree
  add_index "email_applications", ["created_at"], name: "index_email_applications_on_created_at", using: :btree
  add_index "email_applications", ["recruiter_email"], name: "index_email_applications_on_recruiter_email", using: :btree
  add_index "email_applications", ["recruiter_id"], name: "index_email_applications_on_recruiter_id", using: :btree

  create_table "email_settings", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "lookup_token"
    t.uuid     "recruiter_id"
    t.string   "email"
    t.datetime "last_mail_sent_at"
    t.integer  "interval_in_days"
    t.boolean  "individual"
    t.string   "option"
    t.string   "type"
    t.datetime "deleted_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "email_settings", ["email"], name: "index_email_settings_on_email", using: :btree
  add_index "email_settings", ["interval_in_days"], name: "index_email_settings_on_interval_in_days", using: :btree
  add_index "email_settings", ["lookup_token"], name: "index_email_settings_on_lookup_token", using: :btree
  add_index "email_settings", ["recruiter_id"], name: "index_email_settings_on_recruiter_id", using: :btree

  create_table "exposes", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "jobapp_id",  null: false
    t.text     "content"
    t.string   "subject",    null: false
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "exposes", ["created_at"], name: "index_exposes_on_created_at", using: :btree
  add_index "exposes", ["deleted_at"], name: "index_exposes_on_deleted_at", using: :btree
  add_index "exposes", ["jobapp_id"], name: "index_exposes_on_jobapp_id", using: :btree

  create_table "geo_addresses", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.integer  "country_id"
    t.uuid     "state_id"
    t.string   "zip"
    t.string   "city"
    t.string   "address_line"
    t.string   "address_line_2"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "geo_addresses", ["address_line"], name: "index_geo_addresses_on_address_line", using: :btree
  add_index "geo_addresses", ["address_line_2"], name: "index_geo_addresses_on_address_line_2", using: :btree
  add_index "geo_addresses", ["city"], name: "index_geo_addresses_on_city", using: :btree
  add_index "geo_addresses", ["country_id"], name: "index_geo_addresses_on_country_id", using: :btree
  add_index "geo_addresses", ["created_at"], name: "index_geo_addresses_on_created_at", using: :btree
  add_index "geo_addresses", ["latitude"], name: "index_geo_addresses_on_latitude", using: :btree
  add_index "geo_addresses", ["longitude"], name: "index_geo_addresses_on_longitude", using: :btree
  add_index "geo_addresses", ["state_id"], name: "index_geo_addresses_on_state_id", using: :btree
  add_index "geo_addresses", ["zip"], name: "index_geo_addresses_on_zip", using: :btree

  create_table "geo_states", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.integer  "country_id", null: false
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "geo_states", ["country_id"], name: "index_geo_states_on_country_id", using: :btree
  add_index "geo_states", ["created_at"], name: "index_geo_states_on_created_at", using: :btree
  add_index "geo_states", ["name"], name: "index_geo_states_on_name", using: :btree

  create_table "integration_logs", force: :cascade do |t|
    t.string   "talent_email"
    t.string   "company_name"
    t.string   "job_name"
    t.string   "ats_integration_type"
    t.string   "endpoint"
    t.string   "key"
    t.string   "priority"
    t.string   "source"
    t.string   "log_type"
    t.integer  "amount"
    t.string   "talent_id"
    t.string   "company_id"
    t.string   "ats_integration_id"
    t.string   "sync_id"
    t.string   "jobapp_id"
    t.string   "job_id"
    t.string   "sharing_response_id"
    t.string   "resume_sharing_id"
    t.string   "recruiter_id"
    t.string   "resume_id"
    t.string   "hashed_talent_id"
    t.string   "details"
    t.text     "data"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "integration_logs", ["company_id"], name: "index_integration_logs_on_company_id", using: :btree
  add_index "integration_logs", ["key"], name: "index_integration_logs_on_key", using: :btree
  add_index "integration_logs", ["log_type"], name: "index_integration_logs_on_log_type", using: :btree
  add_index "integration_logs", ["priority"], name: "index_integration_logs_on_priority", using: :btree
  add_index "integration_logs", ["talent_email"], name: "index_integration_logs_on_talent_email", using: :btree

  create_table "job_sources", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "company_id"
    t.uuid     "referrer_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "job_sources", ["company_id"], name: "index_job_sources_on_company_id", using: :btree
  add_index "job_sources", ["referrer_id"], name: "index_job_sources_on_referrer_id", using: :btree

  create_table "jobapps", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "resume_id"
    t.uuid     "job_id",                                          null: false
    t.string   "job_type",                                        null: false
    t.integer  "language_id"
    t.text     "cover_letter"
    t.text     "contact_preferences"
    t.text     "blocked_companies"
    t.string   "source"
    t.integer  "recruiter_rating"
    t.text     "recruiter_note"
    t.string   "talent_info_status"
    t.boolean  "added_by_recruiter",              default: false, null: false
    t.datetime "read_at"
    t.uuid     "resume_snapshot_id"
    t.string   "application_referrer_url"
    t.boolean  "allow_recruiter_contact_sharing", default: false, null: false
    t.datetime "company_terms_accepted_at"
    t.string   "company_terms_accepted_ip"
    t.datetime "withdrawn_at"
    t.text     "withdrawn_reason"
    t.datetime "access_revoked_at"
    t.datetime "hidden_at"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.boolean  "draft",                           default: true
    t.text     "source_details"
    t.string   "revoke_reason"
    t.datetime "submitted_at"
    t.integer  "intial_resume_version"
    t.datetime "rejected_at"
    t.string   "rejected_by"
    t.string   "reject_reason"
    t.uuid     "referrer_id"
    t.string   "started_as"
  end

  add_index "jobapps", ["access_revoked_at"], name: "index_jobapps_on_access_revoked_at", using: :btree
  add_index "jobapps", ["added_by_recruiter"], name: "index_jobapps_on_added_by_recruiter", using: :btree
  add_index "jobapps", ["created_at"], name: "index_jobapps_on_created_at", using: :btree
  add_index "jobapps", ["job_id"], name: "index_jobapps_on_job_id", using: :btree
  add_index "jobapps", ["job_type"], name: "index_jobapps_on_job_type", using: :btree
  add_index "jobapps", ["language_id"], name: "index_jobapps_on_language_id", using: :btree
  add_index "jobapps", ["read_at"], name: "index_jobapps_on_read_at", using: :btree
  add_index "jobapps", ["recruiter_rating"], name: "index_jobapps_on_recruiter_rating", using: :btree
  add_index "jobapps", ["referrer_id"], name: "index_jobapps_on_referrer_id", using: :btree
  add_index "jobapps", ["resume_id"], name: "index_jobapps_on_resume_id", using: :btree
  add_index "jobapps", ["talent_info_status"], name: "index_jobapps_on_talent_info_status", using: :btree

  create_table "jobs", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "company_id",                            null: false
    t.uuid     "recruiter_id"
    t.uuid     "assigned_recruiter_id"
    t.integer  "language_id",                           null: false
    t.string   "status"
    t.string   "name"
    t.string   "code"
    t.string   "reference"
    t.text     "contact_details"
    t.datetime "deleted_at"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "ext_id"
    t.boolean  "unsolicited_job",       default: false
    t.string   "redirect_link"
  end

  add_index "jobs", ["company_id"], name: "index_jobs_on_company_id", using: :btree
  add_index "jobs", ["created_at"], name: "index_jobs_on_created_at", using: :btree
  add_index "jobs", ["ext_id"], name: "index_jobs_on_ext_id", using: :btree
  add_index "jobs", ["name"], name: "index_jobs_on_name", using: :btree
  add_index "jobs", ["recruiter_id"], name: "index_jobs_on_recruiter_id", using: :btree
  add_index "jobs", ["status"], name: "index_jobs_on_status", using: :btree

  create_table "logs", force: :cascade do |t|
    t.string   "source"
    t.uuid     "source_user_id"
    t.string   "source_user_email"
    t.string   "source_ip"
    t.string   "source_referrer_url"
    t.string   "priority"
    t.string   "key"
    t.string   "resource_type"
    t.uuid     "resource_id"
    t.uuid     "status"
    t.string   "details"
    t.text     "data"
    t.uuid     "resume_id"
    t.uuid     "jobapp_id"
    t.uuid     "company_id"
    t.uuid     "talent_id"
    t.uuid     "recruiter_id"
    t.uuid     "admin_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "logs", ["admin_id"], name: "index_logs_on_admin_id", using: :btree
  add_index "logs", ["company_id"], name: "index_logs_on_company_id", using: :btree
  add_index "logs", ["jobapp_id"], name: "index_logs_on_jobapp_id", using: :btree
  add_index "logs", ["key"], name: "index_logs_on_key", using: :btree
  add_index "logs", ["priority"], name: "index_logs_on_priority", using: :btree
  add_index "logs", ["recruiter_id"], name: "index_logs_on_recruiter_id", using: :btree
  add_index "logs", ["resource_id"], name: "index_logs_on_resource_id", using: :btree
  add_index "logs", ["resource_type"], name: "index_logs_on_resource_type", using: :btree
  add_index "logs", ["resume_id"], name: "index_logs_on_resume_id", using: :btree
  add_index "logs", ["source"], name: "index_logs_on_source", using: :btree
  add_index "logs", ["source_user_email"], name: "index_logs_on_source_user_email", using: :btree
  add_index "logs", ["source_user_id"], name: "index_logs_on_source_user_id", using: :btree
  add_index "logs", ["status"], name: "index_logs_on_status", using: :btree
  add_index "logs", ["talent_id"], name: "index_logs_on_talent_id", using: :btree

  create_table "myveeta_domain_tr_api_manufacturers", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "oauth_nonces", force: :cascade do |t|
    t.string   "nonce"
    t.integer  "timestamp"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "oauth_nonces", ["nonce", "timestamp"], name: "index_oauth_nonces_on_nonce_and_timestamp", unique: true, using: :btree

  create_table "oauth_tokens", force: :cascade do |t|
    t.uuid     "user_id"
    t.string   "type",                  limit: 20
    t.uuid     "client_application_id"
    t.string   "token",                 limit: 40
    t.string   "secret",                limit: 40
    t.string   "callback_url"
    t.string   "verifier",              limit: 20
    t.string   "scope"
    t.datetime "authorized_at"
    t.datetime "invalidated_at"
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "oauth_tokens", ["token"], name: "index_oauth_tokens_on_token", unique: true, using: :btree

  create_table "pdf_template_settings", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.integer  "ext_id"
    t.string   "pdf_template_type"
    t.string   "color"
    t.string   "font_face"
    t.string   "name"
    t.string   "page_format"
    t.integer  "format"
    t.string   "line_height"
    t.string   "template"
    t.uuid     "resume_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.datetime "last_generated_at"
    t.string   "template_identifier"
    t.text     "custom_render_settings"
  end

  create_table "pools", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "company_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pools", ["company_id"], name: "index_pools_on_company_id", using: :btree
  add_index "pools", ["created_at"], name: "index_pools_on_created_at", using: :btree

  create_table "pools_talents", id: false, force: :cascade do |t|
    t.uuid "pool_id"
    t.uuid "talent_id"
  end

  add_index "pools_talents", ["pool_id"], name: "index_pools_talents_on_pool_id", using: :btree
  add_index "pools_talents", ["talent_id"], name: "index_pools_talents_on_talent_id", using: :btree

  create_table "recruiter_auths", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "email",                           default: "", null: false
    t.string   "encrypted_password",              default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.text     "tokens"
    t.string   "provider"
    t.string   "uid",                             default: "", null: false
    t.uuid     "recruiter_id"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.string   "authentication_token"
    t.datetime "authentication_token_created_at"
    t.integer  "failed_attempts",                 default: 0
    t.string   "unlock_token"
    t.datetime "locked_at"
  end

  add_index "recruiter_auths", ["authentication_token"], name: "index_recruiter_auths_on_authentication_token", using: :btree
  add_index "recruiter_auths", ["confirmation_token"], name: "index_recruiter_auths_on_confirmation_token", using: :btree
  add_index "recruiter_auths", ["email"], name: "index_recruiter_auths_on_email", unique: true, using: :btree
  add_index "recruiter_auths", ["recruiter_id"], name: "index_recruiter_auths_on_recruiter_id", unique: true, using: :btree
  add_index "recruiter_auths", ["reset_password_token"], name: "index_recruiter_auths_on_reset_password_token", unique: true, using: :btree
  add_index "recruiter_auths", ["unlock_token"], name: "index_recruiter_auths_on_unlock_token", using: :btree

  create_table "recruiter_comments", force: :cascade do |t|
    t.uuid     "company_id",     null: false
    t.integer  "commented_id",   null: false
    t.string   "commented_type", null: false
    t.uuid     "recruiter_id"
    t.text     "content"
    t.datetime "deleted_at"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "recruiter_comments", ["commented_id"], name: "index_recruiter_comments_on_commented_id", using: :btree
  add_index "recruiter_comments", ["commented_type"], name: "index_recruiter_comments_on_commented_type", using: :btree
  add_index "recruiter_comments", ["company_id"], name: "index_recruiter_comments_on_company_id", using: :btree
  add_index "recruiter_comments", ["deleted_at"], name: "index_recruiter_comments_on_deleted_at", using: :btree

  create_table "recruiter_settings", force: :cascade do |t|
    t.uuid     "recruiter_id"
    t.text     "additional_settings"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "recruiter_settings", ["recruiter_id"], name: "index_recruiter_settings_on_recruiter_id", using: :btree

  create_table "recruiter_talent_infos", force: :cascade do |t|
    t.uuid     "recruiter_id"
    t.uuid     "talent_id"
    t.datetime "followed_at"
    t.datetime "hidden_at"
    t.datetime "in_pool_since"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "recruiter_talent_infos", ["followed_at"], name: "index_recruiter_talent_infos_on_followed_at", using: :btree
  add_index "recruiter_talent_infos", ["in_pool_since"], name: "index_recruiter_talent_infos_on_in_pool_since", using: :btree
  add_index "recruiter_talent_infos", ["recruiter_id"], name: "index_recruiter_talent_infos_on_recruiter_id", using: :btree
  add_index "recruiter_talent_infos", ["talent_id"], name: "index_recruiter_talent_infos_on_talent_id", using: :btree

  create_table "recruiters", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "company_id"
    t.string   "email",                        null: false
    t.string   "first_name"
    t.string   "last_name",                    null: false
    t.string   "phone_number"
    t.datetime "deleted_at"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "avatar"
    t.string   "token"
    t.text     "avatar_meta"
    t.string   "locale",       default: "en"
    t.boolean  "read_all",     default: false
  end

  add_index "recruiters", ["company_id"], name: "index_recruiters_on_company_id", using: :btree
  add_index "recruiters", ["created_at"], name: "index_recruiters_on_created_at", using: :btree
  add_index "recruiters", ["deleted_at"], name: "index_recruiters_on_deleted_at", using: :btree
  add_index "recruiters", ["email"], name: "index_recruiters_on_email", using: :btree
  add_index "recruiters", ["last_name"], name: "index_recruiters_on_last_name", using: :btree

  create_table "referrers", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name"
    t.string   "slug"
    t.text     "description"
    t.boolean  "active"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "resume_pdfs", force: :cascade do |t|
    t.uuid     "resume_id"
    t.string   "file"
    t.string   "file_tmp"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "resume_pdfs", ["resume_id"], name: "index_resume_pdfs_on_resume_id", using: :btree

  create_table "resume_sharing_requests", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "email"
    t.string   "contact_name"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.uuid     "resume_id",    null: false
  end

  add_index "resume_sharing_requests", ["created_at"], name: "index_resume_sharing_requests_on_created_at", using: :btree
  add_index "resume_sharing_requests", ["email"], name: "index_resume_sharing_requests_on_email", using: :btree
  add_index "resume_sharing_requests", ["resume_id"], name: "index_resume_sharing_requests_on_resume_id", using: :btree

  create_table "resume_sharings", force: :cascade do |t|
    t.uuid     "resume_id",            null: false
    t.uuid     "company_id"
    t.uuid     "recruiter_id"
    t.datetime "access_revoked_at"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "revoke_reason"
    t.uuid     "email_application_id"
  end

  add_index "resume_sharings", ["access_revoked_at"], name: "index_resume_sharings_on_access_revoked_at", using: :btree
  add_index "resume_sharings", ["company_id"], name: "index_resume_sharings_on_company_id", using: :btree
  add_index "resume_sharings", ["recruiter_id"], name: "index_resume_sharings_on_recruiter_id", using: :btree
  add_index "resume_sharings", ["resume_id"], name: "index_resume_sharings_on_resume_id", using: :btree

  create_table "resume_templates", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name",       null: false
    t.string   "preview"
    t.string   "html"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "resume_templates", ["created_at"], name: "index_resume_templates_on_created_at", using: :btree
  add_index "resume_templates", ["name"], name: "index_resume_templates_on_name", using: :btree

  create_table "resumes", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "talent_id",                            null: false
    t.integer  "language_id",                          null: false
    t.string   "name",                                 null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.float    "work_experience_duration"
    t.uuid     "origin_id"
    t.integer  "version",                  default: 1
    t.datetime "origin_updated_at"
    t.string   "pdf"
    t.string   "token"
  end

  add_index "resumes", ["created_at"], name: "index_resumes_on_created_at", using: :btree
  add_index "resumes", ["deleted_at"], name: "index_resumes_on_deleted_at", using: :btree
  add_index "resumes", ["language_id"], name: "index_resumes_on_language_id", using: :btree
  add_index "resumes", ["name"], name: "index_resumes_on_name", using: :btree
  add_index "resumes", ["origin_id"], name: "index_resumes_on_origin_id", using: :btree
  add_index "resumes", ["talent_id"], name: "index_resumes_on_talent_id", using: :btree

  create_table "rs_abouts", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "resume_id"
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rs_abouts", ["created_at"], name: "index_rs_abouts_on_created_at", using: :btree
  add_index "rs_abouts", ["resume_id"], name: "index_rs_abouts_on_resume_id", using: :btree

  create_table "rs_awards", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "resume_id"
    t.string   "name"
    t.string   "occupation"
    t.string   "awarded_by"
    t.datetime "year"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rs_awards", ["created_at"], name: "index_rs_awards_on_created_at", using: :btree
  add_index "rs_awards", ["resume_id"], name: "index_rs_awards_on_resume_id", using: :btree

  create_table "rs_certifications", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "resume_id"
    t.string   "subject"
    t.string   "organization"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "certification_type"
    t.integer  "year"
  end

  add_index "rs_certifications", ["created_at"], name: "index_rs_certifications_on_created_at", using: :btree
  add_index "rs_certifications", ["resume_id"], name: "index_rs_certifications_on_resume_id", using: :btree

  create_table "rs_documents", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "resume_id"
    t.string   "file"
    t.string   "document_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "file_type"
    t.string   "token"
  end

  add_index "rs_documents", ["created_at"], name: "index_rs_documents_on_created_at", using: :btree
  add_index "rs_documents", ["resume_id"], name: "index_rs_documents_on_resume_id", using: :btree
  add_index "rs_documents", ["token"], name: "index_rs_documents_on_token", using: :btree

  create_table "rs_educations", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "resume_id",         null: false
    t.string   "type_of_education"
    t.string   "school_name"
    t.string   "degree"
    t.string   "subject"
    t.integer  "country_id"
    t.date     "from"
    t.date     "to"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "completed"
  end

  add_index "rs_educations", ["country_id"], name: "index_rs_educations_on_country_id", using: :btree
  add_index "rs_educations", ["created_at"], name: "index_rs_educations_on_created_at", using: :btree
  add_index "rs_educations", ["from"], name: "index_rs_educations_on_from", using: :btree
  add_index "rs_educations", ["resume_id"], name: "index_rs_educations_on_resume_id", using: :btree
  add_index "rs_educations", ["school_name"], name: "index_rs_educations_on_school_name", using: :btree
  add_index "rs_educations", ["subject"], name: "index_rs_educations_on_subject", using: :btree
  add_index "rs_educations", ["to"], name: "index_rs_educations_on_to", using: :btree
  add_index "rs_educations", ["type_of_education"], name: "index_rs_educations_on_type_of_education", using: :btree

  create_table "rs_languages", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "resume_id"
    t.integer  "language_id"
    t.integer  "level"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rs_languages", ["created_at"], name: "index_rs_languages_on_created_at", using: :btree
  add_index "rs_languages", ["language_id"], name: "index_rs_languages_on_language_id", using: :btree
  add_index "rs_languages", ["level"], name: "index_rs_languages_on_level", using: :btree
  add_index "rs_languages", ["resume_id"], name: "index_rs_languages_on_resume_id", using: :btree

  create_table "rs_links", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "resume_id",  null: false
    t.string   "linkedin"
    t.string   "xing"
    t.string   "website"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rs_links", ["created_at"], name: "index_rs_links_on_created_at", using: :btree

  create_table "rs_memberships", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "resume_id"
    t.string   "organization"
    t.string   "role"
    t.string   "area"
    t.datetime "from"
    t.datetime "to"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rs_memberships", ["created_at"], name: "index_rs_memberships_on_created_at", using: :btree
  add_index "rs_memberships", ["resume_id"], name: "index_rs_memberships_on_resume_id", using: :btree

  create_table "rs_projects", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "resume_id"
    t.string   "name"
    t.string   "role"
    t.string   "link"
    t.datetime "from"
    t.datetime "to"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rs_projects", ["created_at"], name: "index_rs_projects_on_created_at", using: :btree
  add_index "rs_projects", ["resume_id"], name: "index_rs_projects_on_resume_id", using: :btree

  create_table "rs_publications", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "resume_id"
    t.string   "title"
    t.string   "publication_type"
    t.datetime "year"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rs_publications", ["created_at"], name: "index_rs_publications_on_created_at", using: :btree
  add_index "rs_publications", ["resume_id"], name: "index_rs_publications_on_resume_id", using: :btree

  create_table "rs_skills", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "resume_id"
    t.string   "name"
    t.integer  "level"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rs_skills", ["created_at"], name: "index_rs_skills_on_created_at", using: :btree
  add_index "rs_skills", ["level"], name: "index_rs_skills_on_level", using: :btree
  add_index "rs_skills", ["name"], name: "index_rs_skills_on_name", using: :btree
  add_index "rs_skills", ["resume_id"], name: "index_rs_skills_on_resume_id", using: :btree

  create_table "rs_work_experiences", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "resume_id",           null: false
    t.string   "position"
    t.string   "company"
    t.string   "industry"
    t.string   "job_level"
    t.string   "terms_of_employment"
    t.datetime "from"
    t.datetime "to"
    t.integer  "country_id"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rs_work_experiences", ["company"], name: "index_rs_work_experiences_on_company", using: :btree
  add_index "rs_work_experiences", ["country_id"], name: "index_rs_work_experiences_on_country_id", using: :btree
  add_index "rs_work_experiences", ["created_at"], name: "index_rs_work_experiences_on_created_at", using: :btree
  add_index "rs_work_experiences", ["from"], name: "index_rs_work_experiences_on_from", using: :btree
  add_index "rs_work_experiences", ["industry"], name: "index_rs_work_experiences_on_industry", using: :btree
  add_index "rs_work_experiences", ["position"], name: "index_rs_work_experiences_on_position", using: :btree
  add_index "rs_work_experiences", ["resume_id"], name: "index_rs_work_experiences_on_resume_id", using: :btree
  add_index "rs_work_experiences", ["to"], name: "index_rs_work_experiences_on_to", using: :btree

  create_table "shortlists", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "talent_id"
    t.uuid     "recruiter_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "shortlists", ["created_at"], name: "index_shortlists_on_created_at", using: :btree
  add_index "shortlists", ["recruiter_id"], name: "index_shortlists_on_recruiter_id", using: :btree
  add_index "shortlists", ["talent_id"], name: "index_shortlists_on_talent_id", using: :btree

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "talent_auths", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "email",                           default: "",    null: false
    t.string   "encrypted_password",              default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                   default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.text     "tokens"
    t.string   "provider"
    t.string   "uid",                             default: "",    null: false
    t.boolean  "guest",                           default: false
    t.uuid     "talent_id",                                       null: false
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "authentication_token"
    t.datetime "authentication_token_created_at"
    t.string   "xing_request_token"
    t.string   "xing_request_token_secret"
    t.string   "xing_access_token"
    t.string   "xing_access_token_secret"
    t.string   "linkedin_request_token"
    t.string   "linkedin_request_token_secret"
    t.string   "linkedin_access_token"
    t.string   "linkedin_access_token_secret"
    t.integer  "failed_attempts",                 default: 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "reg_type"
  end

  add_index "talent_auths", ["authentication_token"], name: "index_talent_auths_on_authentication_token", using: :btree
  add_index "talent_auths", ["confirmation_token"], name: "index_talent_auths_on_confirmation_token", unique: true, using: :btree
  add_index "talent_auths", ["created_at"], name: "index_talent_auths_on_created_at", using: :btree
  add_index "talent_auths", ["email"], name: "index_talent_auths_on_email", unique: true, using: :btree
  add_index "talent_auths", ["reset_password_token"], name: "index_talent_auths_on_reset_password_token", unique: true, using: :btree
  add_index "talent_auths", ["talent_id"], name: "index_talent_auths_on_talent_id", unique: true, using: :btree
  add_index "talent_auths", ["unlock_token"], name: "index_talent_auths_on_unlock_token", using: :btree

  create_table "talent_settings", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "talent_id",                                           null: false
    t.text     "additional_settings"
    t.integer  "country_id"
    t.integer  "professional_experience_level"
    t.string   "newsletter_locale"
    t.string   "newsletter_updates"
    t.text     "working_locations"
    t.text     "working_industries"
    t.string   "availability"
    t.string   "job_seeker_status",             default: "very_much"
    t.string   "prefered_employment_type"
    t.integer  "salary_exp_min"
    t.integer  "salary_exp_max"
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
  end

  add_index "talent_settings", ["created_at"], name: "index_talent_settings_on_created_at", using: :btree
  add_index "talent_settings", ["talent_id"], name: "index_talent_settings_on_talent_id", using: :btree

  create_table "talents", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "birthdate"
    t.string   "avatar"
    t.string   "salutation"
    t.text     "avatar_meta"
    t.integer  "nationality_country_id"
    t.string   "title"
    t.string   "email",                                  null: false
    t.string   "phone_number"
    t.uuid     "address_id"
    t.datetime "veeta_terms_accepted_at"
    t.string   "veeta_terms_accepted_ip"
    t.string   "locale"
    t.datetime "deleted_at"
    t.string   "delete_reason"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "ext_id"
    t.boolean  "new",                     default: true
    t.string   "token"
    t.string   "military_service"
  end

  add_index "talents", ["avatar"], name: "index_talents_on_avatar", using: :btree
  add_index "talents", ["birthdate"], name: "index_talents_on_birthdate", using: :btree
  add_index "talents", ["created_at"], name: "index_talents_on_created_at", using: :btree
  add_index "talents", ["deleted_at"], name: "index_talents_on_deleted_at", using: :btree
  add_index "talents", ["email"], name: "index_talents_on_email", using: :btree
  add_index "talents", ["first_name"], name: "index_talents_on_first_name", using: :btree
  add_index "talents", ["last_name"], name: "index_talents_on_last_name", using: :btree
  add_index "talents", ["nationality_country_id"], name: "index_talents_on_nationality_country_id", using: :btree
  add_index "talents", ["salutation"], name: "index_talents_on_salutation", using: :btree
  add_index "talents", ["token"], name: "index_talents_on_token", using: :btree

  create_table "temp_cv_imports", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "token"
    t.string   "email"
    t.string   "source_type"
    t.text     "cv_content"
    t.datetime "converted_at"
    t.datetime "expires_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "temp_cv_imports", ["email"], name: "index_temp_cv_imports_on_email", using: :btree
  add_index "temp_cv_imports", ["token"], name: "index_temp_cv_imports_on_token", unique: true, using: :btree

  create_table "temp_uploads", force: :cascade do |t|
    t.string   "type"
    t.string   "file"
    t.string   "file_meta"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "temp_uploads", ["file"], name: "index_temp_uploads_on_file", unique: true, using: :btree

  create_table "trapi_client_instances", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "trapi_client_instance_key"
    t.uuid     "trapi_client_id",           null: false
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "trapi_client_instances", ["trapi_client_id"], name: "index_trapi_client_instances_on_trapi_client_id", using: :btree
  add_index "trapi_client_instances", ["trapi_client_instance_key"], name: "index_trapi_client_instances_on_trapi_client_instance_key", unique: true, using: :btree

  create_table "trapi_clients", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "trapi_client_key"
    t.uuid     "company_id",       null: false
    t.string   "name"
    t.text     "description"
    t.text     "contact_info"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "trapi_clients", ["company_id"], name: "index_trapi_clients_on_company_id", using: :btree
  add_index "trapi_clients", ["trapi_client_key"], name: "index_trapi_clients_on_trapi_client_key", unique: true, using: :btree

  create_table "trapi_consumers", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "trapi_manufacturer_id"
    t.uuid     "trapi_client_id"
    t.uuid     "trapi_client_instance_id"
    t.datetime "last_login"
    t.string   "name"
    t.string   "current_session_token"
    t.datetime "current_session_token_expires_at"
    t.string   "created_by"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "trapi_consumers", ["current_session_token"], name: "index_trapi_consumers_on_current_session_token", using: :btree
  add_index "trapi_consumers", ["trapi_client_id"], name: "index_trapi_consumers_on_trapi_client_id", using: :btree
  add_index "trapi_consumers", ["trapi_client_instance_id"], name: "index_trapi_consumers_on_trapi_client_instance_id", using: :btree
  add_index "trapi_consumers", ["trapi_manufacturer_id"], name: "index_trapi_consumers_on_trapi_manufacturer_id", using: :btree

  create_table "trapi_manufacturers", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "trapi_manufacturer_key"
    t.string   "name"
    t.text     "description"
    t.text     "contact_info"
    t.string   "manufacturer_type"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "trapi_manufacturers", ["manufacturer_type"], name: "index_trapi_manufacturers_on_type", using: :btree
  add_index "trapi_manufacturers", ["trapi_manufacturer_key"], name: "index_trapi_manufacturers_on_trapi_manufacturer_key", unique: true, using: :btree

  create_table "update_settings", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "update_triggerable_id"
    t.string   "update_triggerable_type"
    t.string   "update_type"
    t.datetime "starting_timestamp"
    t.string   "interval"
    t.datetime "last_executed_at"
    t.string   "lookup_token"
    t.boolean  "individual"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "update_settings", ["lookup_token"], name: "index_update_settings_on_lookup_token", using: :btree
  add_index "update_settings", ["update_triggerable_id", "update_triggerable_type", "update_type", "lookup_token"], name: "id_type_update_type_lookup_token", unique: true, using: :btree
  add_index "update_settings", ["update_triggerable_id", "update_triggerable_type", "update_type"], name: "id_type_update_type", using: :btree

  create_table "veeta_button_sharing_connections", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "talent_id"
    t.uuid     "client_application_id"
    t.integer  "oauth_token_id"
    t.datetime "access_revoked_at"
    t.datetime "connected_at"
    t.datetime "deleted_at"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "revoke_reason"
    t.datetime "third_party_data_wiped_out_at"
  end

  add_index "veeta_button_sharing_connections", ["client_application_id"], name: "index_veeta_button_sharing_connections_on_client_application_id", using: :btree
  add_index "veeta_button_sharing_connections", ["oauth_token_id"], name: "index_veeta_button_sharing_connections_on_oauth_token_id", using: :btree
  add_index "veeta_button_sharing_connections", ["talent_id"], name: "index_veeta_button_sharing_connections_on_talent_id", using: :btree

  create_table "veeta_button_sharings", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "resume_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.datetime "last_synced_at"
    t.datetime "deleted_at"
    t.uuid     "veeta_button_sharing_connection_id"
  end

  add_index "veeta_button_sharings", ["created_at"], name: "index_veeta_button_sharings_on_created_at", using: :btree
  add_index "veeta_button_sharings", ["resume_id"], name: "index_veeta_button_sharings_on_resume_id", using: :btree

  add_foreign_key "veeta_button_sharing_connections", "oauth_tokens"
end
