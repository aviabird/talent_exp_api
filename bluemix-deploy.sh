#!/bin/bash

# connect to bluemix and deploy
# rake localeapp:pull
cf api https://api.eu-gb.bluemix.net
cf login -o 'Talent Solutions' -s ${1}
#cf zero-downtime-push vta-main-prod2 -f manifest-${1}.yml
./app-deploy-with-rename.sh intapi2-${1} intapi2-${1}-bak . manifest-${1}.yml
