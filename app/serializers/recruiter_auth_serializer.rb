class RecruiterAuthSerializer < ActiveModel::Serializer
  attributes :id, :email, :recruiter_id
end
