module Concerns
  module AbilitedSerializer
    extend ActiveSupport::Concern

    included do
      attributes :can
    end

    def can
      object.can(current_user)
    end
  end
end
