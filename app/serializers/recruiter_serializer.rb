# == Schema Information
#
# Table name: recruiters
#
#  id           :uuid             not null, primary key
#  company_id   :uuid
#  email        :string           not null
#  first_name   :string
#  last_name    :string           not null
#  phone_number :string
#  locale       :string 
#  deleted_at   :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_recruiters_on_company_id  (company_id)
#  index_recruiters_on_created_at  (created_at)
#  index_recruiters_on_deleted_at  (deleted_at)
#  index_recruiters_on_email       (email)
#  index_recruiters_on_last_name   (last_name)
#

class RecruiterSerializer < ActiveModel::Serializer
  attributes :email, :first_name, :last_name, :phone_number, :name, :avatar_url, :locale, :company_name
  def company_name
  	object.try(:company).try(:name)
  end
end
