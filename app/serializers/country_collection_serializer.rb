class CountryCollectionSerializer < ActiveModel::Serializer
  attributes :id, :name, :iso_code
end
