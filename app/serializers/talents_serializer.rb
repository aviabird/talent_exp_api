class TalentsSerializer < ActiveModel::Serializer
  attributes :first_name, :last_name, :age, :title, :salutation, :cv_pdf_uri,
    :cv_attachments, :email, :avatar_url,:work_experience_duration, :education_duration, :rating, :talent_id
  #, :age,:work_industries, :latest_work_experience,
  #           :salutation, :work_experience_duration, :avatar_url,
  #           :rating, :labels, :company_info_id,  :applied_for_jobs_positions, :is_followed

  has_one :latest_work_experience, serializer: Rs::WorkExperienceSerializer
  has_one :latest_education, serializer: Rs::EducationSerializer

  def first_name
    object.talent.first_name
  end

  def last_name
    object.talent.last_name
  end

  def email
    object.talent.email
  end

  def age
    object.talent.age
  end

  def title
    object.talent.title
  end

  def salutation
    object.talent.salutation
  end


  def cv_pdf_uri
    object.pdf.url if object.pdf
  end

  def avatar_url
    object.talent.avatar_url
  end

  def cv_attachments
    docs = []
    object.documents.each do |d|
      doc = {}
      doc[:document_name] = d.name
      doc[:document_uri] = d.external_url(serialization_options[:current_company])
      doc[:document_type] = d.document_type
      docs << doc
    end if object.documents
    docs
  end

  def talent_id
    object.talent_id
  end

  def rating
    company_talent_info=object.talent.company_infos.where(company_id: serialization_options[:current_company]).first
    rating= company_talent_info.rating if company_talent_info    
    rating ? rating : 0  
  end

end
