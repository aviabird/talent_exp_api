
class Rs::EducationSerializer < ActiveModel::Serializer
  attributes :type_of_education, :school_name, :degree, :subject, :from, :to, :description, :current, :country_id, :completed

end
