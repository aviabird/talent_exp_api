
class Rs::WorkExperienceSerializer < ActiveModel::Serializer
  attributes  :position, :company, :industry, :job_level, :terms_of_employment, :from, :to, :current, :description, :country_id
end
