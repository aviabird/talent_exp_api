class ExternalLinkService
  ###
  # handles mappinps of virtual paths to phyiscal paths
  ###

  #create all mappings of a resume (avatar, documents) and a certain company/tp
  def resume_mapping resume, external_entity
    if resume && external_entity
      mapping external_entity, resume.talent
      if resume.documents
        resume.documents.each do |doc|
          document_mapping doc, external_entity
        end
      end
    end
  end

  #create all mappings of a resume (avatar, documents) and a certain company/tp
  def talent_mapping talent, external_entity
    if talent && external_entity && !mapping_exists(talent, external_entity)
      mapping external_entity, talent
    end
  end

  #create all mappings of a resume (avatar, documents) and a certain company/tp
  def mapping_exists resource, external_entity
    AccessSecurityTokenMapping.exists?(resource_type: resource.class.name, resource_id: resource.id, external_entity_type: external_entity.class.name, external_entity_id: external_entity.id)
  end

  #create all mappings of a resume (avatar, documents) and a certain company/tp
  def remove_talent_mapping talent, external_entity
    remove_performed = false
    if talent && external_entity
      mapping = AccessSecurityTokenMapping.find_mapping talent, external_entity
      if mapping
        mapping.destroy
        remove_performed = true
      end
    end
    remove_performed
  end

  #create mapping for a document
  def document_mapping document, external_entity
    if document && external_entity
      mapping external_entity, document
    end
  end

  #remove all mappings for a certain resume and company/third party
  def remove_mapping resume, external_entity
    remove_performed = false
    if resume && external_entity
      #talent/avatar
      if resume.talent
        mapping = AccessSecurityTokenMapping.find_mapping resume.talent, external_entity
        if mapping
          mapping.destroy
          remove_performed = true
        end
      end
      #documents
      if resume.documents
        resume.documents.each do |doc|
          mapping = AccessSecurityTokenMapping.find_mapping doc, external_entity
          if mapping
            mapping.destroy
            remove_performed = true
          end
        end
      end
    end
    remove_performed
  end

  # update the access_security_part of all mappings with the same uniqe part (so all mappings into same folder)
  def update_mappings unique_part, access_security_part
    #check how to query via method
    AccessSecurityTokenMapping.where(unique_part: unique_part).each do |mapping|
      mapping.access_security_part = access_security_part
      mapping.save
    end
  end

  def kickresume_mapping resource
    mapping = AccessSecurityTokenMapping.find_or_initialize_kickresume_mapping(resource)
    mapping.resource_type = resource.class.name
    mapping.resource_id = resource.id
    mapping.external_entity_type = "Kickresume"
    mapping.external_entity_id = nil
    mapping.save!
  end

  #create all mappings of a resume (avatar, documents) and a certain company/tp
  def remove_kickresume_mapping resource
    remove_performed = false
    if resource
      mapping = AccessSecurityTokenMapping.find_kickresume_mapping resource
      if mapping
        mapping.destroy
        remove_performed = true
      end
    end
    remove_performed
  end

  private
    #simply map company/tp to a resource
    def mapping external_entity, resource
      mapping = AccessSecurityTokenMapping.find_or_initialize_mapping(resource, external_entity)
      mapping.resource_type = resource.class.name
      mapping.resource_id = resource.id
      mapping.external_entity_type = external_entity.class.name
      mapping.external_entity_id = external_entity.id
      mapping.save!
    end

end
