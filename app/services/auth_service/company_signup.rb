class AuthService::CompanySignup
  attr_accessor :company, :recruiter, :auth

  include Virtus.model

  # Company
  attribute :company_name, String

  # Recruiter
  attribute :first_name, String
  attribute :last_name, String

  # Recruiter Auth
  attribute :email, String
  attribute :password, String
  attribute :password_confirmation, String
  attribute :plan, String

  # Additional info
  attribute :ip, String

  def signup!
    ActiveRecord::Base.transaction do
      @company = Company.new
      @company.accept_terms(email, ip)
      @company.name = company_name
      @company.plan = plan

      @recruiter = Recruiter.new
      @recruiter.email = email
      @recruiter.first_name = first_name
      @recruiter.last_name = last_name
      @recruiter.company = @company

      @company.owner = @recruiter

      @auth = RecruiterAuth.new
      @auth.recruiter = @recruiter
      @auth.email = email
      @auth.password = password
      @auth.password_confirmation = password_confirmation

      @recruiter.auth = @auth

      [@company.errors, @recruiter.errors, @auth.errors] unless @company.save && @recruiter.save && @auth.save
    end
  end
end