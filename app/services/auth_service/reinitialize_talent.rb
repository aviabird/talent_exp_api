module AuthService
  class ReinitializeTalent < Service
    attr_reader :email

    def initialize email
      @email = email
    end

    def call
      remove if possible?
      super
    end

    private
      def possible?
        exist? && guest?
      end

      def exist?
        @auth ||= TalentAuth.find_by(email: @email)
      end

      def guest?
        @auth.guest?
      end

      def remove
        @auth.destroy
      end
  end
end