module AuthService
  class SetEmail < Service
    attr_reader :current_auth

    def initialize current_auth, email
      @current_auth = current_auth
      @email = email
    end

    def call
      possible? ? set_email : set_errors
      super
    end

    private
      def possible?
        !@email.nil?
      end

      def set_errors
        #@current_auth.errors.add(:confirmed, "can't force update email") if @current_auth.confirmed?
        @current_auth.errors.add(:email, "wasn't filled in") if @email.nil?
      end

      # Updateing the email of the talent - talent_auth email is set in talten.rb in a filter
      def set_email
        @current_auth.talent.update(email: @email)
      end
  end
end
