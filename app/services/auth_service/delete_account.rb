module AuthService
  class DeleteAccount < Service
    attr_reader :current_auth

    def initialize current_auth, options = {}
      @current_auth = current_auth
      @email = options.delete(:email)
      @password = options.delete(:password)
      @delete_reason = options.delete(:delete_reason)
    end

    def call
      fine? ? delete_account : set_errors
      super
    end

    def success?
      fine?
    end

    private
      def fine?
        params? && valid_password? && valid_email?
      end

      def params?
        !@email.nil? && !@password.nil? && !@delete_reason.nil?
      end

      def valid_password?
        @current_auth.valid_password?(@password.to_s)
      end

      def valid_email?
        @email.to_s == @current_auth.email.to_s
      end

      def delete_account
        password = Devise.friendly_token.first(8)
        @current_auth.email = "ghost-#{@current_auth.id}@email.com"
        @current_auth.password = password
        @current_auth.password_confirmation = password
        @current_auth.skip_reconfirmation!
        @current_auth.save!(validate: false)
        @current_auth.delete(@delete_reason)
      end

      def set_errors
        # All the other errors except the translation are for now taken care of 
        # on the frontend CLEANUP required in future after consultation
        @current_auth.errors.add(:password, "wasn't filled in") if @password.nil?
        @current_auth.errors.add(:base, I18n.t('talent.errors.password.wrong')) unless valid_password?
        @current_auth.errors.add(:email, "wasn't filled in") if @email.nil?
        @current_auth.errors.add(:email, "is not the same") unless valid_email?
        @current_auth.errors.add(:delete_reason, "wasn't filled in") if @delete_reason.nil?
      end
  end
end