module AuthService
  class SetPassword < Service
    attr_reader :current_auth

    def initialize current_auth, password
      @current_auth = current_auth
      @password = password
    end

    def call
      possible? ? set_password : set_errors
      super
    end

    private
      def possible?
        @current_auth.guest?# && !@password.nil?
      end

      def set_errors
        @current_auth.errors.add(:guest, "only can update password") unless @current_auth.guest?
        @current_auth.errors.add(:password, "wasn't filled in") if @password.nil?
      end

      def set_password
        if @current_auth.update(password: @password, password_confirmation: @password)
          @current_auth.send_confirmation_instructions if @current_auth.guest?
          @current_auth.update(guest: false)
        end
      end
  end
end
