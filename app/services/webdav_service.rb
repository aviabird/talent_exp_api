require 'rubygems'
require 'net/dav'


class WebdavService

  def initialize
    @res = Net::DAV.new(ENV['WEBDAV_WRITE_URL'], :curl => false)
    @res.verify_server = false
    @res.credentials(ENV['WEBDAV_USER'], ENV['WEBDAV_PASSWORD'])
    @uri = URI::parse(ENV['WEBDAV_WRITE_URL'])
  end

  # renames a folder via webdav
  def rename_folder from, to
    @res.start do |dav|
      dav.move("#{@uri.path}/#{from}","#{@uri.path}/#{to}")
    end
  end
end
