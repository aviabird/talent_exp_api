class JobImportService < Service


    # if job already exists, just update title, otherwise create job
    def batch_upsert company, company_code, imported_jobs
      created = 0
      updated = 0
      disabled = 0
      imported_jobs.each do |job|
        if Job.exists?(ext_id: job[:job_ext_id])
          existing_job = Job.find_by(ext_id: job[:job_ext_id])
          existing_job.name = CGI.unescapeHTML(job[:job_title])
          existing_job.language = Language.find(job[:job_language])
          existing_job.status = 'new'
          existing_job.save
          updated = updated + 1
        else
          new_job = Job.new
          new_job.name = CGI.unescapeHTML(job[:job_title])
          new_job.company = company
          new_job.status = 'new'
          new_job.recruiter = company.owner
          new_job.language = Language.find(job[:job_language])
          new_job.accepted_jobs_languages.build(language: Language.find('en'))
          new_job.accepted_jobs_languages.build(language: Language.find('de'))
          new_job.ext_id = job[:job_ext_id]
          new_job.save
          created = created + 1
        end
      end

      unless imported_jobs.empty?
        # disable jobs of this company, which were not fetched anymore
        ext_ids = imported_jobs.collect {|imported_job| imported_job[:job_ext_id] }
        jobs_to_disable = Job.where(company: company, status: 'new').where("ext_id NOT IN (?)", ext_ids)
        jobs_to_disable.each do |job|
          job.disable
          disabled = disabled + 1
        end
      else
        LogService.call "JobImportService.batch_upsert", {company: company, details: "No jobs disabled for #{company.name} due to empty job list fetched for this company.", priority: :high}
      end

      LogService.call "JobImportService.batch_upsert", {company: company, details: "#{created+updated+disabled} jobs proccessed for company #{company.name}: #{created} created, #{updated} updated, #{disabled} disabled", priority: :low}
    end


end
