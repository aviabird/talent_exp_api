class ResumeSharingSearch
  include ActiveData::Model

  SORT = {relevance: :_score}

  attribute :query, type: String
  attribute :sort, type: String, enum: %w(first_name date relevance), default_blank: 'relevance'


  def index
    ResumeSharingIndex
  end

  

end
