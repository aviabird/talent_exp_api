class OldTalentSearch

  include Virtus.model

  SORT = {relevance: :_score}

  TALENT_FIRST_NAME = 'first_name'
  TALENT_LAST_NAME = 'last_name'
  RESUME_SKILLS = 'skills'
  RESUME_LANGUAGES = 'languages'
  WORKING_LOCATIONS = 'working_locations'
  WORKING_INDUSTRIES = 'working_industries'

  SALARY_EXPECTATION_MIN = 'salary_exp.min'
  SALARY_EXPECTATION_MAX = 'salary_exp.max'

  WORK_EXPERIENCE_DURATION = 'work_experience_duration'


  attribute :sort, String, enum: %w(relevance), default: 'relevance'

  attribute :query, String

  attribute :skills, Array[String]
  attribute :languages, Array[String]
  attribute :working_locations, Array[String]
  attribute :salary_expectation, Hash[Symbol => Integer]
  attribute :work_experience, Hash[Symbol => Integer]
  attribute :education_degree, Hash[Symbol => Integer]
  attribute :availability, Hash[Symbol => Float]

  def index
    Talent
  end

  def search
    search_criteria = [
        sort_criteria,
        query_criteria,
        aggregations_criteria
    ].compact.reduce(:merge) || '*'

    index.search(search_criteria)
  end

  def suggest

  end

  def query_criteria
    scope = [
        match_filter,
        # hidden_filter,
        # followed_filter,
        salary_expectation_filter,
    # work_experience_filter,
    # education_degree_filter,
    # skills_filter,
    # languages_filter,
    # working_areas_filter,
    ].compact.reduce(:merge)

    scope.nil? ? nil : {
        query: {
            bool: {must: scope}
        }
    }
  end

  def sort_criteria
    {
        sort: [
            {
                "#{SORT[sort.to_sym]}" => {
                    order: :desc
                }
            }
        ]
    }
  end

  def aggregations_criteria
    {
        aggs: {
            skills: {
                terms: {
                    field: RESUME_SKILLS
                }
            },
            languages: {
                terms: {
                    field: RESUME_LANGUAGES
                }
            },
            working_locations: {
                terms: {
                    field: WORKING_LOCATIONS
                }
            },
            working_industries: {
                terms: {
                    field: WORKING_INDUSTRIES
                }
            }
        }
    }
  end


  def match_filter
    {
        #     constant_score: {
        #         filter: {
        multi_match: {
            query: "#{query}",
            fields: [TALENT_FIRST_NAME, TALENT_LAST_NAME]
        }
        # },
        # boost: 1.2
        # }
    } unless self.query.nil? or self.query.length < 2
  end

  def hidden_filter

  end

  def followed_filter

  end

  def label_filter

  end

  def salary_expectation_filter
    return if self.salary_expectation.nil?
    min_scope = {}
    max_scope = {}

    min_scope = {
        constant_score: {
            filter: {
                range: {
                    SALARY_EXPECTATION_MIN => {
                        from: self.salary_expectation[:min]
                    }
                }
            },
            boost: 1.2
        }
    } unless self.salary_expectation[:min].nil?
    max_scope = {
        constant_score: {
            filter: {
                range: {
                    SALARY_EXPECTATION_MAX => {
                        to: self.salary_expectation[:max]
                    }
                }
            },
            boost: 1.2
        }
    } unless self.salary_expectation[:max].nil?
    [min_scope, max_scope].compact.reduce(:merge)
  end

  def work_experience_filter
    return if self.work_experience.nil?
    min_scope = {}
    max_scope = {}

    min_scope = {
        constant_score: {
            filter: {
                range: {
                    WORK_EXPERIENCE_DURATION => {
                        from: self.work_experience[:min]
                    }
                }
            },
            boost: 1.2
        }
    } unless self.work_experience[:min].nil?
    max_scope = {
        constant_score: {
            filter: {
                range: {
                    WORK_EXPERIENCE_DURATION => {
                        to: self.work_experience[:max]
                    }
                }
            },
            boost: 1.2
        }
    } unless self.work_experience[:max].nil?
    [min_scope, max_scope].compact.reduce(:merge)
  end

=begin
  def education_degree_filter
    return if self.education_degree.nil?
    min_scope = {}
    max_scope = {}

    min_scope = {
        constant_score: {
            filter: {
                range: {
                    EDUCATION_DEGREE => {
                        from: self.education_degree[:min]
                    }
                }
            },
            boost: 1.2
        }
    } unless self.education_degree[:min].nil?
    max_scope = {
        constant_score: {
            filter: {
                range: {
                    EDUCATION_DEGREE => {
                        to: self.education_degree[:max]
                    }
                }
            },
            boost: 1.2
        }
    } unless self.education_degree[:max].nil?
    [min_scope, max_scope].compact.reduce(:merge)
  end
=end
  def skills_filter
    {
        constant_score: {
            filter: {
                terms: {
                    RESUME_SKILLS => self.skills,
                    execution: 'and',
                    _cache: true
                }
            },
            boost: 1.2
        }
    } unless self.skills.nil? or self.skills.count < 1
  end


  def languages_filter
    {
        constant_score: {
            filter: {
                terms: {
                    RESUME_LANGUAGES => self.languages,
                    execution: 'and',
                    _cache: true
                }
            },
            boost: 1.2
        }
    } unless self.languages.nil? or self.languages.count < 1
  end

  def working_locations_filter
    {
        constant_score: {
            filter: {
                terms: {
                    WORKING_LOCATIONS => self.working_locations,
                    execution: 'and',
                    _cache: true
                }
            },
            boost: 1.2
        }
    } unless self.working_locations.nil? or self.working_locations.count < 1
  end

  def availability_filter

  end
end
