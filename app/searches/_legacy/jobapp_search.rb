class JobappSearch < TalentSearch

  include Virtus.model

  SORT = {relevance: :_score}

  TALENT_NAME = 'talent.name'
  RESUME_SKILLS = 'resume.skills'
  RESUME_LANGUAGES = 'resume.language_codes'
  WORKING_LOCATIONS = 'talent.setting.working_locations'
  WORKING_INDUSTRIES = 'talent.setting.working_industries'

  SALARY_EXPECTATION_MIN = 'talent.setting.salary_exp_min'
  SALARY_EXPECTATION_MAX = 'talent.setting.salary_exp_max'

  WORK_EXPERIENCE_DURATION = 'resume.work_experience_duration'

  attribute :sort, String, enum: %w(relevance), default: 'relevance'

  attribute :query, String

  attribute :skills, Array[String]
  attribute :language_codes, Array[String]
  attribute :working_locations, Array[String]
  attribute :salary_expectation, Hash[Symbol => Integer]
  attribute :work_experience, Hash[Symbol => Integer]
  attribute :availability, Hash[Symbol => Float]

  def index
    Jobapp
  end

  def should_query
    scope = [
        # followed_filter,
        # label_filter,
        # salary_expectation_filter,
        # work_experience_filter,
        # education_degree_filter,
        # skills_filter,
        # languages_filter,
        # working_areas_filter,
        # availability_filter,
    ].compact.reduce(:merge)
    scope.nil? ? nil : {should: scope}
  end

  def must_query
    scope = [
        query_matching_part,
        hidden_filter,
        followed_filter,
        # salary_expectation_filter,
        # work_experience_filter,
        # education_degree_filter,
        skills_filter,
        languages_filter,
        working_locations_filter,
        # availability_filter
    ].compact.reduce(:merge)
    scope.nil? ? nil : {must: scope}
  end

end
