class TalentSearch
  include ActiveData::Model

  SORT = {first_name: {first_name: :asc}, date: {updated_at: :desc}, relevance: :_score}
  SALARY = {salary_bucket_0: {min: 0, max: 0}, salary_bucket_1: {min: 10000, max: 10000}, salary_bucket_2: {min: 20000, max: 20000}, salary_bucket_3: {min: 30000, max: 30000}, salary_bucket_4: {min: 40000, max: 40000}, salary_bucket_5: {min: 50000, max: 50000}, salary_bucket_6: {min: 60000, max: 70000}, salary_bucket_7: {min: 90000, max: 90000}, salary_bucket_8: {min: 110000}}
  WORK_EXPERIENCE = {work_experience_bucket_0: {min: 0, max: 0}, work_experience_bucket_1: {min:1, max:1}, work_experience_bucket_2: {min:2, max:2}, work_experience_bucket_3: {min: 5, max: 5}, work_experience_bucket_4: {min: 10, max: 10}, work_experience_bucket_5: {min: 10}}

  # the follwing attributes can be passed into the elasticsearch index query.
  attribute :query, type: String
  attribute :working_locations, mode: :arrayed, type: Array, normalize: ->(value) { value.reject{|i| i.blank?} if value}
  attribute :working_industries, mode: :arrayed, type: Array, normalize: ->(value) { value.reject{|i| i.blank?} if value}
  attribute :availability, type: String, enum: %w(immediately 1m 2m 3m 4-6m 6+m)
  attribute :prefered_employment_types, mode: :arrayed, type: Array, normalize: ->(value) { value.reject{|i| i.blank?}  if value}
  attribute :job_seeker_statuses, mode: :arrayed, type: Array, normalize: ->(value) { value.reject{|i| i.blank?} if value}
  attribute :languages, mode: :arrayed, type: Array, normalize: ->(value) { value.reject{|i| i.blank?} if value} 
  attribute :terms_of_employment, mode: :arrayed, type: Array, normalize: ->(value) { value.reject{|i| i.blank?} if value}
  attribute :job_names, mode: :arrayed, type: Array, normalize: ->(value) { value.reject{|i| i.blank?} if value}
  attribute :sort, type: String, enum: %w(first_name date relevance), default_blank: 'relevance'
  attribute :salary_min, type: String
  attribute :salary_max, type: String
  attribute :company_scope, type: String
  attribute :skills, mode: :arrayed, type: Array, normalize: ->(value) { value.reject{|i| i.blank?} if value }
  attribute :jobs, mode: :arrayed, type: Array, normalize: ->(value) { value.reject{|i| i.blank?} if value }
  attribute :min_work_experience_duration, type: String
  attribute :max_work_experience_duration, type: String
  attribute :read_all, type: Boolean
  attribute :experience_in_industries, type: Array, normalize: ->(value) { value.reject{|i| i.blank?} if value}
  attribute :job_titles, type: Array, normalize: ->(value) { value.reject{|i| i.blank?} if value}
  attribute :current_job, type: Boolean


  attribute :important_skills, type: Boolean, default_blank: false
  attribute :important_salary, type: Boolean, default_blank: false
  attribute :important_job_seeker_status, type: Boolean, default_blank: false
  attribute :important_prefered_employment_types, type: Boolean, default_blank: false
  attribute :important_availability, type: Boolean, default_blank: false
  attribute :important_working_industries, type: Boolean, default_blank: false
  attribute :important_working_locations, type: Boolean, default_blank: false
  attribute :important_work_experience_duration, type: Boolean, default_blank: false
  attribute :important_languages, type: Boolean, default_blank: false
  attribute :important_experience_in_industries, type: Boolean, default_blank: false
  attribute :important_job_titles, type: Boolean, default_blank: false
  # work_experience_min
  # work_experience_max
  # highest_education 
  # ...
  # furthermore for every attribute an important marker needs to be applicable:
  #  only if all values of that attribute are met, include them in the search results


  def index
    TalentIndex.limit(10000)
  end

  def search
    # compile the query/filter
    es_query = ([multi_match_query_string, working_locations_list_filter,working_industries_list_filter, availability_filter, salary_min_filter,
        salary_max_filter, prefered_employment_type_filter, job_seeker_status_filter, sorting, work_experience_duration_filter, languages_filter, skills_list_filter, terms_of_employment_filter, experience_in_industries_filter, job_titles_filter, jobs_filter]).compact.reduce(:merge).query_mode(:bool)

    # add all aggregations
    es_query = es_query.aggs(:agg_working_locations).aggs(:agg_working_industries).aggs(:agg_salary_bucket).
        aggs(:agg_availability).aggs(:agg_job_seeker_status).aggs(:agg_prefered_employment_type).aggs(:agg_skills).aggs(:agg_work_experience_duration).aggs(:agg_languages).aggs(:agg_experience_in_industries)

    # always add end enforce the company scope such that a recuriter always only has access to their talents
    # skip company_scope when read_all is set true
    es_query = es_query.filter(term: {"company_id" => company_scope}) unless read_all

    es_query = es_query.filter_mode(:must)

    es_query
  end

  def query_string
    #https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html
    index.query(query_string: { query: query }) if query?
  end

  def multi_match_query_string
    # Need to be add more columns
    index.query(multi_match: {query: ActiveSupport::Inflector.transliterate(query).downcase, fields: set_fields_according_to_query(query), analyzer: 'keyword'}) if query
    # index.query(multi_match: {query: query.downcase, fields: set_fields_according_to_query(query), analyzer: 'keyword'}) if query
  end

  def set_fields_according_to_query(query)
    # set fields where to search
    # ^value is used for boost
    fields = ['email^5', 'address.city^5', 'address.address_line^4', 'address.country^3', 'cv.work_experiences.job_title.prepared^5','cv.work_experiences.experience_in_industry^3', 'cv.work_experiences.company^3', 'cv.work_experiences.country^3', 'cv.work_experiences.description^2', 'cv.educations.subject^5', 'cv.educations.school_name^4', 'cv.educations.degree^1', 'cv.educations.country^2', 'cv.educations.description^3', 'cv.continued_educations.subject^5', 'cv.continued_educations.organization^3', 'cv.continued_educations.description^3', 'cv.languages.language_name.prepared^4', 'cv.skills.skill_name.prepared^5', 'cv.about^4', 'cv.links.links^2', 'cv.links.xing^2', 'cv.links.website^2', 'jobseeker_info.working_locations.prepared^3', 'jobseeker_info.working_industries.prepared^3']
    if query.present?
      fields << ['first_name^5', 'last_name^5'] unless query.scan(/^[0-9]{1,45}$/).present? # check for only number
      fields << ['address.zip^2'] if query.scan(/\d{4}/).present?
      fields << ['birthdate^2'] if include_birthdate_field
      fields << ['phone_number^4'] unless query.scan(/\+\d{10}/).present?
    end
    fields.flatten
  end

  # GETTERS / SETTERS
  def working_locations_list= value
    self.working_locations = value.split(',').map(&:strip)
  end

  def working_locations_list
    self.working_locations.join(', ')
  end

  def working_industries_list= value
    self.working_industries = value.split(',').map(&:strip)
  end

  def working_industries_list
    self.working_industries.join(', ')
  end

  def job_seeker_statuses_list= value
    self.job_seeker_statuses = value.split(',').map(&:strip)
  end

  def job_seeker_statuses_list
    self.job_seeker_statuses.join(', ')
  end

  def prefered_employment_types_list= value
    self.prefered_employment_types = value.split(',').map(&:strip)
  end

  def prefered_employment_types_list
    self.prefered_employment_types.join(', ')
  end

  def skills_list= value
    self.skills = value.split(',').map(&:strip)
  end

  def skills_list
    self.skills.join(', ')
  end

  # SORTING
  def sorting
    index.order(sort.blank? ? SORT[:relevance] : SORT[sort.to_sym])
  end

  ### CV FILTERS

  def skills_list_filter
    # filter according to skills
    skills_hash = {name: "cv.skills.skill_name.raw", filtered_array: skills, important: important_skills}
    set_filter_according_to_important_marker skills_hash
  end

  ### JOBSEEKER INFORMATION FILTERS
  def working_locations_list_filter
    # filter according to working locations
    if working_locations?
      locations = working_locations
      index.filter{ (jobseeker_info.working_locations.raw == locations) }
    end
  end

  def working_industries_list_filter
    # filter according to working industries
    working_industries_hash = {name: "jobseeker_info.working_industries.raw", filtered_array: working_industries, important: important_working_industries}
    set_filter_according_to_important_marker working_industries_hash
  end

  def availability_filter
    if availability?
      if availability == "6+m"
        index.filter{ (!jobseeker_info.availability) }
      else        
        index.filter(terms: {"jobseeker_info.availability" =>  availability_matrix(availability)})
      end
    end
  end

  def salary_min_filter
    min_symbol = salary_min.try(:to_sym)
    max_symbol = salary_max.try(:to_sym)
    min = SALARY[min_symbol][:min] if SALARY[min_symbol].present?
    max = SALARY[max_symbol][:max] if SALARY[max_symbol].present?
    if min.present? && max.present?
      index.filter(range: {"jobseeker_info.salary_min": {gte: min, lte: max}}) 
    elsif min.present?
      index.filter(range: {"jobseeker_info.salary_min": {gte: min}})
    end
  end

  def salary_max_filter
    min_symbol = salary_min.try(:to_sym)
    max_symbol = salary_max.try(:to_sym)
    min = SALARY[min_symbol][:min] if SALARY[min_symbol].present?
    max = SALARY[max_symbol][:max] if SALARY[max_symbol].present?
    if max.present? 
      if min.present? 
        index.filter(range: {"jobseeker_info.salary_max": {gte: min, lte: max}})
      else
        index.filter(range: {"jobseeker_info.salary_max": {lte: max}}) 
     end
    end 
  end

  def prefered_employment_type_filter
    # filter according to prefered_employment_type
    prefered_employment_type_hash = {name: "jobseeker_info.prefered_employment_type", filtered_array: prefered_employment_types, important: important_prefered_employment_types}
    set_filter_according_to_important_marker prefered_employment_type_hash
  end

  def job_seeker_status_filter
    # filter according to job_seeker_status
    if job_seeker_statuses?
      status = job_seeker_statuses
      index.filter{ (jobseeker_info.job_seeker_status  == status) }
    end
  end

  def work_experience_duration_filter
    # currently we have same condition for important and without impotant marker
    work_experience_duration_with_impotant_marker
  end

  def languages_filter
    if languages?
      filter = index.filter
      if important_languages
        language_with_name = (languages.product [3,4,5]).map{|ar| ar.join("-")} # [3,4,5] is fluency         
        filter.criteria.filters << {terms: {"cv.languages.language_name.raw" => language_with_name}}
      else
        languages.each do |language|
          filter.criteria.filters << {term: {"cv.languages.language_name.raw" => language}}
        end
      end
      filter
    end
  end

  def job_names_filter
    # filter according to job_name enterned
    job_names_hash = {name: "cv.work_experiences.job_name", filtered_array: job_names, important: important_job_names}
    set_filter_according_to_important_marker job_names_hash
  end


  def terms_of_employment_filter
    if terms_of_employment?
      index.filter(terms: {"cv.work_experiences.terms_of_employment" => terms_of_employment})
    end
  end

  def work_experience_duration_with_impotant_marker
    max_symbol = max_work_experience_duration.try(:to_sym)
    min_symbol = min_work_experience_duration.try(:to_sym)
    min = WORK_EXPERIENCE[min_symbol][:min] if WORK_EXPERIENCE[min_symbol].present?
    max = WORK_EXPERIENCE[max_symbol][:max] if WORK_EXPERIENCE[max_symbol].present?

    return nil if (min.blank? && max.blank?)
    
    if min.present? && max.present?
      work_exp_duration({ gte: min, lte: max })
    elsif max.present? 
      work_exp_duration({ lte: max })
    elsif min.present?
      work_exp_duration({ gte: min })
    end
  end

  def work_exp_duration(hash)
    # to add work experience filter
    index.filter(range: { "cv.work_experience_duration": hash })
  end

  def experience_in_industries_filter
     # filter according to industries
    industries_hash = {name: "cv.work_experiences.experience_in_industry.raw", filtered_array: experience_in_industries, important: important_experience_in_industries}
    set_filter_according_to_important_marker industries_hash
  end

  def job_titles_filter
    industries_hash = {name: "cv.work_experiences.job_title.prepared", current_job: "cv.job_title_with_current", filtered_array: job_titles, important: important_job_titles, current: current_job}
    set_job_title_according_to_important_marker industries_hash
  end

  def jobs_filter
    # filter to search jobs applied by talent to company
    if jobs?
      index.filter(terms: {jobs: jobs} )
    end
  end

  private

    def availability_matrix key
      av = {}
      av['immediately'] = ['immediately']
      av['1m'] = ['immediately','1m']
      av['2m'] = ['immediately','1m', '2m']
      av['3m'] = ['immediately','1m', '2m', '3m']
      av['4-6m'] = ['immediately','1m', '2m', '3m', '4-6m']
      # av['6+m'] = ['immediately','1m', '2m', '3m', '6+m']
      av[key]
    end

    def set_filter_according_to_important_marker options = {}
      # add fiter according to important and without important marker
      if options[:filtered_array].present?
        filter = index.filter
        if options[:important]
          options[:filtered_array].each do |filter_value|
            filter.criteria.filters << {term: {"#{options[:name]}" =>  filter_value}}
          end
        else
          filter.criteria.filters << {terms: {"#{options[:name]}" =>  options[:filtered_array]}}
        end
        filter
      end
    end
    # if continuos 4 char are present include birthdate in search field
    def include_birthdate_field
      query.scan(/[0-9.-]{4}/).present?
    end

    def set_job_title_according_to_important_marker options = {}
      if options[:filtered_array].present?
        options[:filtered_array] =  options[:filtered_array].map(&:downcase)
        filter = index.filter
        if options[:important]
          options[:filtered_array].each do |filter_value|
            filter.criteria.filters << {term: {"#{options[:current_job]}" =>  filter_value}}
          end
        else
          if options[:current]
            filter.criteria.filters << {terms: {"#{options[:current_job]}" =>  options[:filtered_array]}}
          else
            filter.criteria.filters << {terms: {"#{options[:name]}" =>  options[:filtered_array]}}
          end
        end
        filter
      end
    end
end
