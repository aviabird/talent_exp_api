class TalentFuzzySearch < TalentSearch
  MIN_WORK_EXPERIENCE = 0
  MAX_WORK_EXPERIENCE = 30

  def search

    es_query = ([multi_match_query_string, languages_filter, availability_filter, terms_of_employment_filter, salary_filter, skills_filter, job_seeker_status_filter, working_locations_list_filter, work_experience_duration_filter, working_industries_filter, experience_in_industries_filter, job_titles_filter]).compact
    if es_query.present?
      es_query = es_query.reduce(:merge).query_mode(:should)
      # add all aggregations
      es_query = es_query.aggs(:agg_working_locations).aggs(:agg_working_industries).aggs(:agg_salary_bucket).
          aggs(:agg_availability).aggs(:agg_job_seeker_status).aggs(:agg_prefered_employment_type).aggs(:agg_skills).aggs(:agg_work_experience_duration).aggs(:agg_languages)
      # always add end enforce the company scope such that a recuriter always only has access to their talents
      es_query = es_query.filter(term: {"company_id" => company_scope}) unless read_all

      es_query = es_query.filter_mode(:must)

      es_query
    end
  end

  # create new filter according to close search
  def languages_filter
    if languages?
      filter = index.filter
      if important_languages
        languages.each do |language|
          filter.criteria.filters << {term: {"cv.languages.language_name.raw" => language}}
        end
      else
        filter.criteria.filters << {terms: {"cv.languages.language_name.raw" => languages}}
      end
      filter
    end
  end

  def availability_filter
    if availability? && close_availability_array(availability)
      q = []
      close_availability_array(availability).each_with_index do |available, i|
        q << {constant_score: {filter: {terms: {"jobseeker_info.availability" => available}}, boost: 20 - i}}
      end
      q << {constant_score: {filter: {:missing=>{:field=>"jobseeker_info.availability", :existence=>true, :null_value=>false}}}}
      index.query(q)
    end
  end

  def salary_filter
    min_symbol = salary_min.try(:to_sym)
    max_symbol = salary_max.try(:to_sym)
    min = SALARY[min_symbol][:min] if SALARY[min_symbol].present?
    max = SALARY[max_symbol][:max] if SALARY[max_symbol].present?
    if min.present?
      conditions = []
      close_salary_array(min, max).each_with_index do |salary, i|
        conditions << {constant_score: {filter: {:terms=>{"jobseeker_info.salary_min"=>salary}}, boost: 20 - i}}
        conditions << {constant_score: {filter: {:terms=>{"jobseeker_info.salary_minsalary_max"=>salary}}, boost: 20 - i}}
      end
      # To search for null value in case of not important marker
      unless important_salary
        conditions << {constant_score: {filter: {:missing=> {:field => "jobseeker_info.salary_min"}}}}
        conditions << {constant_score: {filter: {:missing=> {:field => "jobseeker_info.salary_max"}}}}
      end
      index.query(conditions)
    end
  end

  def skills_filter
    filter = index.filter
    if skills?
      if important_skills
        filter.criteria.filters << {terms: { "cv.skills.skill_name.raw" => skills }}
      else
        skll_list = skills
        filter = index.filter{ (cv.skills.skill_name.raw == skll_list) | (!cv.skills) }
      end
    end
    filter
  end

  def working_industries_filter
    filter = index.filter
    if working_industries?
      if important_working_industries
        filter.criteria.filters << {terms: { "jobseeker_info.working_industries.raw" =>working_industries }}
      else
        working_industres = working_industries
        filter =  index.filter{( jobseeker_info.working_industries.raw == working_industres) | (!jobseeker_info.working_industries) }
      end
    end
    filter
  end


    def job_titles_filter
      if job_titles?
        if important_job_titles
          job_titles_list = job_titles.map(&:downcase)
          if current_job
            index.filter{cv.job_title_with_current == job_titles_list}
          else
            index.filter{cv.work_experience.job_title.raw == job_titles_list} 
          end
        else
          job_titles_list = job_titles.map(&:downcase)
          if current_job
            index.filter{ (cv.job_title_with_current == job_titles_list)}   
          else
            index.filter{ (cv.work_experience.job_title.raw == job_titles_list) | (!cv.work_experience.job_titles) }
          end
        end
      end
    end
  
  def job_seeker_status_filter
    if job_seeker_statuses?
      status = job_seeker_statuses
      index.filter{ (jobseeker_info.job_seeker_status == status) | (!jobseeker_info.job_seeker_status)}
    end
  end

  def working_locations_filter
    # filter according to working locations
    if working_locations?
      locations = working_locations
      index.filter{ (jobseeker_info.working_locations.raw == locations) | (!jobseeker_info.working_locations)}
    end
  end

  def work_experience_duration_filter
    if important_work_experience_duration
      work_experience_duration_with_impotant_marker
    else
      work_experience_duration_without_impotant_marker
    end
  end

  def work_experience_duration_without_impotant_marker
    max_symbol = max_work_experience_duration.try(:to_sym)
    min_symbol = min_work_experience_duration.try(:to_sym)
    min = WORK_EXPERIENCE[min_symbol][:min] if WORK_EXPERIENCE[min_symbol].present?
    max = WORK_EXPERIENCE[max_symbol][:max] if WORK_EXPERIENCE[max_symbol].present?

    return nil if (min.blank? && max.blank?)

    close_durations = close_work_duration_array(min, max)

    conditions = []
    if min.present? && max.present?
      conditions << filter_conditions_with_min_and_max_durations(min, max, close_durations)
    elsif min.present?
      conditions << filter_conditions_only_min_durations(min, close_durations)
    elsif max.present?
      conditions << filter_conditions_only_max_durations(max, close_durations)
    end
    conditions << {constant_score: {filter: {:missing=>{:field=>"cv.work_experience_duration", :existence=>true, :null_value=>false}}}}
    index.query(conditions)
  end

  def experience_in_industries_filter
    if experience_in_industries?
      if important_experience_in_industries
        index.filter << {terms: { "cv.work_experience.experience_in_industry.raw" => experience_in_industries }}
      else
        experience_in_industry_list = experience_in_industries
        index.filter{ (cv.work_experience.experience_in_industry.raw == experience_in_industry_list) | (!cv.work_experience.experience_in_industry) }
      end
    end
  end

  private
    def close_availability_array(availability)
      # create startic array for the close_availability_array.
      case availability
        when "immediately" 
          [["1m"], ["2m"], ["3m"], ["4-6m"], ["6+m"]]
        when "1m"
          [["immediately", "2m"], ["3m"], ["4-6m"], ["6+m"]]
        when "2m"
          [["1m", "3m"], ["immediately", "4-6m"], ["6+m"]]
        when "3m"
          [["2m", "4-6m"], ["1m", "6+m"], ["immediately"]]
        when "4-6m"
          [["3m", "6+m"], ["2m"], ["1m"], ["immediately"]]
        when "6+m"
          [["4-6m"], ["3m"], ["2m"], ["1m"], ["immediately"]]
      end  
    end

    def close_salary_array(min, max)
      # create a close array to search close results.
      min_salaries =  min.present? ? TalentSetting.where("salary_exp_min < ?", min).order(salary_exp_min: :desc).map(&:salary_exp_min).uniq : []
      max_salaries =  max.present? ? TalentSetting.where("salary_exp_max > ?", max).order(salary_exp_max: :asc).map(&:salary_exp_max).uniq : []
      close_salaries = []
      if min_salaries.size > max_salaries.size
        close_salaries = min_salaries.zip (max_salaries)
      else
        max_salaries.each_with_index {|salary, index| close_salaries << [min_salaries[index], salary]}
      end
      close_salaries.map(&:compact)
    end

    def close_work_duration_array(min, max)
      min_experiences = min.present? ? (MIN_WORK_EXPERIENCE..((min > 0) ? min - 1 : 0)).to_a.reverse : []
      max_experiences = max.present? ? ((max + 1)..MAX_WORK_EXPERIENCE).to_a : []
      close_durations = []
      if min_experiences.size > max_experiences.size
        close_durations = min_experiences.zip (max_experiences)
      else
        max_experiences.each_with_index {|exp, index| close_durations << [min_experiences[index], exp]}
      end
      close_durations.map(&:compact)
    end

    def filter_conditions_with_min_and_max_durations(min, max, close_durations)
      conditions = []
      close_durations.each_with_index do |exp, i|
        if exp[0].present? && exp[1].present?
          conditions << sorting_query_for_work_experience_duration({gte: exp[0], lt: min}, i)
          conditions << sorting_query_for_work_experience_duration({gte: max, lt: exp[1]}, i)
          min = exp[0]
          max = exp[1]
        else
          if min < exp[0]
            conditions << sorting_query_for_work_experience_duration({gte: max, lt: exp[0]}, i)
          else
            conditions << sorting_query_for_work_experience_duration({gte: exp[0], lt: min}, i)
          end
          min = max = exp[0]
        end 
        
      end
      conditions
    end

    def filter_conditions_only_max_durations(max, close_durations)
      conditions = []
      close_durations.each_with_index do |exp, i|
        conditions << sorting_query_for_work_experience_duration({gte: max, lt: exp[1]}, i) if exp[1].present?
        max = exp[1]
      end
      conditions
    end

    def filter_conditions_only_min_durations(min, close_durations)
      conditions = []
      close_durations.each_with_index do |exp, i|
        conditions << sorting_query_for_work_experience_duration({gte: exp[0], lt: min}, i) if exp[0].present?
        min = exp[0]
      end
      conditions
    end

    def sorting_query_for_work_experience_duration(hash, i)
      {constant_score: {filter: {range: {"cv.work_experience_duration": hash}}, boost: 40 - i} }
    end
end




