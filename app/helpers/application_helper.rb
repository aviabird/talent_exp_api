module ApplicationHelper

  def date_helper date, locale
    if locale == 'de'
      date.strftime('%m/%Y')
    else
      date.strftime('%m/%Y')
    end
  end

  def birthdate_helper date, locale
    if locale == 'de'
      date.strftime('%d.%m.%Y')
    else
      date.strftime('%Y-%m-%d')
    end
  end

  def iso_code_translator iso_code
    unless iso_code.blank?
      I18n.t(iso_code)
    else
      ''
    end
  end

  def industry_with_label industry
    unless industry.blank?
      industry = I18n.t("talent.data.industry.#{industry}")
      I18n.t("talent.pdf.work_experience.industry_with_label", industry: industry)
    else
      ''
    end
  end

  def html_encode text
    unless text.blank?
      text = CGI::escapeHTML text
      text.gsub(/\n/, "<br/>")
    else
      ''
    end
  end
end
