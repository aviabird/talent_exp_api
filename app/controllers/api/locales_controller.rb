class Api::LocalesController < Api::ApplicationController

  ###
  # No authorization required, access to locales is public
  ###

  def index
    render json: TranslationsService.call(locale)
  end

  private
    def locale
      params['lang'] || I18n.default_locale.to_s
    end
end
