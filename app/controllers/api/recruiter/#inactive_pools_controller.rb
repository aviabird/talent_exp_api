class Api::Recruiter::PoolsController < Api::ApplicationController
  include RecruiterApi

  before_action :set_pool, only: [:show]

  def index
    @pools = Pool.in_company(current_recruiter.company)
    respond_with @pools, options(log: true, each_serializer: Recruiters::PoolsSerializer)
  end

  def show
    respond_with @pool, options(log: true, serializer: Recruiters::PoolSerializer)
  end

  private
    def set_pool
      @pool = Pool.find(params[:id])
    end
end
