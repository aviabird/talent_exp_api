class Api::Recruiter::JobsController < Api::ApplicationController
  include RecruiterApi

  before_action :set_job, only: [:show, :edit, :update, :destroy]

  authorize_resource

  def index
    @jobs = Job.maintained_by(current_user)
    respond_with @jobs, options
  end

  def show
    respond_with @job, options(serializer: Recruiters::JobSerializer , log: true)
  end

  def new
    @job = Job.new
    respond_with @job, options(log: true)
  end

  def create
    @job = Job.create(job_params)
    respond_with @job, options(log: true)
  end

  def update
    @job.update(job_params)
    respond_with @job, options(log: true)
  end

  def destroy
    @job.destroy
    respond_with @job, options(log: true)
  end

  def validate
    @job = Job.new(job_params)
    @job.valid?
    respond_with @job, options(log: true)
  end

  private
    def set_job
      @job = Job.find(params[:id])
    end

    def job_params
      params[:job].permit(:company_id, :recruiter_id, :assigned_recruiter_id, :language_id, :status,
        :name, :code, :reference, :contact_details, :tag_list, accepted_language_ids: [])
    end
end
