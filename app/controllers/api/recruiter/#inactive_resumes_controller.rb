class Api::Recruiter::ResumesController < Api::ApplicationController
  include RecruiterApi

  # authorize_resource
  before_action :set_resume, only: [:show]

  def show
    respond_with @resume, options(log: true, serializer: Recruiters::ResumeSerializer)
  end

  private
    def set_resume
      @resume = Resume.find(params[:id])
    end
end
