module Api
  module Recruiter
    module Auth
      class PasswordsController < DeviseTokenAuth::PasswordsController
        def update
          LogWrapper.new self, @resource, request, {}.merge(log: { priority: :high, user: @resource })
          if @resource.valid_password?(params[:current_password])
            # make sure user is authorized
            unless @resource
              return render json: {
                success: false,
                errors: ['Unauthorized']
              }, status: 401
            end

            # make sure account doesn't use oauth2 provider
            unless @resource.provider == 'email'
              return render json: {
                success: false,
                errors: ["This account does not require a password. Sign in using "+
                         "your #{@resource.provider.humanize} account instead."]
              }, status: 422
            end

            # ensure that password params were sent
            unless password_resource_params[:password] and password_resource_params[:password_confirmation]
              return render json: {
                success: false,
                errors: ['You must fill out the fields labeled "password" and "password confirmation".']
              }, status: 422
            end
            if @resource.update_attributes(password_resource_params)
              return render json: {
                success: true,
                data: {
                  user: @resource.token_validation_response,
                  message: "Your password has been successfully updated."
                }
              }
            else
              return render json: {
                success: false,
                # errors: @resource.errors.to_hash.merge(full_messages: @resource.errors.full_messages)
                errors: ['talent.ui.account.password.invalid_length']
              }, status: 422
            end
          else
            return render json: {
              success: false,
              errors: ['recruiter.ui.settings.password.wrong']
            }, status: 422
          end
        end
        def password_resource_params
          params.permit(devise_parameter_sanitizer.for(:account_update))
        end

        def resource_params
          params.permit(:email, :password, :password_confirmation, :reset_password_token)
        end
      end
    end
  end
end