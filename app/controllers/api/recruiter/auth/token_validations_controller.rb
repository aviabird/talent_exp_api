module Api
  module Recruiter
    module Auth
      class TokenValidationsController < DeviseTokenAuth::TokenValidationsController
        ###
        # No authorization required, override devise controller
        ###
      end
    end
  end
end
