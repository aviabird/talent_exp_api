module Api
  module Recruiter
    module Auth
      class SessionsController < DeviseTokenAuth::SessionsController
        respond_to :json
        before_filter :set_default_response_format

        ###
        # No authorization required, override devise controller
        ###

        def create
          LogWrapper.new self, @resource, request, {log: { priority: :medium, user: @resource }}
          super
        end

        def destroy
          LogWrapper.new self, @resource, request, {log: { priority: :medium, user: @resource }}
          super
        end

        private
          def set_default_response_format
            request.format = :json
          end

      end
    end
  end
end
