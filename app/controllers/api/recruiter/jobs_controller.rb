class Api::Recruiter::JobsController < Api::ApplicationController
  include RecruiterApi
  
  authorize_resource

  def index
    jobs = current_company.jobs.present? ? current_company.jobs.where(unsolicited_job: false) : []
    respond_with jobs, options(log: true)
  end

end