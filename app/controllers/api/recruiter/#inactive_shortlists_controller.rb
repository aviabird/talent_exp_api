class Api::Recruiter::ShortlistsController < Api::ApplicationController
  include RecruiterApi

  def index
    @talents = current_recruiter.talents_on_shortlist
    respond_with @talents, options(log: true, each_serializer: Recruiters::TalentsOnShortlistSerializer)
  end
end
