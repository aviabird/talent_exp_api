class Api::Recruiter::TalentsController < Api::ApplicationController
  include RecruiterApi

  # load_and_authorize_resource
  before_action :set_talent, only: [:show]

  def index
    # generate search query hash with possible search options
    search_params ={}
    search_params = params[:search].slice(:working_locations, :working_industries, :availability, :prefered_employment_types, :job_seeker_statuses, :sort, :salary_min, :salary_max, :skills, :languages, :min_work_experience_duration, :max_work_experience_duration, :experience_in_industries, :job_titles, :current_job, :jobs, :important_experiance_in_industries, :important_skills, :important_salary, :important_job_seeker_status, :important_prefered_employment_types, :important_availability, :important_working_industries, :important_working_locations, :important_languages, :important_work_experience_duration, :important_job_titles) if params[:search].present?
    search_params = search_params.merge({query: params[:search][:query]}) if params[:search] && params[:search][:query]
    search_params.delete(:company_scope)
    search_params.delete("company_scope")
    search_params.merge!(read_all: current_recruiter.read_all)
    # skip company_scope when read_all flag is true
    search_params = search_params.merge({company_scope: current_recruiter.company_id.to_s}) unless current_recruiter.read_all
  #  search_params = search_params.merge({company_scope: '52487d57-958c-4906-b95b-2ab0c1cd7f9c'})
  #  c = Company.find('52487d57-958c-4906-b95b-2ab0c1cd7f9c')
    @search = TalentSearch.new(search_params).search
    # @total = @search.total_count
    @aggregations = @search.aggs
    @resume_sharings = @search.only(:id).load(resume: {scope: ResumeSharing.includes(:resume)})
    @talents = @resume_sharings.map(&:resume).uniq
    @total = @talents.size
    results =[]
    @close_total, @close_total_page = 0, 0
    # add exact match to results
    exact_total_pages = pagination_pages(@total)
    results << @talents
    if any_filter?
      # perform close query 
      @close_search = TalentFuzzySearch.new(search_params).search
      if @close_search.present?
        @close_aggregations = @close_search.aggs
        @close_resume_sharings = @close_search.only(:id).load(resume: {scope: ResumeSharing.includes(:resume)})
        @close_talents = @close_resume_sharings.map(&:resume).uniq
        # add close match to results
        results << @close_talents
        @close_total = (@close_talents - @talents).size # 0
        @close_total_page = pagination_pages(@close_total)
      end
    end
    total = @talents.size + @close_total
    total_pages = pagination_pages(total)
    meta = {meta: {per: @options[:per], page: @options[:page], exact_total: @total, exact_total_pages: exact_total_pages }}
    #total_pages = @resume_sharings.total_pages
    meta[:meta].merge!({ close_total: @close_total, close_total_pages: @close_total_page, total: total })
    meta[:meta].merge!(total_pages: total_pages)
    meta[:meta].merge!({aggregations: @aggregations})
    respond_with Kaminari.paginate_array(results.flatten.uniq).page(params[:page]).per(@options[:per]), options(each_serializer: TalentsSerializer,  current_company: current_recruiter.company,
                                   log: true
                                   ).merge!(meta)

  # rescue Elasticsearch::Transport::Transport::Errors::BadRequest => e
  #   @talents = []
  #   @error = e.message.match(/QueryParsingException\[([^;]+)\]/).try(:[], 1)
  #   respond_with @talents, options(each_serializer: Recruiters::TalentsSerializer, log: true)
  end


  #def show
  #  respond_with @talent, options(log: true, serializer: Recruiters::TalentSerializer)
  #end
  def update_talents_rating 
    company_info = current_company.talent_infos.where(talent_id: params[:talent_id]).first
    if company_info
      value = company_info.update_attributes(rating: params[:rating_value]) if params[:rating_value]
    else
     value=current_company.talent_infos.create(talent_id: params[:talent_id], rating: params[:rating_value]) if params[:rating_value]
    end
    if value
      return render json: {
                  success: true,
                }, status: 200
    else
       return render json: {
                  success: false,
                }, status: 422
    end
  end

  private
  def set_talent
    @talent = ::Talent.find(params[:id])
  end

  def available_search_filter
    ["availability", "languages", "terms_of_employment", "salary_min", "salary_max", "skills", "job_seeker_statuses", "working_locations", "min_work_experience_duration", "max_work_experience_duration", "experience_in_industries", "job_titles", "working_industries", "important_working_industries" ,"important_experience_in_industries", "important_availability", "important_languages", "important_salary", "important_skills", "important_work_experience_duration", "important_job_titles"]
  end

  def any_filter?
    params[:search].keys.any? {|key| available_search_filter.include?(key)} if params[:search].present?
  end

  def pagination_pages total_results
    # this return number of pages
    pages = total_results / @options[:per]
    pages = (total_results % @options[:per] > 0) ? pages + 1 : pages
  end
end
