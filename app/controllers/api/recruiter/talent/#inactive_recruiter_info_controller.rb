class Api::Recruiter::Talent::RecruiterInfoController < Api::ApplicationController
  include RecruiterApi

  # authorize_resource :talent

  before_action :set_recruiter_info, only: [:show, :update]

  def show
    respond_with @recruiter_info, options
  end

  def update
    @recruiter_info.update recruiter_info_params
    respond_with @recruiter_info, options(log: { priority: :medium })
  end

  private
    def set_recruiter_info
      @recruiter_info = RecruiterTalentInfo.find(params[:id])
    end

    def recruiter_info_params
      params[:recruiter_info].permit(:followed_at, :hidden_at, :in_pool_since)
    end
end
