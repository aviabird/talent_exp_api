class Api::Recruiter::Talent::CompanyInfoController < Api::ApplicationController
  include RecruiterApi

  # authorize_resource :talent

  before_action :set_company_info, only: [:show, :update]

  def show
    respond_with @company_info, options
  end

  def update
    @company_info.update company_info_params
    respond_with @company_info, options(log: { priority: :medium })
  end

  private
    def set_company_info
      @company_info = CompanyTalentInfo.find(params[:id])
    end

    def company_info_params
      params[:company_talent_info].permit(:rating)
    end
end
