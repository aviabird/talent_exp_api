class Api::Recruiter::JobappsController < Api::ApplicationController
  include RecruiterApi

  authorize_resource
  before_action :set_jobapp, only: [:show, :edit, :update, :destroy, :withdrawn]

  def index
    # if params[:search].present?
    #   search_params = JSON.parse params[:search]
    # else
    #   search_params = {}
    # end

    # @search = JobappSearch.new(search_params)

    # @search_scope = @search.search.page(params[:page])

    # @aggregations = @search_scope.aggregations

    # @jobapps = @search_scope.load(
    #     jobapp: {scope: Jobapp.includes(resume: [
    #                                         :languages,
    #                                         :work_experiences,
    #                                         :educations,
    #                                         :languages,
    #                                         :skills,
    #                                         :link,
    #                                         :continued_educations,
    #                                         talent: [
    #                                             :recruiter_infos,
    #                                             :company_infos
    #                                         ]
    #                                     ])}).to_a

    @jobapps = Jobapp.where(job: params[:job_id])
    # respond_with @jobapps, options(each_serializer: Recruiters::JobappsSerializer ,log: true, meta: {aggregations: @aggregations})
    respond_with @jobapps, options(each_serializer: Recruiters::JobappsSerializer ,log: true)

  # rescue Elasticsearch::Transport::Transport::Errors::BadRequest => e
  #   @jobapps = []
  #   @error = e.message.match(/QueryParsingException\[([^;]+)\]/).try(:[], 1)
  #   respond_with @jobapps, options(each_serializer: Recruiters::JobappsSerializer, log: true)
  end

  def show
    respond_with @jobapp, options(serializer: Recruiters::JobappSerializer, log: true)
  end

  def update
    @jobapp.update(jobapp_params)
    respond_with @jobapp, options(log: { priority: :medium })
  end

  def destroy
    @jobapp.destroy
    respond_with @jobapp, options(log: { priority: :high })
  end

  private
    def set_jobapp
      @jobapp = Jobapp.find(params[:id])
    end

    def jobapp_params
      params[:jobapp].permit(:resume_id, :job_id, :job_type, :cover_letter, :contact_preferences, :blocked_companies, :recruiter_rating, :recruiter_note, :talent_info_status, 
        :read_at, :application_referrer_url, :allow_recruiter_contact_sharing, :withdrawn_reason, :terms, :hidden_at, :source,
        job_attributes: [ :company_name, :jobname, :recruiter_email ])
    end

    def job
      Job.find(params[:job_id]) if params[:job_id]
    end
end
