class Api::Recruiter::RecruitersController < Api::ApplicationController
  include RecruiterApi

  #load_and_authorize_resource
  before_action :set_recruiter

  def show
    respond_with @recruiter, options(log: true)
  end

  def update_locale
    if @recruiter
      @recruiter.update_attribute(:locale, recruiter_params[:locale])
    end
    respond_with @recruiter, @options.merge(log: true)
  end

=begin
  def update
    @recruiter.update recruiter_params
    respond_with @recruiter, options(log: { priority: :medium })
  end

  def avatar
    path = Rails.root.join('private', 'uploads', 'recruiter', 'avatar', "#{params[:basename]}.#{params[:extension]}")
    send_file path, :x_sendfile=>true
  end

  def avatar_upload
    @recruiter.update(avatar_params)
    respond_with @recruiter, options(serializer: RecruiterAvatarSerializer, log: { priority: :medium })
  end

  def validate
    @recruiter.assign_attributes(recruiter_params)
    @recruiter.valid?
    respond_with @recruiter, options(log: { priority: :medium })
  end
=end
  private
    def set_recruiter
      @recruiter = current_recruiter
    end

    def recruiter_params
      params[:recruiter].permit(:company_id, :email, :first_name, :last_name, :phone_number, :deleted_at, :locale)
    end

    def avatar_params
      params[:recruiter].permit(:avatar)
    end
end
