module Api
  class ApplicationController < ::ApplicationController
    include DeviseTokenAuth::Concerns::SetUserByToken

    ###
    # No authorization required, top level API controller accessible publicly
    ###

    respond_to :json
    self.responder = JsonResponder

    before_action :set_options, :set_paging, :set_locale, :check_myveeta_verification_token

    rescue_from CanCan::AccessDenied do |exception|
      render json: { error: exception.message, meta: { error: { errors: { authorization: [ exception.message ] } , messages: [ exception.message ] }}}, status: 403
      log_event self, params, request, {log: { priority: :high }}
      Rails.logger.warn("Authorization error: #{params}")
    end

    rescue_from ActiveRecord::RecordNotFound do
      render json: { error: 'Record not found', meta: { error: { errors: { record: [ 'not found' ] } , messages: [ 'Record not found' ] }}}, status: 404
      log_event self, params, request, {log: { priority: :high }}
      Rails.logger.warn("Not found error: #{params}")
    end

    # Set options by mergin @options with additional info
    def options opt = {}
      @options.merge(opt)
    end

    private
    def set_options
      @options = {}
    end


    def set_paging
      @options[:page] = params[:page] ? params[:page] : 1 #set 1 as default page if not given
      @options[:per] = params[:per] ? [params[:per],ENV['PAGINATION_MAX_PAGE_SIZE']].map(&:to_i).min : Kaminari.config.default_per_page
    end

    protected

      # check for verification token https://talentsolutions.atlassian.net/wiki/pages/editpage.action?pageId=30605331
      def check_myveeta_verification_token
        unless ENV['MYVEETA_VERIFICATION_TOKENS'].split(",").include?(request.headers['myveeta-verification-token'])
          log_event self, params, request, {log: {details: "Unknown client tried to access Intapi with myveeta-verification-token: #{request.headers['myveeta-verification-token']}", priority: :high , notify_im: true}}
          render json: { error: "Unknown client" }, status: 403
        end
      end

      def set_locale

        if request.headers['x-locale'] == 'xx'
          request.headers['x-locale'] = nil
          if !request.env['HTTP_ACCEPT_LANGUAGE'].nil?
            request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
          else
            :de  #default = de
          end
        end
        I18n.locale = request.headers['x-locale'] || (current_user.locale if current_user && current_user.respond_to?(:locale)) || I18n.default_locale
      end
  end
end
