class ApplicationController < ActionController::Base
  include DeviseTokenAuth::Concerns::SetUserByToken
  include BaseLogger

  ###
  # No authorization required, base controller
  ###

  serialization_scope :current_user

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception

  # https://github.com/gonzalo-bulnes/simple_token_authentication/issues/67
  # http://jamescpoole.com/2013/10/31/rails-4-csrf-protection-with-clients-using-apis/
  protect_from_forgery with: :null_session

  # Helper method to log
  def log key, args = {}
    LogService.call(key, args)
  end


  def current_user
    return current_recruiter_auth.user if current_recruiter_auth
  end

  def current_recruiter_auth
    current_api_recruiter_recruiter_auth
  end

  def recruiter_auth_signed_in?
    current_api_recruiter_recruiter_auth_signed_in?
  end

  def authenticate_recruiter_auth!
    authenticate_api_recruiter_recruiter_auth!
  end

  # Always try to get email from url
  def current_email
    params[:email] || params[:user_email] || current_user.try(:email)
  end
end
