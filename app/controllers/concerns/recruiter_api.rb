module RecruiterApi
  extend ActiveSupport::Concern

  included do

    before_action :authenticate_recruiter_auth!
    include DeviseTokenAuth::Concerns::SetUserByToken

    alias_method :current_user, :current_recruiter_auth
    alias_method :user_signed_in?, :recruiter_auth_signed_in?

    acts_as_token_authentication_handler_for RecruiterAuth, fallback_to_devise: false

  end

  def current_recruiter
    current_user.recruiter if current_user
  end

  def current_ability
    @current_ability ||= Ability::RecruiterAbility.new(current_recruiter)
  end

  def current_company
    current_recruiter.company
  end

end
