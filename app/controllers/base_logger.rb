module BaseLogger
  def log_event controller, resource, request, options={}
    LogWrapper.new controller, resource, request, options
  end
end
