#!/bin/bash
# Use this script to deploy your application onto a newer buildpack with zero downtime

if [ "$#" -ne 4 ]
then
  echo "Usage: app-deploy-with-rename.sh originalAppName backupAppName fullyQualifiedPathToNewApp manifestName"
  echo ""
  echo "This script will rename originalAppName to backupAppName. "\
       "Then it will push the artifact specified on the third argument"\
       "using the attributes of originalAppName via a manifest.yml file."
  echo " "
  echo "NOTE 1: You must be logged in to Cloud Foundry to run this command."
  echo "NOTE 2: The minimum version supported of cf command line client is 6.1.1."
  echo "NOTE 3: To capture output use:"
  echo "             app-deploy-with-rename.sh arg1 arg2 arg3 arg4 2>&1 | tee results.txt"
  exit 1
fi

if ! cf --version | awk '{print $3}' | grep -q -e "^6\.";
  then echo "cf version must be 6"; exit 1;
fi

echo "originalAppName..[$1]"
echo "backupAppName....[$2]"
echo "pathToApp........[$3]"
echo "manifestName........[$4]"

#CF_COLOR=false gets rid of hidden chars in output
export CF_COLOR=false

if ! cf app "$1" >&/dev/null;
  then echo "originalAppName $1 does not exist"; exit 1;
fi

if cf app "$2">&/dev/null;
  then echo "backupAppName $2 already exists"; exit 1;
fi

if [ ! -e $3 ];
  then echo "Path $3 does not exist"; exit 1;
fi

echo " "

echo "Do you want to rename $1 to $2 and push $1 using $4? [y/n]"
read
if [ $REPLY = 'y' ];
then
  cf rename $1 $2
  cf push -f $4 --no-start
else
  echo "Not renaming or pushing $1, exiting."
  exit
fi

#get host from manifest
STR=`cat $4 | grep 'host:'`
IFS=': ' read -ra HOST <<< "$STR"
echo ${HOST[1]}

#get domain from manifest
STR=`cat $4 | grep 'domain:'`
IFS=': ' read -ra DOMAIN <<< "$STR"
echo ${DOMAIN[1]}


echo "cf map-route $1 ${DOMAIN[1]} -n ${HOST[1]}"
cf map-route $1 ${DOMAIN[1]} -n ${HOST[1]}

#for host in $hostArray
#do
#  cf map-route $1 'myveeta.com' -n 'node2'
#done

cf start $1

echo "Horizontal scaling - increasing number of instances to 2..."
cf scale $1 -i 2

echo " "
cf apps

echo "Do you want to delete $2? If not, the instance is just stopped. [y/n]"
read
if [ $REPLY = 'y' ];
then
  cf delete $2 -f
  cf apps
else
  echo "Not deleting $2, but stopping it"
  cf stop $2
fi
