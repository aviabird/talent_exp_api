# == Schema Information
#
# Table name: jobs
#
#  id                    :uuid             not null, primary key
#  company_id            :uuid             not null
#  recruiter_id          :uuid
#  assigned_recruiter_id :uuid
#  language_id           :integer          not null
#  status                :string
#  name                  :string           not null
#  code                  :string
#  reference             :string
#  contact_details       :text
#  deleted_at            :datetime
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  ext_id                :string
#
# Indexes
#
#  index_jobs_on_company_id    (company_id)
#  index_jobs_on_created_at    (created_at)
#  index_jobs_on_ext_id        (ext_id)
#  index_jobs_on_name          (name)
#  index_jobs_on_recruiter_id  (recruiter_id)
#  index_jobs_on_status        (status)
#

require 'test_helper'

describe Job do
  let(:job) { build_stubbed(:job) }

  belong_to(:company)
  belong_to(:recruiter)
  belong_to(:assigned_recruiter)
  belong_to(:language)
  have_many(:jobapps)
  have_and_belong_to_many(:accepted_jobs_languages)

  validate_presence_of(:name)
  validate_presence_of(:company)
  validate_presence_of(:language)
  validate_presence_of(:accepted_jobs_languages)

  have_db_index(:company_id)
  have_db_index(:recruiter_id)
  have_db_index(:status)
  have_db_index(:name)

  it "must be valid" do
    job.must_be :valid?
  end

  it "must figure out if job is closed" do
    job.status = 'something'
    job.wont_be :closed?
    job.status = 'closed'
    job.must_be :closed?
  end

  it "must figure out if is deleted" do
    job.deleted_at = nil
    job.wont_be :deleted?
    job.deleted_at = 2.days.ago
    job.must_be :deleted?
  end

  it "must be available if is not closed or deleted" do
    job.stubs(:closed?).returns(false)
    job.stubs(:deleted?).returns(false)
    job.must_be :available?
  end

  it "can't be available if is closed or deleted" do
    job.stubs(:closed?).returns(true)
    job.stubs(:deleted?).returns(false)
    job.wont_be :available?
  end
end
