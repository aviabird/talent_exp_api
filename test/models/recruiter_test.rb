# == Schema Information
#
# Table name: recruiters
#
#  id           :uuid             not null, primary key
#  company_id   :uuid
#  email        :string           not null
#  first_name   :string
#  last_name    :string           not null
#  phone_number :string
#  deleted_at   :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_recruiters_on_company_id  (company_id)
#  index_recruiters_on_created_at  (created_at)
#  index_recruiters_on_deleted_at  (deleted_at)
#  index_recruiters_on_email       (email)
#  index_recruiters_on_last_name   (last_name)
#

require "test_helper"

describe Recruiter do
  let(:recruiter) { build_stubbed(:recruiter, auth: build_stubbed(:recruiter_auth)) }
  let(:company) { create(:company) }

  belong_to :company
  have_many :talent_infos
  have_one  :setting
  have_many :comments
  have_many :shortlists
  have_many :talents_on_shortlist

  validate_presence_of(:company)
  validate_presence_of(:email)
  validate_presence_of(:last_name)

  have_db_index(:company_id)
  have_db_index(:email)
  have_db_index(:last_name)
  have_db_index(:deleted_at)

  it "must be valid" do
    recruiter.must_be :valid?
  end

  it "must have some IP for recruiter" do
    recruiter.ip.must_equal '10.0.0.1'
  end

  it "must have defined name" do
    recruiter.name.must_equal "#{recruiter.first_name} #{recruiter.last_name}"
  end

  it "should be recruiter" do
    recruiter.must_be :recruiter?
  end

  it "wont be talent or admin" do
    recruiter.wont_be :talent?
    recruiter.wont_be :admin?
  end

  it "must have right ability" do
    recruiter.ability.must_be_instance_of Ability::RecruiterAbility
  end
end
