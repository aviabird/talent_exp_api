# == Schema Information
#
# Table name: resumes
#
#  id                       :uuid             not null, primary key
#  talent_id                :uuid             not null
#  language_id              :integer          not null
#  name                     :string           not null
#  deleted_at               :datetime
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  work_experience_duration :float
#  origin_id                :uuid
#  version                  :integer          default(1)
#  origin_updated_at        :datetime
#  pdf                      :string
#  token                    :string
#
# Indexes
#
#  index_resumes_on_created_at   (created_at)
#  index_resumes_on_deleted_at   (deleted_at)
#  index_resumes_on_language_id  (language_id)
#  index_resumes_on_name         (name)
#  index_resumes_on_talent_id    (talent_id)
#

require "test_helper"

describe Resume do
  let(:resume) { build_stubbed(:resume) }

  belong_to(:talent)
  belong_to(:origin)
  have_many(:jobapps)
  have_many(:pdfs)
  have_many(:work_experiences)
  have_many(:educations)
  have_many(:languages)
  have_many(:skills)
  have_one(:link)
  have_many(:documents)
  have_many(:memberships)
  have_many(:projects)
  have_many(:awards)
  have_one(:about)
  have_many(:publications)

  validate_presence_of(:name).on(:update)

  have_db_index(:name)
  have_db_index(:deleted_at)

  it "must be valid" do
    resume.must_be :valid?
  end

  it "cant have image snapshot url" do
    resume.image_snapshot_url.must_equal nil
  end

  it "must return empty array, when is no application on resume" do
    resume.shared_with.must_be_kind_of Array
    resume.shared_with.must_equal []
  end

  it "should look for companies" do
    resume.expects(:list_companies)
    resume.companies
  end

  it "must have shared companies" do
    company = build_stubbed(:company)
    resume.stubs(:jobapps).returns([build_stubbed(:jobapp, job: build_stubbed(:job, company: build_stubbed(:company))), build_stubbed(:jobapp, job: build_stubbed(:job, company: company))])
    resume.stubs(:sharings).returns([build_stubbed(:resume_sharing, company: build_stubbed(:company)), build_stubbed(:resume_sharing, company: company)])
    resume.send(:list_companies).first.must_be_instance_of Company
    resume.send(:list_companies).count.must_equal 3
  end

  it "should have default sequence" do
    resume.sequence.must_equal 1
  end

  # it "should be prefilled from talent info" do
  #   resume.email = nil
  #   resume.phone_number = nil
  #   resume.__send__(:prefill)
  #   resume.email.must_equal resume.talent.email
  #   resume.phone_number.must_equal resume.talent.phone_number
  # end

  # it "shouln't prefille anything, when talent is not set" do
  #   resume.stubs(:talent)
  #   resume.email = nil
  #   resume.phone_number = nil
  #   resume.__send__(:prefill)
  #   resume.email.must_equal nil
  #   resume.phone_number.must_equal nil
  # end

  it "should know how to prepare empty resume" do
    Resume.any_instance.expects(:save)
    Resume.prepare(build_stubbed(:talent)).must_be_instance_of Resume
  end

  it "should determinate and give dummy data" do
    talent = build_stubbed(:talent)
    talent.jobapps.stubs(:empty?).returns(true)

    Resume.expects(:dummy)
    Resume.my_or_dummy(talent)
  end

  it "should determinate and give users data" do
    talent = build_stubbed(:talent)
    talent.resumes.stubs(:empty?).returns(false)

    Resume.expects(:my).with(talent)
    Resume.my_or_dummy(talent)
  end

  it "should try to fill out" do
    resume_pdf = build_stubbed(:resume_pdf)
    ResumeService::PrefillPDF.expects(:call)#.with(resume, resume_pdf)
    resume.expects(:clean)
    resume.fillin(resume_pdf)
  end

  it "calculate working duraction" do
    resume.stubs(:work_experiences).returns([build_stubbed(:rs_work_experience)])
    WorkExperienceCalculator.expects(:working_duration).returns(1)
    resume.work_experience_duration
  end

  it "calculate international working duraction" do
    resume.stubs(:work_experiences).returns([build_stubbed(:rs_work_experience)])
    WorkExperienceCalculator.expects(:international_duration).returns({})
    resume.international_work_experience_durations
  end

  it "generate array of work experiences" do
    work_experiences = [build_stubbed(:rs_work_experience, industry: 'AgriFish'), build_stubbed(:rs_work_experience, industry: 'MiningFuel')]
    resume.stubs(:work_experiences).returns(work_experiences)
    resume.work_industries.must_equal(['AgriFish', 'MiningFuel'])
  end

  it "must map documents urls" do
    document1 = build_stubbed(:rs_document)
    document1.stubs(:url).returns('http://localhost:3000/myfile.pdf')
    document1.stubs(:save).returns(document1)
    document2 = build_stubbed(:rs_document)
    document2.stubs(:url).returns('http://localhost:3000/myfile2.pdf')
    document2.stubs(:save).returns(document2)
    resume.documents << document1
    resume.documents << document2
    resume.document_urls.must_equal ['http://localhost:3000/myfile.pdf', 'http://localhost:3000/myfile2.pdf']
  end

  it "should use share service for sharing with companies" do
    Resume::Share.expects(:call).with(resume, [1, 2], true)
    resume.share(company_ids: [1, 2])
  end

  it "should use share service for sharing with email companies" do
    Resume::Share.expects(:call).with(resume, [1, 2], false)
    resume.share(company_ids: [1, 2],real_company: false)
  end
end
