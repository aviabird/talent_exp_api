# == Schema Information
#
# Table name: talent_auths
#
#  id                              :uuid             not null, primary key
#  email                           :string           default(""), not null
#  encrypted_password              :string           default(""), not null
#  reset_password_token            :string
#  reset_password_sent_at          :datetime
#  remember_created_at             :datetime
#  sign_in_count                   :integer          default(0), not null
#  current_sign_in_at              :datetime
#  last_sign_in_at                 :datetime
#  current_sign_in_ip              :string
#  last_sign_in_ip                 :string
#  confirmation_token              :string
#  confirmed_at                    :datetime
#  confirmation_sent_at            :datetime
#  unconfirmed_email               :string
#  tokens                          :text
#  provider                        :string
#  uid                             :string           default(""), not null
#  guest                           :boolean          default(FALSE)
#  talent_id                       :uuid             not null
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#  authentication_token            :string
#  authentication_token_created_at :datetime
#  xing_request_token              :string
#  xing_request_token_secret       :string
#  xing_access_token               :string
#  xing_access_token_secret        :string
#  linkedin_request_token          :string
#  linkedin_request_token_secret   :string
#  linkedin_access_token           :string
#  linkedin_access_token_secret    :string
#  failed_attempts                 :integer          default(0)
#  unlock_token                    :string
#  locked_at                       :datetime
#
# Indexes
#
#  index_talent_auths_on_authentication_token  (authentication_token)
#  index_talent_auths_on_confirmation_token    (confirmation_token) UNIQUE
#  index_talent_auths_on_created_at            (created_at)
#  index_talent_auths_on_email                 (email) UNIQUE
#  index_talent_auths_on_reset_password_token  (reset_password_token) UNIQUE
#  index_talent_auths_on_talent_id             (talent_id) UNIQUE
#  index_talent_auths_on_unlock_token          (unlock_token) UNIQUE
#

require "test_helper"

describe TalentAuth do
  let(:talent_auth) { build_stubbed(:talent_auth, is_guest: true) }

  belong_to(:talent)

  validate_presence_of(:email)
  validate_presence_of(:password)
  validate_presence_of(:country_id)
  validate_confirmation_of(:password)
  validate_presence_of(:resume_type)
  validate_uniqueness_of(:email)

  have_db_index(:talent_id)
  have_db_index(:email)

  it "must be valid" do
    talent_auth.must_be :valid?
  end

  it "must set talent before is record created" do
    auth = build(:talent_auth, talent_id: 1, is_guest: true)
    auth.expects(:set_talent)
    auth.confirm!
    auth.save
  end

  it "must invoke talent initialization" do
    Talent.expects(:init)
    talent_auth.__send__(:set_talent)
  end

  it "should set if is guest" do
    talent_auth.stubs(:is_guest).returns('true')
    talent_auth.expects(:skip_confirmation_notification!)
    talent_auth.send(:finish)
    talent_auth.must_be :guest?
  end

  it "should send email after while" do
    Sidekiq::Testing.fake! do
      Sidekiq::Worker.clear_all
      Sidekiq::Extensions::DelayedModel.jobs.must_be :empty?
      talent_auth.send(:trigger_email)
      Sidekiq::Extensions::DelayedModel.jobs.wont_be :empty?
    end
  end

  it "should not invoke sending email if user is not guest" do
    talent_auth.guest = false
    AuthMailer.expects(:continue_applying).never
    talent_auth.send_continue_applying_email
  end

  it "should not invoke sending email if user is not guest" do
    talent_auth.guest = true
    mailer = mock('mailer')
    mailer.stubs(:deliver)
    AuthMailer.expects(:continue_applying).once.returns(mailer)
    talent_auth.send_continue_applying_email
  end

  it "must know which kind of user is used" do
    talent_auth.respond_to?(:talent?).must_equal true
    talent_auth.respond_to?(:recruiter?).must_equal true
    talent_auth.respond_to?(:admin?).must_equal true
  end

  it "won't be valid without first name and last name" do
    skip("find better way to test validation with context")
    talent_auth = build_stubbed(:talent_auth, is_guest: false)
    # talent_auth.valid?
    talent_auth.wont_be :valid?
  end

  it "won't be valid with wrong names" do
    talent_auth = build_stubbed(:talent_auth, is_guest: true, first_name: 'J!', last_name: 'K!')
    talent_auth.wont_be :valid?
    talent_auth.errors.messages[:first_name].wont_be :empty?
    talent_auth.errors.messages[:last_name].wont_be :empty?
  end

  it "needs to be ok, with right first name and last name" do
    talent_auth = build_stubbed(:talent_auth, is_guest: true, first_name: 'Jiří', last_name: "Doe's-")
    talent_auth.must_be :valid?
  end

  describe 'serialization' do
    it 'hash should not include sensitive info' do
      talent_auth = TalentAuth.new
      refute talent_auth.as_json[:tokens]
    end
  end
end
