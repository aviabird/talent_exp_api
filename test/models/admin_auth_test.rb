# == Schema Information
#
# Table name: admin_auths
#
#  id                     :uuid             not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  admin_id               :uuid             not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  failed_attempts        :integer          default(0)
#  unlock_token           :string
#  locked_at              :datetime
#
# Indexes
#
#  index_admin_auths_on_created_at            (created_at)
#  index_admin_auths_on_email                 (email) UNIQUE
#  index_admin_auths_on_reset_password_token  (reset_password_token) UNIQUE
#  index_admin_auths_on_unlock_token          (unlock_token) UNIQUE
#

require "test_helper"

describe AdminAuth do
  let(:admin_auth) { build_stubbed(:admin_auth) }

  belong_to(:admin)

  have_db_index(:email)

  validate_presence_of(:email)
  validate_presence_of(:password)
  validate_confirmation_of(:password)

  it "must be valid" do
    admin_auth.must_be :valid?
  end

  it "must know which kind of user is used" do
    admin_auth.respond_to?(:talent?).must_equal true
    admin_auth.respond_to?(:recruiter?).must_equal true
    admin_auth.respond_to?(:admin?).must_equal true
  end
end
