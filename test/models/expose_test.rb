# == Schema Information
#
# Table name: exposes
#
#  id         :uuid             not null, primary key
#  jobapp_id  :uuid             not null
#  content    :text
#  subject    :string           not null
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_exposes_on_created_at  (created_at)
#  index_exposes_on_deleted_at  (deleted_at)
#  index_exposes_on_jobapp_id   (jobapp_id)
#

require "test_helper"

describe Expose do
  let(:expose) { build_stubbed(:expose) }

  belong_to(:jobapp)
  validate_presence_of(:jobapp_id)
  validate_presence_of(:subject)

  have_db_index(:jobapp_id)

  it "must be valid" do
    expose.jobapp.stubs(:valid?).returns(true)
    expose.must_be :valid?
  end
end
