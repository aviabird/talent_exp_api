# == Schema Information
#
# Table name: rs_memberships
#
#  id           :uuid             not null, primary key
#  resume_id    :uuid
#  organization :string
#  role         :string
#  area         :string
#  from         :datetime
#  to           :datetime
#  created_at   :datetime
#  updated_at   :datetime
#
# Indexes
#
#  index_rs_memberships_on_created_at  (created_at)
#  index_rs_memberships_on_resume_id   (resume_id)
#

require "test_helper"

describe Rs::Membership do
  let(:membership) { build_stubbed(:rs_membership) }
  validate_presence_of(:organization)
  validate_presence_of(:role)

  it "must be valid" do
    membership.must_be :valid?
  end
end
