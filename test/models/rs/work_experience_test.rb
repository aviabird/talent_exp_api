# == Schema Information
#
# Table name: rs_work_experiences
#
#  id                  :uuid             not null, primary key
#  resume_id           :uuid             not null
#  position            :string
#  company             :string
#  industry            :string
#  job_level           :string
#  terms_of_employment :string
#  from                :datetime
#  to                  :datetime
#  country_id          :integer
#  description         :text
#  created_at          :datetime
#  updated_at          :datetime
#
# Indexes
#
#  index_rs_work_experiences_on_company     (company)
#  index_rs_work_experiences_on_country_id  (country_id)
#  index_rs_work_experiences_on_created_at  (created_at)
#  index_rs_work_experiences_on_from        (from)
#  index_rs_work_experiences_on_industry    (industry)
#  index_rs_work_experiences_on_position    (position)
#  index_rs_work_experiences_on_resume_id   (resume_id)
#  index_rs_work_experiences_on_to          (to)
#

require "test_helper"

describe Rs::WorkExperience do
  let(:work_experience) { build_stubbed(:rs_work_experience) }

  validate_presence_of(:position)
  validate_presence_of(:company)
  validate_presence_of(:industry)
  validate_presence_of(:job_level)
  validate_presence_of(:from)
  validate_presence_of(:country_id)
  validate_presence_of(:description)

  it "must be valid" do
    work_experience.must_be :valid?
  end

  it "must generate validated method" do
  	work_experience.to = nil
  	work_experience.to.must_be_nil
  	work_experience.validated_to.wont_be_nil
  end

  it "cannot be valid with to is earlier than from" do
  	work_experience.from = 2.years.ago
  	work_experience.to = 5.years.ago
  	work_experience.wont_be :valid?
  end

  it "could have nil as to" do
  	work_experience.to = nil
  	work_experience.must_be :valid?
  end

  it "should determinate if current is false" do
  	work_experience.to = DateTime.now
  	work_experience.current.must_equal false
  end

  it "should determinate if current is true" do
  	work_experience.to = nil
  	work_experience.current.must_equal true
  end

  it "must be parttime" do
    work_experience.terms_of_employment = 'part_time'
    work_experience.must_be :part_time?
  end
end
