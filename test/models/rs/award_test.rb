# == Schema Information
#
# Table name: rs_awards
#
#  id          :uuid             not null, primary key
#  resume_id   :uuid
#  name        :string
#  occupation  :string
#  awarded_by  :string
#  year        :datetime
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_rs_awards_on_created_at  (created_at)
#  index_rs_awards_on_resume_id   (resume_id)
#

require "test_helper"

describe Rs::Award do
  let(:award) { build_stubbed(:rs_award) }

  validate_presence_of(:name)
  validate_presence_of(:awarded_by)

  it "must be valid" do
    award.must_be :valid?
  end
end
