# == Schema Information
#
# Table name: rs_documents
#
#  id            :uuid             not null, primary key
#  resume_id     :uuid
#  file          :string
#  document_type :string
#  created_at    :datetime
#  updated_at    :datetime
#  name          :string
#  file_type     :string
#  token         :string
#
# Indexes
#
#  index_rs_documents_on_created_at  (created_at)
#  index_rs_documents_on_resume_id   (resume_id)
#  index_rs_documents_on_token       (token)
#

require "test_helper"

describe Rs::Document do
  let(:document) { build_stubbed(:rs_document) }

  it "must be valid" do
    document.must_be :valid?
  end
end
