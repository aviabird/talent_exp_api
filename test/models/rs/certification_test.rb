# == Schema Information
#
# Table name: rs_certifications
#
#  id                 :uuid             not null, primary key
#  resume_id          :uuid
#  subject            :string
#  organization       :string
#  description        :text
#  created_at         :datetime
#  updated_at         :datetime
#  certification_type :string
#  year               :integer
#
# Indexes
#
#  index_rs_certifications_on_created_at  (created_at)
#  index_rs_certifications_on_resume_id   (resume_id)
#

require "test_helper"

describe Rs::Certification do
  let(:certification) { build_stubbed(:rs_certification) }

  validate_presence_of(:subject)
  validate_presence_of(:organization)
  validate_presence_of(:year)

  it "must be valid" do
    certification.must_be :valid?
  end
end
