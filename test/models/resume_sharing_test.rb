# == Schema Information
#
# Table name: resume_sharings
#
#  id                   :integer          not null, primary key
#  resume_id            :uuid             not null
#  company_id           :uuid
#  recruiter_id         :uuid
#  access_revoked_at    :datetime
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  revoke_reason        :string
#  email_application_id :uuid
#
# Indexes
#
#  index_resume_sharings_on_access_revoked_at  (access_revoked_at)
#  index_resume_sharings_on_company_id         (company_id)
#  index_resume_sharings_on_recruiter_id       (recruiter_id)
#  index_resume_sharings_on_resume_id          (resume_id)
#

require "test_helper"

describe ResumeSharing do
  let(:resume_sharing) { build_stubbed(:resume_sharing)}

  let(:user) { build_stubbed(:talent) }

  should belong_to(:email_application)

  should validate_presence_of(:resume)

  it "must be valid" do
    resume_sharing.must_be :valid?
  end

  it "should update revoked at" do
    resume_sharing.expects(:update_columns).returns(true)
    resume_sharing.revoke
  end

  it "should get only unique sharings" do
    apple = build_stubbed(:company)
    google = build_stubbed(:company)
    origin_resume1 = build_stubbed(:resume)
    origin_resume2 = build_stubbed(:resume)
    versioned_resume1 = build_stubbed(:resume, origin: origin_resume1)
    versioned_resume2 = build_stubbed(:resume, origin: origin_resume2)
    versioned_resume3 = build_stubbed(:resume, origin: origin_resume1)
    lastday = []
    lastday << build_stubbed(:resume_sharing, company: apple, resume: versioned_resume1)
    lastday << build_stubbed(:resume_sharing, company: apple, resume: versioned_resume3)
    lastday << build_stubbed(:resume_sharing, company: google, resume: versioned_resume2)
    ResumeSharing.expects(:lastday).returns lastday
    ResumeSharing.ready_to_share.count.must_equal 2
  end

end
