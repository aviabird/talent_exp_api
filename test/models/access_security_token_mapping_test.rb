# == Schema Information
#
# Table name: access_security_token_mappings
#
#  id                   :uuid             not null, primary key
#  individual_key       :string
#  resource_type        :string
#  external_entity_type :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  resource_id          :uuid
#  external_entity_id   :uuid
#  deleted_at           :datetime
#
# Indexes
#
#  index_access_security_token_mappings_on_deleted_at            (deleted_at)
#  index_access_security_token_mappings_on_external_entity_type  (external_entity_type)
#  index_access_security_token_mappings_on_individual_key        (individual_key) UNIQUE
#  index_access_security_token_mappings_on_resource_type         (resource_type)
#

require 'test_helper'

class AccessSecurityTokenMappingTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
