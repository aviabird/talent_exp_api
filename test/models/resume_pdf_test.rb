# == Schema Information
#
# Table name: resume_pdfs
#
#  id         :integer          not null, primary key
#  resume_id  :uuid
#  file       :string
#  file_tmp   :string
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_resume_pdfs_on_resume_id  (resume_id)
#

require "test_helper"

describe ResumePdf do
  let(:resume_pdf) { build_stubbed(:resume_pdf) }

  belong_to(:resume)
  validate_presence_of(:resume)
  validate_presence_of(:file)

  it "must be valid" do
    resume_pdf.must_be :valid?
  end
end
