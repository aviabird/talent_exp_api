# == Schema Information
#
# Table name: temp_uploads
#
#  id         :integer          not null, primary key
#  type       :string
#  file       :string
#  file_meta  :string
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_temp_uploads_on_file  (file) UNIQUE
#

require "test_helper"

describe TempUpload::Avatar do
  let(:temp_upload_avatar) { create(:temp_upload_avatar) }

  have_db_index(:file)


  it "must be valid" do
    temp_upload_avatar.must_be :valid?
  end
end
