require "test_helper"

module Ability
  describe TalentAbility do
    describe 'Anuathorized access' do
      let(:ability) { TalentAbility.new(nil) }

      describe Talent do
        it "cannot show"        do ability.cannot?(:show, Talent.new).must_equal true end
        it "cannot update"      do ability.cannot?(:update, Talent.new).must_equal true end
        it "cannot validate"    do ability.cannot?(:validate, Talent.new).must_equal true end
      end

      describe TalentSetting do
        it "cannot show"        do ability.cannot?(:show, TalentSetting.new).must_equal true end
        it "cannot update"      do ability.cannot?(:update, TalentSetting.new).must_equal true end
        it "cannot validate"    do ability.cannot?(:validate, TalentSetting.new).must_equal true end
      end

      describe Jobapp do
        it "cannot create jobapp" do  ability.cannot?(:create, Jobapp.new).must_equal true end
        it "cannot new jobapp" do  ability.cannot?(:create, Jobapp.new).must_equal true end
      end

      describe Resume do
        it "cannot show resume" do ability.cannot?(:show, Resume.new).must_equal true end
        it "cannot new resume" do ability.cannot?(:new, Resume.new).must_equal true end
        it "cannot create resume" do ability.cannot?(:create, Resume.new).must_equal true end
        it "cannot destroy resume" do ability.cannot?(:destroy, Resume.new).must_equal true end
        it "cannot validate resume" do ability.cannot?(:validate, Resume.new).must_equal true end
      end

      describe ResumeTemplate do
        it "cannot show resume" do ability.cannot?(:index, ResumeTemplate).must_equal true end
      end
    end

    describe 'Talent access' do
      let(:current_talent) { FactoryGirl.build_stubbed(:talent) }
      let(:ability) { TalentAbility.new(current_talent) }

      before { current_talent.stubs(:id).returns(1) }

      describe 'on someone elses objects' do
        describe Talent do
          it "cannot show"        do ability.cannot?(:show, Talent.new).must_equal true end
          it "cannot update"      do ability.cannot?(:update, Talent.new).must_equal true end
          it "cannot validate"    do ability.cannot?(:validate, Talent.new).must_equal true end
        end

        describe TalentSetting do
          it "cannot show"        do ability.cannot?(:show, TalentSetting.new).must_equal true end
          it "cannot update"      do ability.cannot?(:update, TalentSetting.new).must_equal true end
          it "cannot validate"    do ability.cannot?(:validate, TalentSetting.new).must_equal true end
        end

        describe Resume do
          let(:resume) { FactoryGirl.build_stubbed(:resume) }
          let(:fourth_resume) { FactoryGirl.build_stubbed(:resume)}

          before do
            resume.stubs(:sequence).returns(1)
            fourth_resume.stubs(:sequence).returns(4)
          end

          it "can create"         do ability.can?(:create, resume).must_equal true end

          # He can create unlimited count of resumes now
          # it "cannot create 4"    do ability.cannot?(:create, fourth_resume).must_equal true end
        end
      end

      describe 'on his objects' do
        describe Talent do
          before { Talent.any_instance.stubs(:id).returns(1) }

          it "can show"        do ability.can?(:show, Talent.new).must_equal true end
          it "can update"      do ability.can?(:update, Talent.new).must_equal true end
          it "can validate"    do ability.can?(:validate, Talent.new).must_equal true end
        end

        describe TalentSetting do
          before { TalentSetting.any_instance.stubs(:talent_id).returns(1) }

          it "can show"        do ability.can?(:show, TalentSetting.new).must_equal true end
          it "can update"      do ability.can?(:update, TalentSetting.new).must_equal true end
          it "can validate"    do ability.can?(:validate, TalentSetting.new).must_equal true end
        end
      end

      describe 'independent on object' do
        describe Jobapp do
          it "can create jobapp" do ability.can?(:create, Jobapp).must_equal true end

          it "cannot create jobapp if not guest" do
            current_talent.stubs(:guest?).returns(true)
            ability.cannot?(:create, Jobapp).must_equal true
            ability.can?(:new, Jobapp).must_equal true
          end

          it "cannot create jobapp if not confirmed" do
            current_talent.stubs(:confirmed?).returns(false)
            ability.cannot?(:create, Jobapp).must_equal true
            ability.can?(:new, Jobapp).must_equal true
          end

          it "cannot create jobapp if not confirmed and guest" do
            current_talent.stubs(:guest?).returns(true)
            current_talent.stubs(:confirmed?).returns(false)
            ability.cannot?(:create, Jobapp).must_equal true
            ability.can?(:new, Jobapp).must_equal true
          end
        end

        describe ResumeTemplate do
          it "can index resume template" do ability.can?(:index, ResumeTemplate).must_equal true end
        end
      end
    end
  end
end
