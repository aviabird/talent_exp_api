# == Schema Information
#
# Table name: resume_snapshots
#
#  id         :uuid             not null, primary key
#  resume_id  :uuid             not null
#  xml_resume :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_resume_snapshots_on_created_at  (created_at)
#  index_resume_snapshots_on_resume_id   (resume_id)
#

require "test_helper"

describe ResumeSnapshot do
  let(:resume_snapshot) { build_stubbed(:resume_snapshot) }

  belong_to(:resume)

  validate_presence_of(:resume)

  have_db_index(:resume_id)

  it "must be valid" do
    resume_snapshot.must_be :valid?
  end
end
