# == Schema Information
#
# Table name: talents
#
#  id                      :uuid             not null, primary key
#  first_name              :string
#  last_name               :string
#  birthdate               :datetime
#  avatar                  :string
#  salutation              :string
#  avatar_meta             :text
#  nationality_country_id  :integer
#  title                   :string
#  email                   :string           not null
#  phone_number            :string
#  address_id              :uuid
#  veeta_terms_accepted_at :datetime
#  veeta_terms_accepted_ip :string
#  locale                  :string
#  deleted_at              :datetime
#  delete_reason           :string
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  ext_id                  :string
#  new                     :boolean          default(TRUE)
#  token                   :string
#  military_service        :string
#
# Indexes
#
#  index_talents_on_avatar                  (avatar)
#  index_talents_on_birthdate               (birthdate)
#  index_talents_on_created_at              (created_at)
#  index_talents_on_deleted_at              (deleted_at)
#  index_talents_on_email                   (email)
#  index_talents_on_first_name              (first_name)
#  index_talents_on_last_name               (last_name)
#  index_talents_on_nationality_country_id  (nationality_country_id)
#  index_talents_on_salutation              (salutation)
#  index_talents_on_token                   (token)
#

require "test_helper"

describe Talent do
  let(:talent) { build_stubbed(:talent) }

  have_one(:auth)
  have_one(:setting)
  have_many(:resumes)
  have_many(:jobapps)
  have_many(:jobs).through(:jobapps)
  belong_to(:nationality_country)
  belong_to(:address)
  have_many(:recruiter_comments)
  have_many :recruiter_infos
  have_many :company_infos
  have_and_belong_to_many :pools

  validate_presence_of(:setting)

  have_db_index(:first_name)
  have_db_index(:last_name)
  have_db_index(:salutation)
  have_db_index(:birthdate)
  have_db_index(:nationality_country_id)
  have_db_index(:email)
  have_db_index(:deleted_at)

  it "must be valid" do
    talent.must_be :valid?
  end

  it "must initialize itself" do
    Talent.any_instance.expects(:accept_terms)
    options = {
      country_id: 1,
      professional_experience_level: 1,
      dummy: 'something',
      first_name: 'John',
      last_name: 'Doe'
    }
    talent = Talent.init 'info@ts.at', options
    talent.first_name.must_equal 'John'
    talent.last_name.must_equal 'Doe'
    talent.setting.professional_experience_level.must_equal 1
    talent.setting.country_id.must_equal 1
  end

  it "must accept terms properly" do
    talent.accept_terms '10.0.0.1'
    talent.veeta_terms_accepted_ip.must_equal '10.0.0.1'
  end

  it "should have a name" do
    talent.name.must_equal("#{talent.first_name} #{talent.last_name}")
  end

  it "should be talent" do
    talent.must_be :talent?
  end

  it "wont be recruiter or admin" do
    talent.wont_be :recruiter?
    talent.wont_be :admin?
  end

  it "must have right ability" do
    talent.ability.must_be_instance_of Ability::TalentAbility
  end

  # recruiter_info(recruiter).must_equal ..
  it "must get info for one recruiter"

  it "must have job expectation" do
    talent.job_expectation.must_be_instance_of JobExpectation
  end

  it "must know the age of user" do
    talent.stubs(:birthdate).returns(29.years.ago)
    talent.age.must_equal(29)
  end

  it "should get a rating" do
    talent.stubs(:company_info).returns(build_stubbed(:company_talent_info, rating: 4))
    talent.company_rating(mock('company')).must_equal(4)
  end

  it "rating should be 0 instead of nil" do
    talent.stubs(:company_info).returns(build_stubbed(:company_talent_info, rating: nil))
    talent.company_rating(mock('company')).must_equal(0)
  end

  it "should take work experience from latest resume" do
    resume = mock 'resume'
    talent.stubs(:latest_resume).returns(resume)
    resume.expects(:work_industries)
    talent.work_industries
  end

  describe 'create/update avatar from temp file' do
    before do
      @talent = Talent.init 'info@ts.at', country_id: 4, professional_experience_level: 'default', dummy: 'something'
      @temp_upload_avatar = create(:temp_upload_avatar)
      @talent.update(temp_avatar_filename: @temp_upload_avatar.filename)
    end

    it 'save avatar' do
      @talent.avatar.wont_be_nil
    end

    it 'retrieve original filename from temp' do
      @talent.avatar_meta[:original_filename].must_equal 'demo.jpg'
    end
  end
end
