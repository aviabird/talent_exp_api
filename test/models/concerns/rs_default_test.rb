require "test_helper"

describe Concerns::RsDefault do
  let(:work_experience) { FactoryGirl.build_stubbed(:rs_work_experience) }

  it "should update parent resume when saving" do
    work_experience.resume.expects(:update_column)
    work_experience.send(:update_resume)
  end
end
