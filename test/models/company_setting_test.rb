# == Schema Information
#
# Table name: company_settings
#
#  id                  :integer          not null, primary key
#  company_id          :uuid
#  additional_settings :text
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_company_settings_on_company_id  (company_id)
#

require "test_helper"

describe CompanySetting do
  let(:company_setting) { build_stubbed(:company_setting) }

  validate_presence_of(:company)
  have_db_index(:company_id)

  it "must be valid" do
    company_setting.must_be :valid?
  end
end
