require "test_helper"

describe JobSerializer do
  let(:job) { FactoryGirl.build_stubbed(:job) }
  let(:serializer) { JobSerializer.new(job) }

  before do
    serializer.stubs(:current_user).returns(nil)
  end

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:job).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:job].has_key?(:id).must_equal true
    parsed[:job].has_key?(:language).must_equal true
    parsed[:job].has_key?(:company_id).must_equal true
    parsed[:job].has_key?(:recruiter_id).must_equal true
    parsed[:job].has_key?(:company).must_equal true
    parsed[:job].has_key?(:recruiter).must_equal true
    parsed[:job].has_key?(:status).must_equal true
    parsed[:job].has_key?(:name).must_equal true
    parsed[:job].has_key?(:jobname).must_equal true
    parsed[:job].has_key?(:reference).must_equal true
    parsed[:job].has_key?(:code).must_equal true
    parsed[:job].has_key?(:deleted_at).must_equal true
    parsed[:job].has_key?(:created_at).must_equal true
    parsed[:job].has_key?(:updated_at).must_equal true
    parsed[:job].has_key?(:accepted_jobs_languages).must_equal true
    parsed[:job].has_key?(:tags).must_equal true
    parsed[:job].has_key?(:available).must_equal true
  end
end