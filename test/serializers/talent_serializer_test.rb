require "test_helper"

describe TalentSerializer do
  let(:talent) { FactoryGirl.build_stubbed(:talent) }
  let(:serializer) { TalentSerializer.new(talent) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:talent).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:talent].has_key?(:id).must_equal true
    parsed[:talent].has_key?(:first_name).must_equal true
    parsed[:talent].has_key?(:last_name).must_equal true
    parsed[:talent].has_key?(:birthdate).must_equal true
    parsed[:talent].has_key?(:title).must_equal true
    parsed[:talent].has_key?(:email).must_equal true
    parsed[:talent].has_key?(:phone_number).must_equal true
    parsed[:talent].has_key?(:veeta_terms_accepted_at).must_equal true
    parsed[:talent].has_key?(:veeta_terms_accepted_ip).must_equal true
    parsed[:talent].has_key?(:created_at).must_equal true
    parsed[:talent].has_key?(:updated_at).must_equal true
    parsed[:talent].has_key?(:nationality_country).must_equal true
    parsed[:talent].has_key?(:address).must_equal true
    parsed[:talent].has_key?(:avatar_url).must_equal true
    parsed[:talent].has_key?(:guest).must_equal true
    parsed[:talent].has_key?(:confirmed).must_equal true
    parsed[:talent].has_key?(:job_expectation).must_equal true
    parsed[:talent].has_key?(:resumes_ids_with_time).must_equal true
    parsed[:talent].has_key?(:resume_ids).must_equal true
  end
end