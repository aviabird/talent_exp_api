require "test_helper"

describe Rs::ProjectSerializer do
  let(:project) { FactoryGirl.build_stubbed(:rs_project) }
  let(:serializer) { Rs::ProjectSerializer.new(project) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:project).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:project].has_key?(:id).must_equal true
    parsed[:project].has_key?(:name).must_equal true
    parsed[:project].has_key?(:role).must_equal true
    parsed[:project].has_key?(:link).must_equal true
    parsed[:project].has_key?(:from).must_equal true
    parsed[:project].has_key?(:to).must_equal true
    parsed[:project].has_key?(:description).must_equal true
  end
end