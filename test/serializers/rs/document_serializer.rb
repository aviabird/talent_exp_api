require "test_helper"

describe Rs::DocumentSerializer do
  let(:document) { FactoryGirl.build_stubbed(:rs_document) }
  let(:serializer) { Rs::DocumentSerializer.new(document) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:document).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:document].has_key?(:id).must_equal true
    parsed[:document].has_key?(:file).must_equal true
  end
end