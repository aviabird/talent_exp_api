require "test_helper"

describe TalentSerializer do
  let(:skill) { FactoryGirl.build_stubbed(:rs_skill) }
  let(:serializer) { Rs::SkillSerializer.new(skill) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:skill).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:skill].has_key?(:id).must_equal true
    parsed[:skill].has_key?(:name).must_equal true
    parsed[:skill].has_key?(:level).must_equal true
  end
end