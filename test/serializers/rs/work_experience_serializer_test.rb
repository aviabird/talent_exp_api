require "test_helper"

describe TalentSerializer do
  let(:work_experience) { FactoryGirl.build_stubbed(:rs_work_experience) }
  let(:serializer) { Rs::WorkExperienceSerializer.new(work_experience) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:work_experience).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:work_experience].has_key?(:id).must_equal true
    parsed[:work_experience].has_key?(:position).must_equal true
    parsed[:work_experience].has_key?(:company).must_equal true
    parsed[:work_experience].has_key?(:industry).must_equal true
    parsed[:work_experience].has_key?(:job_level).must_equal true
    parsed[:work_experience].has_key?(:terms_of_employment).must_equal true
    parsed[:work_experience].has_key?(:from).must_equal true
    parsed[:work_experience].has_key?(:to).must_equal true
    parsed[:work_experience].has_key?(:current).must_equal true
    parsed[:work_experience].has_key?(:description).must_equal true
    parsed[:work_experience].has_key?(:resume_id).must_equal true
    parsed[:work_experience].has_key?(:country).must_equal true
    parsed[:work_experience][:country].has_key?(:id).must_equal true
    parsed[:work_experience][:country].has_key?(:name).must_equal true
    parsed[:work_experience][:country].has_key?(:iso_code).must_equal true
  end
end