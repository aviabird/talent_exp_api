require "test_helper"

describe Rs::AwardSerializer do
  let(:award) { FactoryGirl.build_stubbed(:rs_award) }
  let(:serializer) { Rs::AwardSerializer.new(award) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:award).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:award].has_key?(:id).must_equal true
    parsed[:award].has_key?(:name).must_equal true
    parsed[:award].has_key?(:occupation).must_equal true
    parsed[:award].has_key?(:awarded_by).must_equal true
    parsed[:award].has_key?(:year).must_equal true
    parsed[:award].has_key?(:description).must_equal true
  end
end