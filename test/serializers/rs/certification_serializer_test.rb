require "test_helper"

describe Rs::CertificationSerializer do
  let(:certification) { FactoryGirl.build_stubbed(:rs_certification) }
  let(:serializer) { Rs::CertificationSerializer.new(certification) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:certification).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:certification].has_key?(:id).must_equal true
    parsed[:certification].has_key?(:certification_type).must_equal true
    parsed[:certification].has_key?(:subject).must_equal true
    parsed[:certification].has_key?(:organization).must_equal true
    parsed[:certification].has_key?(:year).must_equal true
    parsed[:certification].has_key?(:description).must_equal true
  end
end
