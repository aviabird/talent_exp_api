require "test_helper"

describe CompanySerializer do
  let(:company) { FactoryGirl.build_stubbed(:company) }
  let(:serializer) { CompanySerializer.new(company) }

  before do
    serializer.stubs(:current_user).returns(nil)
  end

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:company).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:company].has_key?(:id).must_equal true
    parsed[:company].has_key?(:name).must_equal true
    parsed[:company].has_key?(:tnc_text).must_equal true
    parsed[:company].has_key?(:tnc_link).must_equal true
    parsed[:company].has_key?(:logo_url).must_equal true
    parsed[:company].has_key?(:recruiter_ids).must_equal true
    parsed[:company].has_key?(:recruiter).must_equal true
    parsed[:company][:recruiter_ids].must_be_kind_of Array
  end

  # it 'should provide a correct array of uniq resumes' do
  #   result  = serializer.to_json
  #   parsed = JSON.parse(result, symbolize_names: true)
  # end
end
