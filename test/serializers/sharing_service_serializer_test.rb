require "test_helper"

describe SharingServiceSerializer do
  let(:company) { FactoryGirl.build_stubbed(:company) }
  let(:resume) { FactoryGirl.build_stubbed(:resume) }
  let(:shared_company) { SharedCompany.new(company, resume, 5.days.ago) }
  let(:serializer) { SharingServiceSerializer.new(shared_company) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:sharing_service).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:sharing_service].has_key?(:applied_at)
    parsed[:sharing_service].has_key?(:company)
    parsed[:sharing_service].has_key?(:resumes)
    parsed[:sharing_service][:applied_at].must_be_kind_of Array
    parsed[:sharing_service][:resumes].must_be_kind_of Array
  end
end