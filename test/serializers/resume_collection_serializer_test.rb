require "test_helper"

describe ResumeCollectionSerializer do
  describe "without applications" do
    let(:resume) { FactoryGirl.build_stubbed(:resume) }
    let(:serializer) { ResumeCollectionSerializer.new(resume) }

    it "should have the right API structure" do
      result = serializer.to_json
      parsed = JSON.parse(result, symbolize_names: true)
      parsed.has_key?(:resume_collection).must_equal true
      parsed.has_key?(:meta).must_equal false
      parsed[:resume_collection].has_key?(:id).must_equal true
      parsed[:resume_collection].has_key?(:name).must_equal true
      parsed[:resume_collection].has_key?(:image_snapshot_url).must_equal true
      parsed[:resume_collection].has_key?(:created_at).must_equal true
      parsed[:resume_collection].has_key?(:updated_at).must_equal true
      parsed[:resume_collection].has_key?(:shared_with).must_equal true
      parsed[:resume_collection][:shared_with].must_be_kind_of Array
      parsed[:resume_collection][:shared_with].must_be :empty?
    end
  end

  describe "applied resume" do
    let(:jobapp) { FactoryGirl.build_stubbed(:jobapp) }
    let(:resume) { FactoryGirl.build_stubbed(:resume, jobapps: [jobapp]) }
    let(:serializer) { ResumeCollectionSerializer.new(resume) }

    it "should have the right API structure" do
      result = serializer.to_json
      parsed = JSON.parse(result, symbolize_names: true)
      parsed[:resume_collection][:shared_with].must_be_kind_of Array
      parsed[:resume_collection][:shared_with].wont_be :empty?
      parsed[:resume_collection][:shared_with].first.has_key?(:id).must_equal true
      parsed[:resume_collection][:shared_with].first.has_key?(:name).must_equal true
      parsed[:resume_collection][:shared_with].first.has_key?(:logo_url).must_equal true
    end
  end
end