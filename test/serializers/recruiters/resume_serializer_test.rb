require "test_helper"

describe Recruiters::ResumeSerializer do
  let(:resume) { FactoryGirl.build_stubbed(:resume) }
  let(:serializer) { Recruiters::ResumeSerializer.new(resume) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:resume).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:resume].has_key?(:id).must_equal true
    parsed[:resume].has_key?(:language).must_equal true
    parsed[:resume].has_key?(:name).must_equal true
    parsed[:resume].has_key?(:deleted_at).must_equal true
    parsed[:resume].has_key?(:created_at).must_equal true
    parsed[:resume].has_key?(:updated_at).must_equal true
    parsed[:resume].has_key?(:work_experience_duration).must_equal true

    parsed[:resume].has_key?(:work_experiences).must_equal true
    parsed[:resume].has_key?(:educations).must_equal true
    parsed[:resume].has_key?(:languages).must_equal true
    parsed[:resume].has_key?(:skills).must_equal true
    parsed[:resume].has_key?(:documents).must_equal true
    parsed[:resume].has_key?(:memberships).must_equal true
    parsed[:resume].has_key?(:projects).must_equal true
    parsed[:resume].has_key?(:awards).must_equal true
    parsed[:resume].has_key?(:publications).must_equal true
    parsed[:resume].has_key?(:certifications).must_equal true
    parsed[:resume].has_key?(:about).must_equal true
    parsed[:resume].has_key?(:link).must_equal true
  end
end
