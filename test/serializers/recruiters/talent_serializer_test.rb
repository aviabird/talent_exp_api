require "test_helper"

describe Recruiters::TalentSerializer do
  let(:talent) { FactoryGirl.build_stubbed(:talent) }
  let(:recruiter) { FactoryGirl.build_stubbed(:recruiter) }
  let(:recruiter_auth) { FactoryGirl.build_stubbed(:recruiter_auth, recruiter: recruiter) }
  let(:serializer) { Recruiters::TalentSerializer.new(talent) }

  before do
    talent.stubs(:company_rating).returns(3)
    serializer.stubs(:current_user).returns(recruiter_auth)
  end

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:talent).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:talent].has_key?(:id).must_equal true
    parsed[:talent].has_key?(:first_name).must_equal true
    parsed[:talent].has_key?(:last_name).must_equal true
    parsed[:talent].has_key?(:birthdate).must_equal true
    parsed[:talent].has_key?(:title).must_equal true
    parsed[:talent].has_key?(:email).must_equal true
    parsed[:talent].has_key?(:phone_number).must_equal true
    parsed[:talent].has_key?(:veeta_terms_accepted_at).must_equal true
    parsed[:talent].has_key?(:veeta_terms_accepted_ip).must_equal true
    parsed[:talent].has_key?(:created_at).must_equal true
    parsed[:talent].has_key?(:updated_at).must_equal true
    parsed[:talent].has_key?(:nationality_country).must_equal true
    parsed[:talent].has_key?(:address).must_equal true
    parsed[:talent].has_key?(:avatar_url).must_equal true
    parsed[:talent].has_key?(:job_expectation).must_equal true
    parsed[:talent].has_key?(:resumes).must_equal true
    parsed[:talent].has_key?(:note).must_equal true
    parsed[:talent].has_key?(:labels).must_equal true
    parsed[:talent].has_key?(:in_pools).must_equal true
    parsed[:talent].has_key?(:applied_for).must_equal true
    parsed[:talent].has_key?(:first_applied_at).must_equal true
  end
end