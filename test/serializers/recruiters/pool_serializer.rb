require "test_helper"

describe Recruiters::PoolSerializer do
  let(:pool) { FactoryGirl.build_stubbed(:pool) }
  let(:serializer) { Recruiters::PoolSerializer.new(pool) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:pool).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:pool].has_key?(:id).must_equal true
    parsed[:pool].has_key?(:name).must_equal true
    parsed[:pool].has_key?(:talents_count).must_equal true
  end
end