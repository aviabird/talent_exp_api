require "test_helper"

describe TalentSerializer do
  let(:company) { FactoryGirl.build_stubbed(:company) }
  let(:job) { FactoryGirl.build_stubbed(:job, company: company) }
  let(:jobapp) { FactoryGirl.build_stubbed(:jobapp, job: job) }
  let(:serializer) { JobappSerializer.new(jobapp) }

  it "should have the right API structure" do
    result = serializer.to_json
    parsed = JSON.parse(result, symbolize_names: true)
    parsed.has_key?(:jobapp).must_equal true
    parsed.has_key?(:meta).must_equal false
    parsed[:jobapp].has_key?(:id).must_equal true
    parsed[:jobapp].has_key?(:cover_letter).must_equal true
    parsed[:jobapp].has_key?(:contact_preferences).must_equal true
    parsed[:jobapp].has_key?(:blocked_companies).must_equal true
    parsed[:jobapp].has_key?(:recruiter_rating).must_equal true
    parsed[:jobapp].has_key?(:recruiter_note).must_equal true
    parsed[:jobapp].has_key?(:talent_info_status).must_equal true
    parsed[:jobapp].has_key?(:added_by_recruiter).must_equal true
    parsed[:jobapp].has_key?(:read_at).must_equal true
    parsed[:jobapp].has_key?(:application_referrer_url).must_equal true
    parsed[:jobapp].has_key?(:allow_recruiter_contact_sharing).must_equal true
    parsed[:jobapp].has_key?(:company_terms_accepted_at).must_equal true
    parsed[:jobapp].has_key?(:company_terms_accepted_ip).must_equal true
    parsed[:jobapp].has_key?(:withdrawn_at).must_equal true
    parsed[:jobapp].has_key?(:withdrawn_reason).must_equal true
    parsed[:jobapp].has_key?(:created_at).must_equal true
    parsed[:jobapp].has_key?(:updated_at).must_equal true
    parsed[:jobapp].has_key?(:resume_id).must_equal true
    parsed[:jobapp].has_key?(:job).must_equal true
    # parsed[:jobapp].has_key?(:origin_resume).must_equal true
    parsed[:jobapp].has_key?(:company).must_equal true
    parsed[:jobapp].has_key?(:recruiter).must_equal true
    parsed[:jobapp].has_key?(:job_id).must_equal true
    parsed[:jobapp].has_key?(:job_type).must_equal true
    parsed[:jobapp].has_key?(:available).must_equal true
  end
end
