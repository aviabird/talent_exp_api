ENV['RAILS_ENV'] ||= 'test'
require 'simplecov'
SimpleCov.start

require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'minitest-spec-rails'
require 'minitest/pride'
require 'minitest/reporters'
require 'mocha/mini_test'
require 'webmock/minitest'
require 'shoulda/matchers'
#require 'sidekiq/testing'
#require 'mandrill_mailer/offline'

module Minitest
  module Reporters
    class AwesomeReporter < DefaultReporter
      GREEN = '1;32'
      RED = '1;31'

      def color_up(string, color)
        color? ? "\e\[#{ color }m#{ string }#{ ANSI::Code::ENDCODE }" : string
      end

      def red(string)
        color_up(string, RED)
      end

      def green(string)
        color_up(string, GREEN)
      end
    end
  end
end

reporter_options = { color: true, slow_count: 10 }
Minitest::Reporters.use! [Minitest::Reporters::AwesomeReporter.new(reporter_options)]

include FactoryGirl::Syntax::Methods

# Parse JSON easily and convert that into hash names
def parse json
  JSON.parse(json, symbolize_names: true)
end

RecruiterApi.module_eval do
  def authenticate_recruiter_auth!; end
  def autologin!; end
end

ActionController::TestCase.class_eval do
  before do
    @ability = Object.new
    @ability.extend(CanCan::Ability)
    @ability.can :manage, :all
    @controller.stubs(:current_ability).returns(@ability)
  end
end

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.

#  fixtures :all


  # Add more helper methods to be used by all tests here...
end
