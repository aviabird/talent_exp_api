# == Schema Information
#
# Table name: client_applications
#
#  id                            :uuid             not null, primary key
#  name                          :string
#  url                           :string
#  support_url                   :string
#  callback_url                  :string
#  key                           :string(40)
#  secret                        :string(40)
#  user_id                       :uuid
#  created_at                    :datetime
#  updated_at                    :datetime
#  webhook_url                   :string
#  delete_user_profile_deep_link :string
#  logo                          :string
#  cancel_url                    :string
#  use_cancel_callback           :boolean          default(TRUE)
#  contact                       :string
#  deleted_at                    :datetime
#  token                         :string
#  logo_meta                     :text
#
# Indexes
#
#  index_client_applications_on_created_at  (created_at)
#  index_client_applications_on_key         (key) UNIQUE
#  index_client_applications_on_token       (token)
#

require File.dirname(__FILE__) + '/../test_helper'
module OAuthHelpers

  def create_consumer
    @consumer=OAuth::Consumer.new(@application.key,@application.secret,
      {
        :site=>@application.oauth_server.base_url
      })
  end

end

class ClientApplicationTest < ActiveSupport::TestCase
  include OAuthHelpers
  fixtures :users,:client_applications,:oauth_tokens

  def setup
    @application = ClientApplication.create :name=>"Agree2",:url=>"http://agree2.com",:user=>users(:quentin)
    create_consumer
  end

  def test_should_be_valid
    assert @application.valid?
  end


  def test_should_not_have_errors
    assert_equal [], @application.errors.full_messages
  end

  def test_should_have_key_and_secret
    assert_not_nil @application.key
    assert_not_nil @application.secret
  end

  def test_should_have_credentials
    assert_not_nil @application.credentials
    assert_equal @application.key, @application.credentials.key
    assert_equal @application.secret, @application.credentials.secret
  end

end
