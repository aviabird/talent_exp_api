require "test_helper"
require 'ipaddr'

# some basic tests for rack attack security

describe Rack::Attack do
  include Rack::Test::Methods
  let(:recruiter) { FactoryGirl.build_stubbed(:recruiter) }
  let(:recruiter_auth) { FactoryGirl.build_stubbed(:recruiter_auth, recruiter: recruiter) }
  let(:current_user) { build_stubbed(:recruiter_auth) }
  let(:current_recruiter) { build_stubbed(:recruiter) }
  let(:current_company) { build_stubbed(:company) }
  before do
    #allow(RecruiterApi).to receive(:current_company).and_return(build_stubbed(:company))
  end
  def app
    Rails.application
  end

  describe "throttle excessive sign_in requests by IP address" do
    let(:limit) { 15 }
    context "number of requests is lower than the limit" do
      it "does not change the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        limit.times do
          post '/api/recruiter/auth/sign_in', { 'email': "example#{rand(1..1000)}@t13s.at", 'password': '12345678'},"REMOTE_ADDR" => ipv4.to_s#, "CONTENT_TYPE" => "application/json"#{:format => "json", "REMOTE_ADDR" => ipv4.to_s, "ACCEPT" => "application/json, text/plain, */*", "CONTENT_TYPE" => "application/json"}
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 401
        last_response.status.must_equal 401
      end
    end

    context "number of requests is higher than the limit" do
      it "changes the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        (limit * 2).times do
          post "/api/recruiter/auth/sign_in", {email: "example#{rand(1..1000)}@t13s.at", password: "12341234"},"action_dispatch.request.parameters" => {email: "example#{rand(1..1000)}@t13s.at", password: "12341234"}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 401
        last_response.status.must_equal 429
      end
    end
  end

  describe "throttle excessive sign_in requests by IP address and email address" do
    let(:limit) { 3 }
    context "number of requests is lower than the limit" do
      it "does not change the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        limit.times do
          post "/api/recruiter/auth/sign_in", {email: "example21@t13s.at", password: "12341234"}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 401
        last_response.status.must_equal 401
      end
    end

    context "number of requests is higher than the limit" do
      it "changes the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        (limit * 2).times do
          post "/api/recruiter/auth/sign_in", {email: "example21@t13s.at", password: "12341234"},"action_dispatch.request.parameters" => {email: "example21@t13s.at", password: "12341234"}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 401
        last_response.status.must_equal 429
      end
    end
  end

  describe "throttle excessive sign out requests by IP address" do
    let(:limit) { 15 }
    context "number of requests is lower than the limit" do
      it "does not change the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        limit.times do
          delete '/api/recruiter/auth/sign_out', {}, 'REMOTE_ADDR' => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 404
        last_response.status.must_equal 404
      end
    end

    context "number of requests is higher than the limit" do
      it "changes the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        (limit * 2).times do
          delete "/api/recruiter/auth/sign_out", {}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 404
        last_response.status.must_equal 429
      end
    end
  end

  describe "throttle excessive requests by IP address" do
    let(:limit) { 100 }

    context "number of requests is lower than the limit" do
      it "does not change the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        limit.times do
          get "/", {}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 200
      end
    end

    context "number of requests is higher than the limit" do
      it "changes the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        (2001).times do
          get "/", {}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 200
        last_response.status.must_equal 429
      end
    end
  end

  describe "throttle excessive token validation requests by IP address" do
    let(:limit) { 20 }

    context "number of requests is lower than the limit" do
      it "does not change the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        limit.times do
          get "/api/recruiter/auth/validate_token", {}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 401
        last_response.status.must_equal 401
      end
    end

    context "number of requests is higher than the limit" do
      it "changes the request status" do
        stub_request(:post, "https://hooks.slack.com/services/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").to_return(:status => 200, :body => "", :headers => {})
        first_response = nil
        ipv4 = IPAddr.new(rand(2**32),Socket::AF_INET)
        (limit * 16).times do
          get "/api/recruiter/auth/validate_token", {}, "REMOTE_ADDR" => ipv4.to_s
          if first_response.nil?
            first_response = last_response
          end
        end
        first_response.status.must_equal 401
        last_response.status.must_equal 429
      end
    end
  end

end
