require "test_helper"

describe ResumeService::Store do
  let(:resume) { FactoryGirl.build_stubbed(:resume) }
  let(:service) { ResumeService::Store.new(resume) }

  it "should generate xml" do
    resume.expects(:to_xml)
    service.xml
  end

  it "should try to save it" do 
    service.expects(:save)
    service.call
  end

  it "should invoke saving new snapshot" do 
    service.expects(:snapshot)
    service.send(:save)
  end

  it "snapshot is instance of ResumeSnapshot" do 
    ResumeSnapshot.expects(:create!)
    service.snapshot
  end
end