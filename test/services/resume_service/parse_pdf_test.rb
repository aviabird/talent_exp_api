require "test_helper"

describe ResumeService::ParsePDF do
  let(:path) { Rails.root.join('test', 'data', 'resume', 'cv.pdf') }
  let(:service) { ResumeService::ParsePDF.new(path) }
  let(:response_textkernel) { File.read(Rails.root.join('test', 'data', 'resume', 'response_textkernel.json')) }
  let(:cv_xml) { File.read(Rails.root.join('test', 'data', 'resume', 'cv_textkernel.xml')) }
  let(:lebenslauf_xml) { File.read(Rails.root.join('test', 'data', 'resume', 'lebenslauf_textkernel.xml')) }

  before do
    WebMock.disable_net_connect!
    service.stubs(:response)
    stub_request(:get, "#{ENV['TEXTKERNEL_URL']}").
      with(:headers => {'Host'=>'home.textkernel.nl:443'}).
      to_return(:status => 200, :body => response_textkernel, :headers => {})
  end

  it "should parse into hash" do
    2.times do |i|
      i == 1 ? service.stubs(:xml).returns(cv_xml) : service.stubs(:xml).returns(lebenslauf_xml)
      service.hashed[:profile].wont_be :nil?
      service.hashed[:profile][:personal].wont_be :nil?
      service.hashed[:profile][:personal].has_key?(:title).must_equal true
      service.hashed[:profile][:personal].has_key?(:first_name).must_equal true
      service.hashed[:profile][:personal].has_key?(:last_name).must_equal true
      service.hashed[:profile][:personal].has_key?(:date_of_birth).must_equal true
      service.hashed[:profile][:personal].has_key?(:nationality_code).must_equal true
      service.hashed[:profile][:personal].has_key?(:gender_code).must_equal true
      service.hashed[:profile][:personal].has_key?(:marital_status_code).must_equal true
      service.hashed[:profile][:personal][:address].wont_be :nil?
      service.hashed[:profile][:personal][:address].has_key?(:street_name).must_equal true
      service.hashed[:profile][:personal][:address].has_key?(:street_number_base).must_equal true
      service.hashed[:profile][:personal][:address].has_key?(:postal_code).must_equal true
      service.hashed[:profile][:personal][:address].has_key?(:city).must_equal true
      service.hashed[:profile][:personal][:address].has_key?(:country_code).must_equal true
      service.hashed[:profile][:personal].has_key?(:mobile_phone)
      service.hashed[:profile][:personal][:mobile_phones].wont_be :nil?
      service.hashed[:profile][:personal][:mobile_phones][:mobile_phone].wont_be :nil?
      service.hashed[:profile][:personal][:mobile_phones][:mobile_phone].must_be_kind_of Array
      service.hashed[:profile][:personal].has_key?(:home_phone)
      service.hashed[:profile][:personal][:home_phones].wont_be :nil?
      service.hashed[:profile][:personal][:home_phones][:home_phone].wont_be :nil?
      service.hashed[:profile][:personal][:home_phones][:home_phone].must_be_kind_of Array
      service.hashed[:profile][:personal].has_key?(:email)
      service.hashed[:profile][:personal][:emails].wont_be :nil?
      service.hashed[:profile][:personal][:emails][:email].wont_be :nil?
      service.hashed[:profile][:personal][:emails][:email].must_be_kind_of Array
      service.hashed[:profile][:education_history].wont_be :nil?
      service.hashed[:profile][:education_history][:education_item].wont_be :nil?
      service.hashed[:profile][:education_history][:education_item].must_be_kind_of Array
      service.hashed[:profile][:education_history][:education_item].first.has_key?(:degree_direction).must_equal true
      service.hashed[:profile][:education_history][:education_item].first.has_key?(:start_date).must_equal true
      service.hashed[:profile][:education_history][:education_item].first.has_key?(:end_date).must_equal true
      service.hashed[:profile][:education_history][:education_item].first.has_key?(:institute_name).must_equal true
      service.hashed[:profile][:employment_history].wont_be :nil?
      service.hashed[:profile][:employment_history][:employment_item].wont_be :nil?
      service.hashed[:profile][:employment_history][:employment_item].must_be_kind_of Array
      service.hashed[:profile][:employment_history][:employment_item].first.has_key?(:job_title).must_equal true
      service.hashed[:profile][:employment_history][:employment_item].first.has_key?(:start_date).must_equal true
      service.hashed[:profile][:employment_history][:employment_item].first.has_key?(:end_date).must_equal true
      service.hashed[:profile][:employment_history][:employment_item].first.has_key?(:employer_name).must_equal true
      service.hashed[:profile][:employment_history][:employment_item].first.has_key?(:description).must_equal true
      service.hashed[:profile][:skills].wont_be :nil?
      service.hashed[:profile][:skills][:computer_skills].wont_be :nil?
      service.hashed[:profile][:skills][:computer_skills][:computer_skill].wont_be :nil?
      service.hashed[:profile][:skills][:computer_skills][:computer_skill].must_be_kind_of Array
      service.hashed[:profile][:skills][:computer_skills][:computer_skill].first.wont_be :nil?
      service.hashed[:profile][:skills][:computer_skills][:computer_skill].first.has_key?(:computer_skill_name).must_equal true
      service.hashed[:profile][:skills][:language_skills].wont_be :nil?
      service.hashed[:profile][:skills][:language_skills][:language_skill].wont_be :nil?
      service.hashed[:profile][:skills][:language_skills][:language_skill].must_be_kind_of Array
      service.hashed[:profile][:skills][:language_skills][:language_skill].first.wont_be :nil?
      service.hashed[:profile][:skills][:language_skills][:language_skill].first.has_key?(:language_skill_code).must_equal true
      service.hashed[:profile][:skills][:language_skills][:language_skill].first.has_key?(:language_proficiency_code).must_equal true
      service.hashed[:profile][:skills][:soft_skills].wont_be :nil?
      service.hashed[:profile][:skills][:soft_skills][:soft_skill].wont_be :nil?
      service.hashed[:profile][:skills][:soft_skills][:soft_skill].must_be_kind_of Array
      service.hashed[:profile][:skills][:soft_skills][:soft_skill].first.wont_be :nil?
      service.hashed[:profile][:skills][:soft_skills][:soft_skill].first.has_key?(:soft_skill_name).must_equal true
      service.hashed[:profile][:custom_area].wont_be :nil?
      service.hashed[:profile][:custom_area][:profile_picture].wont_be :nil?
      service.hashed[:profile][:custom_area][:profile_picture].has_key?(:base64_content)
    end
  end
end
