require "test_helper"

describe SharedCompany do
  let(:company) { FactoryGirl.build_stubbed(:company) }
  let(:resume) { FactoryGirl.build_stubbed(:resume) }
  let(:applied_at) { 5.days.ago }
  let(:shared_company) { SharedCompany.new(company, resume, applied_at) }

  it "must be in good mood" do
    shared_company.company.must_equal company
    shared_company.resumes.must_be_kind_of Array
    shared_company.applied_at.must_be_kind_of Array
    shared_company.resumes.include?(resume).must_equal true
    shared_company.applied_at.include?(applied_at).must_equal true
  end

  it "must determinate that it's not in array" do
    companies = []
    shared_company.is_company_in?(companies).must_equal nil
  end

  it "must determinate that it's in array" do
    companies = [shared_company]
    shared_company.is_company_in?(companies).must_equal shared_company
  end
end
