require "test_helper"

describe AuthService::DeleteAccount do
  before do
    @talent_auth = TalentAuth.new do |talent|
      talent.password = '123456789'
      talent.password_confirmation = '123456789'
      talent.guest = true
      talent.is_guest = true
      talent.email = 'services@talentsolutions.at'
      talent.resume_type = 'beginner'
      talent.country_id = 1
      talent.skip_confirmation!
    end
    @talent_auth.save!
  end
  after { DatabaseCleaner.clean_with(:truncation) }
  let(:service) { AuthService::DeleteAccount.new(@talent_auth, email: 'services@talentsolutions.at', password: '123456789', delete_reason: "No more please") }

  it "must be valid" do
    @talent_auth.must_be :valid?
  end

  it "should call new instance" do
    AuthService::DeleteAccount.any_instance.expects(:call)
    AuthService::DeleteAccount.call @talent_auth
  end

  it "should check what to do" do
    service.stubs(:fine?).returns(true)
    service.expects(:delete_account)
    service.call
  end

  it "should check what to do" do
    service.stubs(:fine?).returns(false)
    service.expects(:set_errors)
    service.call
  end

  it "should check if params are there" do
    service.send(:params?).must_equal true
  end

  it "should check that password is the same" do
    service.send(:valid_password?).must_equal true
  end

  it "should check that password is not the same" do
    service.instance_variable_set(:@password, 'different')
    service.send(:valid_password?).must_equal false
  end

  it "should check that password is not the same" do
    service.instance_variable_set(:@password, 123456789)
    service.send(:valid_password?).must_equal true
  end

  it "should 'delete' account" do
    service.send(:delete_account)
    @talent_auth.email.wont_equal 'services@talentsolutions.at'
  end

  it "should set errors when is not pass provided" do
    service.instance_variable_set(:@password, nil)
    service.send(:set_errors)
    @talent_auth.errors.wont_be :empty?
  end

  it "should set errors when talent is no longer guest" do
    service.instance_variable_set(:@email, nil)
    service.send(:set_errors)
    @talent_auth.errors.wont_be :empty?
  end

  it "should set errors when talent is no reason giveb" do
    service.instance_variable_set(:@delete_reason, nil)
    service.send(:set_errors)
    @talent_auth.errors.wont_be :empty?
  end

end