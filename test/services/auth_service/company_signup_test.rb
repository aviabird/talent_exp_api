require "test_helper"

describe AuthService::CompanySignup do
  before do
    @auth = AuthService::CompanySignup.new do |auth|
      auth.company_name = "TalentSolutions"
      auth.first_name = "John"
      auth.last_name = "Doe"
      auth.email = "jiri@talentsolutions.at"
      auth.password = "1q2w3e4r5t6y7u8i9o0p"
      auth.password_confirmation = "1q2w3e4r5t6y7u8i9o0p"
      auth.plan = 'pro'
      auth.ip = '10.0.0.2'
    end
  end
  after { DatabaseCleaner.clean_with(:truncation) }

  it "must have proper pavlue" do
    @auth.signup!
    @auth.company.must_be_instance_of Company
    @auth.recruiter.must_be_instance_of Recruiter
    @auth.auth.must_be_instance_of RecruiterAuth
  end

  it "must be linked properly" do
    @auth.signup!
    @auth.company.owner.must_equal @auth.recruiter
    @auth.recruiter.company.must_equal @auth.company
    @auth.recruiter.auth.must_equal @auth.auth
  end
end