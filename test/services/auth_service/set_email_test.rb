require "test_helper"

describe AuthService::SetEmail do
  before do
    @talent_auth = TalentAuth.new do |talent|
      talent.password = '123456789'
      talent.password_confirmation = '123456789'
      talent.guest = true
      talent.is_guest = true
      talent.email = 'services@talentsolutions.at'
      talent.resume_type = 'beginner'
      talent.country_id = 1
      talent.skip_confirmation!
    end
    @talent_auth.save!
    @talent_auth.update_attribute(:confirmed_at, nil)
  end
  after { DatabaseCleaner.clean_with(:truncation) }
  let(:service) { AuthService::SetEmail.new(@talent_auth, 'services@ts.at') }

  it "must be valid" do
    @talent_auth.must_be :valid?
  end

  it "should call new instance" do
    AuthService::SetEmail.any_instance.expects(:call)
    AuthService::SetEmail.call @talent_auth, 'services@ts.at'
  end

  it "should define if is possible to update pass" do
    service.send(:possible?).must_equal true
  end

  it "shoun't be possible, when no pass provided" do
    service.instance_variable_set(:@email, nil)
    service.send(:possible?).must_equal false
  end

  it "shoun't be possible, when is already confirmed" do
    @talent_auth.stubs(:confirmed?).returns(true)
    service.send(:possible?).must_equal false
  end

  it "should update if is possible" do
    service.expects(:set_email)
    service.call
  end

  it "shouldn't update if is not possible" do
    service.stubs(:possible?).returns(false)
    service.expects(:set_email).never
    service.expects(:set_errors)
    service.call
  end

  it "should set errors when is not pass provided" do
    service.instance_variable_set(:@email, nil)
    service.send(:set_errors)
    @talent_auth.errors.wont_be :empty?
  end

  it "should set errors when talent is no longer guest" do
    @talent_auth.stubs(:confirmed?).returns(true)
    service.send(:set_errors)
    @talent_auth.errors.wont_be :empty?
  end

  it "should send email" do
    mailer = mock('mailer')
    mailer.expects(:deliver)
    DeviseMailer.expects(:confirmation_instructions).returns(mailer)
    service.send(:set_email)
  end

  # it "should set email" do
  #   @talent_auth.email.must_equal 'services@ts.at'
  #   @talent_auth.unconfirmed_email.must_equal 'services@ts.at'
  #   @talent_auth.talent.email.must_equal 'services@talentsolutions.at'
  # end
end
