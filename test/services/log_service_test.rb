require "test_helper"

CURRENT_USER = FactoryGirl.build_stubbed(:talent_auth)

module Dummy
  module LogService
    Routes = ActionDispatch::Routing::RouteSet.new
    Routes.draw do
      get '/:controller(/:action(/:id))'
    end

    class Address 
      def user
        PublicActivity.get_controller.current_user
      end

      def email
        PublicActivity.get_controller.current_email
      end
    end

    class AppController < ApplicationController
      respond_to :json

      def index
        log :error
        @address = Address.new
        respond_with @address
      end

      def loggit
        @log = log :error
        respond_with @log
      end

      def error
        begin
          raise Exception, 'Loading me failed, so do not add to loaded or history.'
        rescue Exception => e
          @log = log(:error, source: 'System', data: e)
          # # Don't raise anything here
          # raise e
        ensure
          respond_with @log
        end
      end

      def current_user
        CURRENT_USER
      end
    end
  end
end

describe Dummy::LogService::AppController do  
  before do
    @routes = Dummy::LogService::Routes
    @request.env['REMOTE_ADDR'] = '1.2.3.4'
    @request.env['HTTP_REFERER'] = 'http://localhost:3000'
  end

  it "should get current email and user"
  #  do
  #   get :loggit, format: :json, email: 'services@talentsolutions.at'
  #   assigns(:log).instance_variable_get(:@user).must_equal CURRENT_USER
  #   assigns(:log).instance_variable_get(:@email).must_equal 'services@talentsolutions.at'
  #   assigns(:log).instance_variable_get(:@source_ip).must_equal '1.2.3.4'
  #   assigns(:log).instance_variable_get(:@source_referrer_url).must_equal 'http://localhost:3000'
  # end

  it "should log exception" do 
    get :error, format: :json
    assigns(:log).source.must_equal 'System'
    assigns(:log).data.must_be_kind_of Exception
  end
end

describe LogService do

  it "should init new instance" do
    LogService.any_instance.expects(:call)
    LogService.call(:error)
  end

  it "should always return instance of class" do
    LogService.any_instance.expects(:save)
    LogService.call(:error).must_be_instance_of LogService
  end


  it "should create new record" do
    Log.expects(:create)
    service = LogService.new(:error, {})
    service.send(:save)
  end

  describe "don't save that" do 
    before { LogService.any_instance.stubs(:save) }

    it "should always keep key" do 
      LogService.call(:error).key.must_equal :error
    end

    it "should check controller information" do 
      LogService.any_instance.expects(:controller_infos)
      LogService.call(:error)
    end

    it "should keep the source" do
      service = LogService.call(:error, source: 'InternalError')
      service.source.must_equal 'InternalError'
    end

    it "should set Talent as source" do
      service = LogService.call(:error, user: FactoryGirl.build_stubbed(:talent))
      service.source.must_equal 'Talent'
    end

    it "should set anonymouse user" do
      LogService.any_instance.stubs(:controller_infos)
      service = LogService.call(:error, email: 'my@mail.com')
      service.source.must_equal 'Anonymous'
    end

    it "should have low priority as default" do 
      LogService.call(:error).priority.must_equal :low
    end

    it "should have object informations"
    #  do
    #   object = FactoryGirl.build_stubbed(:resume)
    #   service = LogService.call(:error, object: object)
    #   service.object_type.must_equal 'Resume'
    #   service.object_id.wont_be_nil
    #   service.resume_id.must_be_nil
    #   service.jobapp_id.must_be_nil
    #   service.talent_id.wont_be_nil
    #   service.recruiter_id.must_be_nil
    #   service.admin_id.must_be_nil
    #   service.data.must_equal object.inspect
    # end
  end
end