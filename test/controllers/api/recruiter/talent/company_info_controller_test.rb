require "test_helper"

describe Api::Recruiter::Talent::CompanyInfoController do
  let(:company_info) { build_stubbed(:company_talent_info) }
  let(:current_user) { build_stubbed(:recruiter_auth) }

  before do 
    CompanyTalentInfo.stubs(:find).returns(company_info)
  end

  describe 'GET /api/recruiter/talent/company_info/:id' do
    it "should show talent" do
      get :show, format: :json, id: company_info
      response.status.must_equal 200
    end
  end
end
