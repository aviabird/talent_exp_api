require "test_helper"

describe Api::Recruiter::Talent::RecruiterInfoController do
  let(:recruiter_info) { build_stubbed(:recruiter_talent_info) }
  let(:current_user) { build_stubbed(:recruiter_auth) }

  before do 
    RecruiterTalentInfo.stubs(:find).returns(recruiter_info)
  end

  describe 'GET /api/recruiter/talent/recruiter_info/:id' do
    it "should show talent" do
      get :show, format: :json, id: recruiter_info
      response.status.must_equal 200
    end
  end
end
