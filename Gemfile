source 'https://rubygems.org'

ruby '2.3.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 4.2.0'

# Use postgresql as the database for Active Record
gem 'pg', '0.18.1'

#########################################################################################################
######################################### Gems for WebDAV Access ########################################

gem 'net_dav'
gem 'curb'
gem 'carrierwave-webdav',  :require => 'carrierwave/webdav'

#########################################################################################################
######################################## Bluemix deployment gems ########################################

# bluemix requirements
gem 'rb-readline'
gem "cf-autoconfig", "~> 0.2.1"
gem 'rails_12factor', group: :production

# process manager, starting procfile
gem 'foreman'

#########################################################################################################

# Use Rails-i18n for internationalization
gem 'rails-i18n', '~> 4.0'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'

# Application Settings
gem 'settingslogic'

# Use kaminari for pagination
gem 'kaminari'

# Use ActiveModel Serializer for model serializations
gem 'active_model_serializers', '~> 0.9.5'

# Use Responders for rendering objects into JSON in our case
gem 'responders', '~> 2.0'

# Use RespondJSON to extend default JSON responses
gem 'respondjson', '=1.0.3', git: 'https://ts-build-user:AmKqbGA32GWsmKHFC8iKp9wR6o5HabZ6@bitbucket.org/talent-solutions/respondjson.git', branch: :develop

# Authehticate users with Devise
#gem 'devise', '~> 3.4.1'

# Extend Devise with easy token authenthication
#gem 'simple_token_authentication'

# Extend Devise with advanced token handling for AngularJS
#gem 'devise_token_auth', "0.1.37"

# Send devise emails in the background
#gem 'devise-async'

# Use OmniAuth for 3rd party authentication
gem 'omniauth'

# Authorize users actions with CanCan (Community)
gem 'cancancan', '~> 1.13.0'

# Upload files with CarrierWave
#gem 'carrierwave'

# Use MiniMagick for low-memory image handling
#gem 'mini_magick'

# Use Resque for background jobs
gem 'resque', '1.25.2', git: 'https://github.com/resque/resque.git', branch: '1-x-stable'
gem 'resque-web', '0.0.6', require: 'resque_web'
gem 'resque_mailer', '2.2.7'
gem 'resque-scheduler'
gem 'rufus-scheduler', '< 3.0.0'

# Server monitoring with New Relic
gem 'newrelic_rpm'

# Use seedbank for mode advanced dummy/production data import
gem 'seedbank'

# Track user activity with PublicActivity
#gem 'public_activity'

# Add tags on models with 'acts as taggable on'
#gem 'acts-as-taggable-on'

# Enable CORS protection
gem 'rack-cors', require: 'rack/cors'

# Generate virtual model attributes with Virtus
gem 'virtus'

# Generate unique identification code of records with hash ids
gem 'hashids'

# Use LogEntries for tracking logs in cloud
gem 'le'

# Versioning
#gem 'amoeba'

# List of countries with ISO3166-1 and currencies ISO4217
gem 'iso_country_codes', github: 'talentsolutions/iso_country_codes'

# List of languages with ISO-639-1
gem 'language_list', '=1.2.0', github: 'talentsolutions/language_list'

# Use Chewy for easier ElasticSearch
gem 'chewy'
gem 'elasticsearch'
gem 'elasticsearch-extensions'
gem 'active_data', github: 'pyromaniac/active_data'

# Use official ElasticSearch Ruby gems for search
gem 'elasticsearch-model'

# Use official ElasticSearch Rails integration
gem 'elasticsearch-rails'

# redis cache
gem 'redis-activesupport'


#gem 'myveeta_domain', '=0.0.11', git: 'https://ts-build-user:AmKqbGA32GWsmKHFC8iKp9wR6o5HabZ6@bitbucket.org/talent-solutions/myveeta_domain.git', branch: 'release/talent_explorer_api'
gem 'myveeta_domain', '~> 0.0.30', git: 'https://ts-build-user:AmKqbGA32GWsmKHFC8iKp9wR6o5HabZ6@bitbucket.org/talent-solutions/myveeta_domain.git', branch: 'develop'

# http://engineering.harrys.com/2014/07/29/hacking-bundler-groups.html
group :web do

  # wrapper for http requests
  gem 'http'

  #slack notifier
#  gem 'slack-notifier'

  # Passenger App server
  gem 'passenger', '~> 5.0.21'

  # Use LocaleApp for getting translations
  gem 'localeapp'

  # Security gem for request filtering/blocking/tracking
  # https://github.com/kickstarter/rack-attack
  gem 'rack-attack'

  # calling intercom API
  gem 'intercom', "~> 3.4.0"
end

group :production, :staging do
  # Track errors with Sentry
  gem 'sentry-raven'
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  gem 'pry'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  # Load env variables form file
  gem 'dotenv-rails'

  # Dump current table schema into file with annotate
  gem 'annotate'

  # Dummy data generator with Forgery
  gem 'forgery'

end

group :test do
  # Use Spec with minitest
  gem 'minitest-spec-rails'
  gem 'minitest-reporters'

  # Use SimpleCov for code coverage
  gem 'simplecov'

  # Fake web requests with webmock
  gem 'webmock', require: false

  # Stubs with objects and methods with mocha
  gem 'mocha'

  # Easy active model tests with shoulda
  gem 'shoulda'

  # Use FactoryGirl instead of fixtures
  gem 'factory_girl_rails'

  # Clean up database records with database cleaner
  gem 'database_cleaner'

  gem 'rspec-rails'
end
