
# Talent Explorer Intapi

## Models

Models are separted in engine *myveeta_domain* as the models are used by all applications. Add all migrations only in the myveeta_domain project!

## Build ES Index
In order to inti / reset the index, execute the following command

```
bundle exec rake chewy:reset
```

## Run app

```
env PROCESS_TYPE=web RAILS_ENV=development bundle exec rails s -b 0.0.0.0
```

## Do search queries

### Talents

#### Query and parameters
The endpoint for doing search queries on talents is available under */api/recruiter/talents*

Any parameter which is part of the search must be a hash entry of the parameter *search*.

+ search[query]=*value* - value is a string used for full text search
+ search[job_titles]=*value1,value1*  - comma-separated list of tags
+ search[working_locations]=*value1,value1*  - comma-separated list of working locations
+ search[working_industries]=*value1,value1*  - comma-separated list of working industries
+ search[experience_in_industries]=*value1,value1*  - comma-separated list of experience in industries
+ search[availability]=*value*  - value can be any of [immediately | 1m | 2m | 3m | 4-6m | 6+m]
+ search[prefered_employment_types]=*value1,value1*  - comma-separated list of any values of []
+ search[job_seeker_statuses]=*value1,value1*  - comma-separated list of any values of []
+ search[salary_min]=*salary_bucket_0*  - string value in between salary_bucket_0 to salary_bucket_8
+ search[salary_max]=*salary_bucket_0*  - string value in bewteen salary_bucket_0 to salary_bucket_8
+ search[skills]=*value1,value1*  - comma-separated list of skill names
+ search[min_work_experience_duration]=*work_experience_bucket_0* - string value in between work_experience_bucket_0 to salary_bucket_5
+ search[max_work_experience_duration]=*work_experience_bucket_0* - string value in between work_experience_bucket_0 to salary_bucket_5
+ search[important_skills]=*value*  - important marker for skills, value can be any value of [true | false], default: false
+ search[jobs] = *value1,value1*  - comma-separated list of jobs which company has
+ search[sort]=*value* - value can be any of [first_name | date | relevance], default: relevance
+ per=*value* - pagination, how many talent entries should be in the response. max is 100
+ page=*value* -  pagination, which page should be displayed

####  Filter details below
+ place of work = search[working_locations]
+ Interest in jobs = search[job_seeker_statuses]
+ Experience in industries = search[working_industries]
+ Skills = search[skills]
+ Languages = search[Languages] 
```
*Example:* /api/recruiter/talents?utf8=✓&search%5Bquery%5D=auhofstrasse&search%working_locations%5D=Vienna,Graz&search%5Bsort%5D=first_name&per=10&page=2
```

#### Response

The response of a talent search consists of data representing an array of talents (hash key *talents*) and metadata which includes pagination and aggregations (hash key *metadata*).
A signle talent object contains general personal information as well as links to downloadable resources (cf_pdf_uri, cv_attachments). In addition to that it holds information about the overall duration of work experience and education and the latest entries of those two CV modules.

Response contain both first the exact matches, followed by the close matches. In the metadata there is a total for exact and a total for close matches, which then can be used for splitting the results in the UI.


The metadata section provides details regarding pagination:

+ per - number of talents per page
+ page - current page
+ total - number of total entries for the given search criteria
+ total_pages - number of total pages available
+ exact_total - number of total entries for the given search criteria(exaxt match)
+ exact_total_pages - number of total pages available(exact_match)
+ close_total - number of total entries for close match
+ close_total_pages - numbe of total page available for close match

Aggregations are part of the reponses meta section and can be found under *aggregations*. Simple term aggregations (e.g. *agg_working_industries*) do have a buckets array including entries for each *key* plus the keys *doc_count*. Range aggregations (e.g. *agg_salary_bucket*) additionally have range limits in each bucket, namely *from* and/or *to*.

```
{
  "talents": [
    {
      "first_name": "Arthur",
      "last_name": "Warren",
      "age": 27,
      "title": "Dkfm. Mag. Dr.",
      "salutation": "mr",
      "cv_pdf_uri": "http://10.0.0.130:8080/prod2_dl/dl/v/umcrn4yv/w0ne2quqtn7ikywz2vxrkdo4oezxferf/aptkvybh/0an_vcY_s9ZnrQflE1P3CrMJr3W1G4Qc3_dLZcM8Ilo20160222-10926-zjckvl.pdf",
      "cv_attachments": [
        {
          "document_name": "Certificate.pdf",
          "document_uri": "http://10.0.0.130:8080/prod2_dl/dl/d/cus738xsk/aefaequqtn7ikywz2vxr23hsoezxferf/uycvnhf/Certificate.pdf",
          "document_type": "certificate"
        }
      ],
      "email": "t-13@t13s.at",
      "avatar_url": {
        "thumb": "http://10.0.0.130:8080/prod2_dl/dl/t/t9qkjofr/dt1awfiymqwqxvwxqyov2gveqjqcoozd/1sekrr8j/thumb_cbb897e1-46aa-4cfa-be6c-aa074d5d91be.png",
        "small": "http://10.0.0.130:8080/prod2_dl/dl/t/t9qkjofr/dt1awfiymqwqxvwxqyov2gveqjqcoozd/1sekrr8j/small_cbb897e1-46aa-4cfa-be6c-aa074d5d91be.png",
        "medium": "http://10.0.0.130:8080/prod2_dl/dl/t/t9qkjofr/dt1awfiymqwqxvwxqyov2gveqjqcoozd/1sekrr8j/medium_cbb897e1-46aa-4cfa-be6c-aa074d5d91be.png",
        "large": "http://10.0.0.130:8080/prod2_dl/dl/t/t9qkjofr/dt1awfiymqwqxvwxqyov2gveqjqcoozd/1sekrr8j/large_cbb897e1-46aa-4cfa-be6c-aa074d5d91be.png",
        "orginal": "http://10.0.0.130:8080/prod2_dl/dl/t/t9qkjofr/dt1awfiymqwqxvwxqyov2gveqjqcoozd/1sekrr8j/cbb897e1-46aa-4cfa-be6c-aa074d5d91be.png"
      },
      "work_experience_duration": 376.45065598447337,
      "education_duration": "235.23223423423",
      "latest_work_experience": {
        "position": "Environmental Tech",
        "company": "Topicware",
        "industry": "Manufacturing",
        "job_level": "intern",
        "terms_of_employment": "part_time",
        "from": "2015-09-02T22:38:42.144Z",
        "to": "2015-11-02T22:38:42.144Z",
        "current": false,
        "description": "lorem ipsum dolor sit amet consectetuer adipiscing elit proin risus",
        "country_id": 40
      },
      "latest_education": {
        "type_of_education": "Uni",
        "school_name": "School of Vista",
        "degree": null,
        "subject": "Software Engineer IV at The Baxter Building",
        "from": "2015-07-02",
        "to": "2015-10-02",
        "description": null,
        "current": false,
        "country_id": 40,
        "completed": null
      }
    }
  ],
  "meta": {
    "per": 25,
    "page": 1,
    "total": 1,
    "total_pages": 1,
    "aggregations": {
      "agg_availability": {
        "doc_count_error_upper_bound": 0,
        "sum_other_doc_count": 0,
        "buckets": [
          {
            "key": "immediately",
            "doc_count": 1
          }
        ]
      },
      "agg_prefered_employment_type": {
        "doc_count_error_upper_bound": 0,
        "sum_other_doc_count": 0,
        "buckets": [
          {
            "key": "full_time",
            "doc_count": 1
          }
        ]
      },
      "agg_skills": {
        "doc_count_error_upper_bound": 0,
        "sum_other_doc_count": 0,
        "buckets": []
      },
      "agg_job_seeker_status": {
        "doc_count_error_upper_bound": 0,
        "sum_other_doc_count": 0,
        "buckets": [
          {
            "key": "not_searching",
            "doc_count": 1
          }
        ]
      },
      "agg_working_locations": {
        "doc_count_error_upper_bound": 0,
        "sum_other_doc_count": 0,
        "buckets": [
          {
            "key": "Baden",
            "doc_count": 1
          }
        ]
      },
      "agg_salary_bucket": {
        "buckets": [
          {
            "key": "*-4000.0",
            "to": 4000,
            "to_as_string": "4000.0",
            "doc_count": 0
          },
          {
            "key": "4000.0-10000.0",
            "from": 4000,
            "from_as_string": "4000.0",
            "to": 10000,
            "to_as_string": "10000.0",
            "doc_count": 0
          },
          {
            "key": "10000.0-20000.0",
            "from": 10000,
            "from_as_string": "10000.0",
            "to": 20000,
            "to_as_string": "20000.0",
            "doc_count": 0
          },
          {
            "key": "20000.0-30000.0",
            "from": 20000,
            "from_as_string": "20000.0",
            "to": 30000,
            "to_as_string": "30000.0",
            "doc_count": 0
          },
          {
            "key": "30000.0-40000.0",
            "from": 30000,
            "from_as_string": "30000.0",
            "to": 40000,
            "to_as_string": "40000.0",
            "doc_count": 0
          },
          {
            "key": "40000.0-50000.0",
            "from": 40000,
            "from_as_string": "40000.0",
            "to": 50000,
            "to_as_string": "50000.0",
            "doc_count": 0
          },
          {
            "key": "50000.0-70000.0",
            "from": 50000,
            "from_as_string": "50000.0",
            "to": 70000,
            "to_as_string": "70000.0",
            "doc_count": 1
          },
          {
            "key": "70000.0-90000.0",
            "from": 70000,
            "from_as_string": "70000.0",
            "to": 90000,
            "to_as_string": "90000.0",
            "doc_count": 0
          },
          {
            "key": "90000.0-110000.0",
            "from": 90000,
            "from_as_string": "90000.0",
            "to": 110000,
            "to_as_string": "110000.0",
            "doc_count": 0
          },
          {
            "key": "110000.0-130000.0",
            "from": 110000,
            "from_as_string": "110000.0",
            "to": 130000,
            "to_as_string": "130000.0",
            "doc_count": 0
          },
          {
            "key": "130000.0-150000.0",
            "from": 130000,
            "from_as_string": "130000.0",
            "to": 150000,
            "to_as_string": "150000.0",
            "doc_count": 0
          },
          {
            "key": "150000.0-*",
            "from": 150000,
            "from_as_string": "150000.0",
            "doc_count": 0
          }
        ]
      },
      "agg_working_industries": {
        "doc_count_error_upper_bound": 0,
        "sum_other_doc_count": 0,
        "buckets": [
          {
            "key": "IT",
            "doc_count": 1
          },
          {
            "key": "Recruiting",
            "doc_count": 1
          },
          {
            "key": "Sales",
            "doc_count": 1
          }
        ]
      },
      "agg_experience_in_industries": {
        "doc_count_error_upper_bound": 0,
        "sum_other_doc_count": 0,
        "buckets": [
          {
            "key": "AuditTaxLaw",
            "doc_count": 2
          },
          {
            "key": "Craft",
            "doc_count": 2
          },
          {
            "key": "MarketingDesign",
            "doc_count": 2
          },
          {
            "key": "AgriFish",
            "doc_count": 1
          },
          {
            "key": "Arts",
            "doc_count": 1
          }
        ]
      },
       "agg_languages": {
        "doc_count_error_upper_bound": 0,
        "sum_other_doc_count": 0,
        "buckets": [
          {
            "key": "Croatian",
            "doc_count": 2
          },
          {
            "key": "Armenian",
            "doc_count": 1
          },
          {
            "key": "Fijian",
            "doc_count": 1
          },
          {
            "key": "Georgian",
            "doc_count": 1
          }
        ]
      },
      "agg_work_experience_duration": {
        "buckets": [
          {
            "key": "*-1.0",
            "to": 1,
            "to_as_string": "1.0",
            "doc_count": 9
          },
          {
            "key": "1.0-3.0",
            "from": 1,
            "from_as_string": "1.0",
            "to": 3,
            "to_as_string": "3.0",
            "doc_count": 9
          },
          {
            "key": "3.0-5.0",
            "from": 3,
            "from_as_string": "3.0",
            "to": 5,
            "to_as_string": "5.0",
            "doc_count": 7
          },
          {
            "key": "5.0-7.0",
            "from": 5,
            "from_as_string": "5.0",
            "to": 7,
            "to_as_string": "7.0",
            "doc_count": 20
          },
          {
            "key": "7.0-10.0",
            "from": 7,
            "from_as_string": "7.0",
            "to": 10,
            "to_as_string": "10.0",
            "doc_count": 31
          },
          {
            "key": "10.0-*",
            "from": 10,
            "from_as_string": "10.0",
            "doc_count": 8
          }
        ]
      }
    }
  }
}
```
## Deploy

* Deploy to Bluemix

Before *every* deployment, check the corresponding manifest file which *command:* is executed upon deployment. This line specifies whether a migration is applied or translations are pulled from localeapp.

```
./bluemix-deploy.sh [env]
```
where env can be dev2 or prod2
